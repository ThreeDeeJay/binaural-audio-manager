::Script by 3DJ
::Special thanks to stefanem, Grub4K and Sintrode

::Global
	@echo off
	CALL :ResetWorkingDirectory
	CHCP 1252 >NUL
	SETlocal EnableExtensions
	SETlocal EnableDelayedExpansion
	SET ScriptName=Binaural Audio Manager
	SET ScriptVersion=2.0-Alpha
	SET ScriptNameVersion=!ScriptName! v!ScriptVersion!
	CALL :SetTitle

::System
	CALL :SetDataFilePaths
	CALL :GetOSVersion
	CALL :GetDateAndTime
	CALL :GetSystemPaths
	if "!WindowsVersion!"=="XP" (MODE con:cols=121 lines=30)

::Default
	::HeSuVi
	SET HeSuViVersion=2.0.0.1
	SET EqualizerAPOVersion=1.2.1
	SET ASIO4ALLName=ASIO4ALL v2
	if !OSVersion! GEQ !Windows10OSVersion! (
		SET ASIO4ALLVersion=2.15 Beta 2
		) ELSE (
		SET ASIO4ALLVersion=2.14
		)
	SET ASIO4ALLCLSID={232685C6-6548-49D8-846D-4141A3EF7560}
	SET FlexASIOVersion=1.7a
	SET HiFiCableVersion=1.0.0.7
	SET "HiFiCableRegistryPath=HKEY_CURRENT_USER\VB-Audio\ASIOBridge"
	SET "HiFiCableInstallationFolderPath=!ProgramFiles32BitPath!\VB\ASIOBridge"
	SET "HiFiCableFullPath=!HiFiCableInstallationFolderPath!\VBCABLE_AsioBridge.exe"
	SET "HiFiCableUninstallerFullPath=!HiFiCableInstallationFolderPath!\Hi-Fi Cable.exe!"
	SET "HiFiCableStartupShortcutPath=!StartupShortcutFolderPath!\ASIO Bridge (VB-Audio).lnk"
	SET "VirtualSurroundDeviceLabel=Virtual Surround"
	SET "VirtualAudioDeviceName=VB-Audio Hi-Fi Cable"
	SET "VirtualAudioDeviceNameWASAPI=!VirtualSurroundDeviceLabel! (!VirtualAudioDeviceName!)"
	SET VolumeLinkerVersion=1.0.0.1
	SET "VolumeLinkerRegistryPath=HKEY_CURRENT_USER\SOFTWARE\VolumeLinker"
	for /f "delims=" %%A in ("!WindowsArchitectureBits!") do (SET "VolumeLinkerInstallationFolderPath=!ProgramFiles%%ABitPath!\VolumeLinker")
	SET "VolumeLinkerInstallationFullPath=!VolumeLinkerInstallationFolderPath!\VolumeLinker!WindowsArchitectureBits!.exe"
	SET "VolumeLinkerStartupShortcutPath=!StartupShortcutFolderPath!\VolumeLinker.lnk"
	CALL :GetEqualizerAPOPath
	CALL :GetHeSuViPath
	::Crystal Mixer
	SET CrystalMixerASIOName=Crystal Mixer Driver
	SET CrystalMixerVersion=[Win32]1.2.0.0
	SET "CrystalMixerUninstallerFullPath=!ProgramFiles32BitPath!\Crystal Mixer\unins000.exe"
	SET "CrystalMixerINIFullPath=!APPDATA!\Crystal Mixer\crystal_mixer_x86.ini"
	::OpenAL Soft
	IF "!WindowsVersion!"=="XP" (
		SET OpenALSoftVersion=[WinXP]2020-10-28_6ddacbc
		) ELSE (
		SET OpenALSoftVersion=[Win32]2021-04-19_2b64008
		)
	SET OpenALVersion=2.1.0.0
	SET "OpenALSoftInstallationPath=!APPDATA!\OpenAL"
	SET "OpenALSoftHRTFPath=!OpenALSoftInstallationPath!\HRTF"
	SET "OpenALSoftPresetsPath=!OpenALSoftInstallationPath!\presets"
	SET "OpenALSoftINIPath=!APPDATA!\alsoft.ini"
	SET "OpenALDLLx32Path=!DLLFolderPath32!\OpenAL32.dll"
	SET "OpenALDLLx64Path=!DLLFolderPath64!\OpenAL32.dll"
	::DSOAL
	IF "!WindowsVersion!"=="XP" (
		SET DSOALVersion=[WinXP]2020-10-13_6ddacbc
		) ELSE (
		SET DSOALVersion=[Win32]2021-05-16_cec17f0
		)
	::X3DAudio HRTF
	SET X3DAudioHRTFVersion=[Win64]2021-05-31_025db24
	::ALAudio
	SET ALAudioVersion=2.4.7-UT
	::MetaAudio
	SET MetaAudioVersion=0.3.0-rc1
	::EAX Unified
	SET EAXUnifiedVersion=4.0.0.1
	::EAXEFX
	SET EAXEFXVersion=[Win32]1.0.5
	::A2D
	SET A2DVersion=3.12
	::A3D-Live!
	SET A3D-LiveVersion=2.11.1014
	::A3D Alchemy
	SET A3DAlchemyVersion=1.0
	::a3dx5
	SET a3dx5Version=1.0
	::Creative Alchemy
	SET CreativeAlchemyVersion=1.45.20
	::CMSS-3D
	SET CMSS-3DHeadphoneVersion=[Win32]Default
	::IndirectSound
	SET IndirectSoundVersion=0.20
	::Microsoft Spatial Sound
	SET "WindowsSonicCLSID={b53d940c-b846-4831-9f76-d102b9b725a0}"
	::Sensaura 3D positional audio
	SET Sensaura3DPositionalAudioVersion=[Win32]Default
	::Creative Labs OpenAL32
	SET CreativeLabsOpenAL32Version=[Win32]2.2.0.7
	::Rapture3D
	SET Rapture3DVersion=2.7.4-Game
	SET "Rapture3DUninstallerFullPath=!ProgramFiles32BitPath!\Blue Ripple Sound\unins000.exe"
	SET "Rapture3DRegistryPath=HKEY_CURRENT_USER\SOFTWARE\BlueRippleSound\Speakers"

::Preferences
	CALL :LoadPreferences
	IF NOT DEFINED SampleRate		(SET SampleRate=48000)
	IF NOT DEFINED BitDepth			(set BitDepth=24)
	IF NOT DEFINED HRTF				(set HRTF=oal_dflt)
	IF NOT DEFINED DatabaseBranch	(SET DatabaseBranch=Beta)
	IF NOT DEFINED BackgroundMusic	(SET BackgroundMusic=On)
	IF NOT DEFINED LogLevel			(SET LogLevel=Off)
	CALL :SetURLs
	CALL :SetLogLevel
	CALL :SetLogTimestamp
	CALL :GetEqualizerAPOPath
	CALL :GetHeSuViPath
	CALL :UpdateHRTFHeSuViFilePath
	CALL :AddDesktopShortcut
	CALL :CheckEULA







::Input
	SET ExecutableFilePath=%~1
	IF NOT DEFINED ExecutableFilePath (
		CALL :CheckElevatedPrivileges
		IF !ERRORLEVEL! NEQ 0 (
			CALL :DeleteFile "!DefaultConfigurationFilePath!"
			)
		CALL :SplashInfoSmall
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[0m Loading system configuration..."
		CALL :GetDevice
		CALL :PlayBackgroundMusic
rem		CALL :ToggleHiFiCable
		GOTO :MainScreen
		) ELSE (
		CALL :CheckElevatedPrivileges
		IF !ERRORLEVEL! NEQ 0 (
			CALL :SplashInfoSmall
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[0m Loading system configuration..."
			CALL :GetDevice
			CALL :PlayBackgroundMusic
			)
		SET DirectDragAndDrop=True
		)
	CALL :SetPaths
	CALL :SetBackupPaths
:ProcessInput
	CALL :DetectDesktopShortcut
	CALL :DetectSteamShortcut
	CALL :CorrectUnrealEngine4ExecutablePath
:DoNotUseSteamID
	CALL :GetHalfLifeModTitle
	CALL :CheckElevatedPrivileges
	IF !ERRORLEVEL! EQU 0 (
		IF NOT DEFINED ExecutableFilePath (
	    	GOTO :MainScreen
			)
		)

:Fallback
	CALL :ClearConfiguration
	IF EXIST "!PortableConfigurationFilePath!" (
		SET "UserConfigurationFilePath=!PortableConfigurationFilePath!"
		CALL :CheckElevatedPrivileges
		IF !ERRORLEVEL! NEQ 0 (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[0m Existing installation configuration found in:[0m"
			CALL :PrintAndLog "[92m !UserConfigurationFilePath![0m"
			CALL :LoadPreferences
			CALL :LoadConfiguration
			CALL :MergeConfiguration
			CALL :SetBackupPaths
			GOTO :Elevate
			)
		)

	CALL :CheckElevatedPrivileges
	IF !ERRORLEVEL! NEQ 0 (
		:Reinstall
		CALL :DeleteFile "!ManualStepsFile!"
		CALL :DeleteFile "!NotesFile!"
		CALL :DeleteFile "!CreditsFile!"
		IF NOT "!DatabaseDownloaded!"=="True" (
			CALL :Fetch Database
			)
		CALL :SetFetchedConfigurationVariables
		for /F "tokens=2 delims==" %%V in ('set FetchedConfigurationVariables[') do (
			IF NOT DEFINED Fetched%%V (set Fetched%%V="")
			)
		IF DEFINED ExecutableFilePath (
			if exist "!DatabaseFile!" (
				CALL :LoadPreferences
				CALL :LoadConfiguration
				IF NOT DEFINED InputFilePath (SET "InputFilePath=%~1")
				CALL :MergeConfiguration
				CALL :SearchDatabase
				CALL :UpdateBinauralSoftwareVersions
				CALL :LoadPreferences
				CALL :SaveConfiguration
				)
			)
		IF NOT "!ProfileFoundInDatabase!"=="True" (
			CALL :AutoDetectConfiguration
			)
		)

:MainScreen
	CALL :SetBackupPaths
	if not "!Reinstalling!"=="True" (
		CALL :CheckElevatedPrivileges
		IF !ERRORLEVEL! EQU 0 (
			IF DEFINED ExecutableFilePath (
				IF DEFINED DirectDragAndDrop (
					CALL :Elevated
					) ELSE (
					GOTO :ExecutableMenu
					)
				) ELSE (
				CALL :DeleteFile "!UserConfigurationFilePath!"
				CALL :Elevated
				)
			) ELSE (
			if !DirectDragAndDrop!==True (
				GOTO :Elevate
				)
			)
		)


	::Check for .bat file drag and drop
	IF NOT DEFINED ExecutableFilePath (
		CALL :DeleteFile "!UserConfigurationFilePath!"
		SET FetchedBackend=""
		SET FetchedWrapper=""
		SET FetchedWrapperVersion=""
		SET "Renderer=HeSuVi"
		SET FetchedRenderer=""
		SET FetchedRendererVersion=""
		SET "Configuration=Headphone Virtual Surround"
		SET FetchedConfiguration=""
		SET FetchedHRTF=""
		GOTO :ExecutableMenu
		) ELSE (

		:ExecutableMenu
		cls
		CALL :LoadPreferences
		CALL :SplashInfo

		IF DEFINED ExecutableFolderPath (
			if exist "!ExecutableFilePath!" (
				CALL :PrintAndLog "[90m - Path:[0m             !ExecutableFolderPath!\"
				) ELSE (
				CALL :PrintAndLog "[90m - Path:[0m             [91m!ExecutableFolderPath!\[0m"
				)
			) ELSE (
			CALL :PrintAndLog "[90m - Path:[0m             Drop game/program shortcut/folder/executable into this window to get [36mbest[0m or [1mdefault/saved[0m settings"
			)

		CALL :GetExecutableArchitecture
		IF DEFINED ExecutableFile (
			if exist "!ExecutableFilePath!" (
				if "!ExecutableArchitecturexBits!"=="x86" (
					CALL :PrintAndLog "[90m - Executable:[0m       !ExecutableFile! [92m[Win32][0m"
					) ELSE (
					if "!ExecutableArchitecturexBits!"=="x64" (
						CALL :PrintAndLog "[90m - Executable:[0m       !ExecutableFile! [36m[Win64][0m"
						) ELSE (
						CALL :PrintAndLog "[90m - Executable:[0m       !ExecutableFile!"
						)
					)
				) ELSE (
				CALL :PrintAndLog "[90m - Executable:[0m       [91m!ExecutableFile! (Not found)[0m"
				)
			)

		IF DEFINED FetchedTitle (
			IF NOT !FetchedTitle!=="" (
				IF NOT "!FetchedTitle!"=="!ScriptParentFolder!" (
					IF DEFINED FetchedBaseVersion (
						IF NOT !FetchedBaseVersion!=="" (
							CALL :PrintAndLog "[90m - Profile:[0m          [36m!FetchedTitle![0m [90m[!FetchedBaseVersion!][0m"
							) ELSE (
							CALL :PrintAndLog "[90m - Profile:[0m          [36m!FetchedTitle![0m"
							)
						) else (
						CALL :PrintAndLog "[90m - Profile:[0m          [36m!FetchedTitle![0m"
						)
					)
				)
			) ELSE (
			IF DEFINED Title (
				SET FetchedTitle=!Title!
				CALL :PrintAndLog "[90m - Profile:[0m          [36m!FetchedTitle![0m"
				)
			)

		IF NOT !FetchedBackend!=="" (CALL :PrintAndLog "[90m - Backend:[0m          [36m!FetchedBackend![0m")

		IF NOT !FetchedWrapper!=="" (
			IF NOT !FetchedWrapperVersion!=="" (
				IF NOT "!WrapperVersion!"=="!FetchedWrapperVersion!" (
					CALL :PrintAndLog "[90m -[0m [1mW[90mrapper:[0m          [36m!FetchedWrapper:,= + ! !FetchedWrapperVersion![0m [93m*WARNING: Not recommended for games with anti-cheat*[0m"
					) ELSE (
					CALL :PrintAndLog "[90m -[0m [1mW[90mrapper:[0m          [36m!FetchedWrapper:,= + ![0m [1m!FetchedWrapperVersion![0m [93m*WARNING: Not recommended for games with anti-cheat*[0m"
					)
				) ELSE (
				for /f "delims=" %%W in ("!FetchedWrapper: =!") do (
					CALL :PrintAndLog "[90m -[0m [1mW[90mrapper:[0m          [36m!FetchedWrapper:,= + ![0m [1m!%%WVersion![0m [93m*WARNING: Not recommended for games with anti-cheat*[0m"
					)
				)
			) ELSE (
			IF DEFINED Wrapper (
				IF DEFINED WrapperVersion (
					CALL :PrintAndLog "[90m -[0m [1mW[90mrapper:[0m          [1m!Wrapper:,= + ! !WrapperVersion![0m [93m*WARNING: Not recommended for games with anti-cheat*[0m"
					) ELSE (
					for /f "delims=" %%A in ("!Wrapper!") do (
						CALL :PrintAndLog "[90m -[0m [1mW[90mrapper:[0m          [1m!Wrapper:,= + ! !%%AVersion![0m [93m*WARNING: Not recommended for games with anti-cheat*[0m"
						)
					)
				) ELSE (
				CALL :PrintAndLog "[90m -[0m [1mW[90mrapper:[0m          [90mNone[0m"
				)
			)




		SET "RendererMenuLabel=[90m -[0m [1mR[90menderer:[0m         "
		IF NOT !FetchedRenderer!=="" (
			IF NOT !FetchedRendererVersion!=="" (
				IF NOT "!RendererVersion!"=="!FetchedRendererVersion!" (
					SET "RendererMenuLabel=!RendererMenuLabel![36m!FetchedRenderer:,= + ! !FetchedRendererVersion![0m"
					) ELSE (
					SET "RendererMenuLabel=!RendererMenuLabel![36m!FetchedRenderer:,= + ![0m [1m!FetchedRendererVersion![0m"
					)
				) ELSE (
				for /f "delims=" %%W in ("!FetchedRenderer: =!") do (
					SET "RendererMenuLabel=!RendererMenuLabel![36m!FetchedRenderer:,= + ![0m [1m!%%WVersion![0m"
					)
				)
			) ELSE (
			IF DEFINED Renderer (
				IF DEFINED RendererVersion (
					SET "RendererMenuLabel=!RendererMenuLabel![1m!Renderer:,= + ! !RendererVersion![0m"
					) ELSE (
					for /f "delims=" %%A in ("!Renderer!") do (
						SET "RendererMenuLabel=!RendererMenuLabel![1m!Renderer:,= + ! !%%AVersion![0m"
						)
					)
				) ELSE (
				SET "RendererMenuLabel=[90mNone[0m"
				)
			)
		IF "!Renderer!"=="X3DAudio HRTF" (SET "RendererMenuLabel=!RendererMenuLabel! [93m*WARNING: Not recommended for games with anti-cheat*[0m")
		CALL :PrintAndLog "!RendererMenuLabel!"

		IF NOT !FetchedConfiguration!=="" (
			CALL :PrintAndLog "[90m -[0m [1mC[0m[90monfiguration:[0m    [36m!FetchedConfiguration:,= ![0m"
			) ELSE (
			IF NOT !Configuration!=="" (
				if "!FetchedRenderer!"=="HeSuVi" (
					CALL :PrintAndLog "[90m -[0m [1mC[0m[90monfiguration:[0m    [1m!Configuration:,= ! 7.1[0m"
					) ELSE (
					CALL :PrintAndLog "[90m -[0m [1mC[0m[90monfiguration:[0m    [1m!Configuration:,= ![0m"
					)
				)
			)

		IF DEFINED FetchedPrelude (
			IF NOT !FetchedPrelude!=="" (
				CALL :PrintAndLog "[90m - Decoder:[0m          [36m!FetchedPrelude![0m"
				)
			)
		if "!FetchedRenderer!"=="HeSuVi" (
			IF !DefaultPlaybackDeviceChannelCount! EQU 8 (
				CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m [36m[7.1 Surround][0m"
				) ELSE (
				IF !DefaultPlaybackDeviceChannelCount! EQU 6 (
					CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m [92m[5.1 Surround][0m"
					) ELSE (
					IF !DefaultPlaybackDeviceChannelCount! EQU 4 (
						CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m [93m[Quadraphonic][0m"
						) ELSE (
						IF !DefaultPlaybackDeviceChannelCount! EQU 2 (
							CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m [91m[Stereo][0m [93mVirtual 7.1 device will be used if not set to Surround[0m"
							) ELSE (
							IF !DefaultPlaybackDeviceChannelCount! EQU 1 (
								CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m [91m[Mono][0m [93mVirtual 7.1 device will be used if not set to Surround[0m"
								) ELSE (
								CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m [91m[Unknown speaker configuration][0m"
								)
							)
						)
					)
				)
			) ELSE (
			if defined DefaultPlaybackDeviceName (
				CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [1m!DefaultPlaybackDeviceName![0m"
				) ELSE (
				CALL :PrintAndLog "[90m -[0m [1mP[0m[90mlayback device:[0m  [91m[Unknown][0m"
				)
			)

		CALL :PrintAndLog "[90m -[0m [1mF[0m[90mormat:[0m           [1m!BitDepth!-bit, !SampleRate! Hz[0m"

		IF NOT "!FetchedBackend!"=="Microsoft Spatial Sound" (
			IF NOT !FetchedHRTF!=="" (
				IF NOT "!FetchedHRTF!"=="!HRTF!" (
					CALL :PrintAndLog "[90m -[0m [1mH[0m[90mRTF:[0m             [36m!FetchedHRTF![0m"
					) ELSE (
					GOTO :CheckDefaultHRTF
					)
				) ELSE (
				:CheckDefaultHRTF
				if "!HRTF!"=="oal_dflt" (
					CALL :PrintAndLog "[90m -[0m [1mH[0m[90mRTF:[0m             [1moal_dflt[0m [90m(default)[0m"
					) ELSE (
					CALL :PrintAndLog "[90m -[0m [1mH[0m[90mRTF:[0m             [1m!HRTF![0m"
					)
				)
			)

		IF DEFINED EqualizerPreset (
			CALL :PrintAndLog "[90m -[0m [1mE[0m[90mqualizer preset:[0m [1m!EqualizerPreset![0m"
			) ELSE (
			CALL :PrintAndLog "[90m -[0m [1mE[0m[90mqualizer preset:[0m [93mNot set[0m"
			)
		CALL :PrintAndLog "[90m -[0m [1mD[0m[90matabase branch:[0m  [1m!DatabaseBranch![0m"

		CALL :PrintNotes

		IF NOT DEFINED ExecutableFilePath (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[90m -[0m [1mM[0m[90music:[0m            [1m!BackgroundMusic![0m"
			CALL :PrintAndLog "[90m -[0m [1mL[0m[90mog level:[0m        [1m!LogLevel![0m"
			)

		CALL :PrintAndLog ""
		CALL :PrintAndLog "[1m L[90metter[0m + [0mEnter[0m      [90mChange setting[0m"
		CALL :PrintAndLog "[0m Enter[0m               [90mContinue[0m"
		CALL :SaveConfiguration

		CALL :PromptInput MenuSelection || GOTO :UseCurrentSettings
		IF DEFINED MenuSelection (
			SET MenuSelectionNoQuotes=!MenuSelection:"=!
			if "!MenuSelection!"=="!MenuSelectionNoQuotes!" (set MenuSelection=!MenuSelection: =!)
			CLS
			IF /I !MenuSelection!==W (
				CALL :SelectBinauralSoftwareVersion Wrapper
				GOTO :ExecutableMenu
				) ELSE (
				IF /I !MenuSelection!==R (
					CALL :SelectBinauralSoftwareVersion Renderer
					GOTO :ExecutableMenu
					) ELSE (
					IF /I !MenuSelection!==H (
						CALL :SelectHRTF
						GOTO :ExecutableMenu
						) ELSE (
						IF /I !MenuSelection!==F (
							CALL :SelectFormat
							GOTO :ExecutableMenu
							) ELSE (
							IF /I !MenuSelection!==C (
								CALL :SelectConfiguration
								GOTO :ExecutableMenu
								) ELSE (
								IF /I !MenuSelection!==E (
									:MenuSelectionE
									CALL :SelectEqualizerPreset
									GOTO :ExecutableMenu
									) ELSE (
									IF /I !MenuSelection!==M (
										:MenuSelectionM
										IF NOT "!BackgroundMusic!"=="Off" (
											SET "BackgroundMusic=Off"
											CALL :SavePreferenceSetting "BackgroundMusic" "Off"
											) ELSE (
											SET "BackgroundMusic=True"
											CALL :SavePreferenceSetting "BackgroundMusic" "On"
											)
										CALL :PlayBackgroundMusic
										GOTO :ExecutableMenu
										) ELSE (
										IF /I !MenuSelection!==P (
											:MenuSelectionP
											cls
											if "!FetchedRenderer!"=="HeSuVi" (
												CALL :ManualSpeakerConfiguration "[36m7.1 Surround[0m or [92m5.1 Surround[0m/[93mQuadraphonic[0m"
												) ELSE (
												CALL :ManualSpeakerConfiguration "[92mStereo[0m"
												)
											CALL :GetDevice
											GOTO :ExecutableMenu
											) ELSE (
											IF /I !MenuSelection!==D (
												CALL :SelectDatabaseBranch
												GOTO :ExecutableMenu
												) ELSE (
												IF /I !MenuSelection!==L (
													CALL :SelectLogLevel
													GOTO :ExecutableMenu
													) ELSE (
													if !MenuSelection!==!MenuSelectionNoQuotes! (set MenuSelection="!MenuSelection!")
													SET MenuSelectionNoQuotes=!MenuSelection:"=!
													SET MenuSelectionNoPeriods=!MenuSelection:.=!
													IF NOT !MenuSelection!==!MenuSelectionNoPeriods! (set MenuSelection="!MenuSelection!")
													iF /I not !MenuSelection!==!MenuSelectionNoQuotes! (
														if exist "!MenuSelection!" (
															CALL :DeleteFile "!UserConfigurationFilePath!"
															SET "ExecutableFilePath=!MenuSelection:"=!"
															SET "InputFilePath=!MenuSelection:"=!"
															SET Title=
															SET FetchedTitle=
															SET Backend=
															SET FetchedBackend=
															SET Wrapper=
															SET FetchedWrapper=
															SET WrapperVersion=
															SET FetchedWrapperVersion=
															SET Renderer=
															SET FetchedRenderer=
															SET RendererVersion=
															SET FetchedRendererVersion=
															SET SteamID=
															SET GameSteamTitle=
															CALL :SetPaths
															GOTO :ProcessInput
															) ELSE (
															GOTO :ExecutableMenu
															)
														)
													)
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)

:UseCurrentSettings
	IF NOT "!Renderer!"=="HeSuVi" (
		IF NOT "!FetchedRenderer!"=="HeSuVi" (
			IF NOT "!Wrapper!"=="Crystal Mixer" (
				IF NOT "!FetchedWrapper!"=="Crystal Mixer" (
					IF NOT DEFINED ExecutableFolderPath (
						CALL :PrintAndLog "[91m ERROR: Installation folder needs to be specified via drag and drop.[0m"
						pause
						GOTO :ExecutableMenu
						)
					)
				)
			)
		)
	CALL :PrintAndLog "[0m"

:Elevate
	CALL :CheckExecutableFilePath
	CALL :StopAllSounds
	SET "ELEVATE=!temp!\ELEVATE.vbs"
	net file 1>NUL 2>NUL || (
	    if "%~1" neq "ELEVATE" (
	        CALL :PrintAndLog "Requesting administrative privileges..."
	        >"!ELEVATE!" echo CreateObject^("Shell.Application"^).ShellExecute "%comspec%", "/c """"%~0"" ""!ExecutableFilePath!"" ""%~1""""", "", "runas", 1
	        start "" "wscript" /B /NOLOGO "!ELEVATE!"
	        exit /B 1
	        exit /B 1
	        exit /B 1
	        exit /B 1
	        exit /B 1
		    ) ELSE (
	        del "!ELEVATE!" 1>NUL 2>&1
	        <nul set /P "=Could not auto elevate, please rerun as administrator..."
	        pause >nul
	        exit /B 9001
		    )
		)
	shift
	del "!ELEVATE!" 1>NUL 2>&1
	CD /D "%CD%"

:Elevated
	IF "!ExecutableFilePath!"=="!ExecutableFilePath:.exe=!" (SET "ExecutableFilePath=%~1")
	IF NOT DEFINED ExecutableFilePath (
		CALL :LoadPreferences
		CALL :LoadConfiguration
		CALL :MergeConfiguration
		GOTO :MainSetup
		)
	CALL :SetPaths
	CALL :LoadPreferences
	CALL :LoadConfiguration
	CALL :MergeConfiguration
	IF NOT DEFINED InputFilePath (SET "InputFilePath=%~1")

:MainSetup
	cls
	::Check versions and architextures
	IF NOT "!FetchedBackend!"=="Microsoft Spatial Sound" (
		CALL :SetBinauralSoftwareTypes
		for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
			if !Fetched%%TVersion!=="" (for /f "delims=" %%A in ("!%%T: =!") do SET "Fetched%%TVersion=!%%AVersion!")
			IF NOT "%%T"=="Base" (
				IF DEFINED Fetched%%T (
					IF NOT !Fetched%%T!=="" (
						IF NOT exist "Resources/%%T/!Fetched%%T!/Version/!Fetched%%TVersion!/*" (
							if exist "Resources/%%T/!Fetched%%T!/Version/*" (
								CALL :PrintAndLog "[91m %%T installation currently unsupported because installation folder is empty.[0m"
								CALL :PrintAndLog "[93m Expected path: Resources/%%T/!Fetched%%T!/Version/[0m"
								IF DEFINED FetchedGuideURL (IF NOT !FetchedGuideURL!=="" (CALL :BrowseURL "!FetchedGuideURL!"))
								CALL :SelectBinauralSoftwareVersion %%T
								IF DEFINED %%TVersion (SET "Fetched%%TVersion=!%%TVersion!")
								) ELSE (
								CALL :PrintAndLog "[91m %%T installation currently unsupported because installation folder does not exist or is empty.[0m"
								CALL :PrintAndLog "[93m Expected path: Resources/%%T/!Fetched%%T!/Version/!Fetched%%TVersion![0m"
								IF DEFINED FetchedGuideURL (IF NOT !FetchedGuideURL!=="" (CALL :BrowseURL "!FetchedGuideURL!"))
								CALL :SelectBinauralSoftware %%T
								IF DEFINED %%T (SET "Fetched%%T=!%%T!")
								)
							GOTO :ExecutableMenu
							)
						CALL :GetExecutableArchitecture
						if "!ExecutableArchitecturexBits!"=="x86" (
							CALL :Log ""
							CALL :Log "Executable architecture detected: 32-bit"
							IF NOT "!Fetched%%TVersion!"=="!Fetched%%TVersion:Win64=!" (
								if exist "Resources/%%T/!Fetched%%T!/Version/!Fetched%%TVersion:Win64=Win32!/*" (
									SET "Fetched%%TVersion=!Fetched%%TVersion:Win64=Win32!"
									CALL :SaveConfiguration
									CALL :PrintAndLog "[93m WARNING: Win32 executable detected. %%T set to[0m [92m!Fetched%%T! !Fetched%%TVersion![0m"
									) ELSE (
									CALL :PrintAndLog "[91mERROR: !Fetched%%T! !Fetched%%TVersion! is incompatible with the selected Win32 executable.[0m"
									CALL :SelectBinauralSoftwareVersion %%T
									IF DEFINED %%TVersion (SET "Fetched%%TVersion=!%%TVersion!")
									GOTO :MainSetup
									)
								)
							) ELSE (
							if "!ExecutableArchitecturexBits!"=="x64" (
								CALL :Log ""
								CALL :Log "Executable architecture detected: 64-bit"
								IF NOT "!Fetched%%TVersion!"=="!Fetched%%TVersion:Win32=!" (
									if exist "Resources/%%T/!Fetched%%T!/Version/!Fetched%%TVersion:Win32=Win64!/*" (
										SET "Fetched%%TVersion=!Fetched%%TVersion:Win32=Win64!"
										CALL :PrintAndLog "[93m WARNING: Win64 executable detected. %%T set to[0m [92m!Fetched%%T! !Fetched%%TVersion![0m"
										) ELSE (
										CALL :PrintAndLog "[91mERROR: !Fetched%%T! !Fetched%%TVersion! is incompatible with the selected Win64 executable.[0m"
										CALL :SelectBinauralSoftwareVersion %%T
									IF DEFINED %%TVersion (SET "Fetched%%TVersion=!%%TVersion!")
										GOTO :MainSetup
										)
									)
								)
							)
						)
					)
				)
			)
		) ELSE (
		IF !SampleRate! GTR 48000 (CALL :AdjustSampleRate Backend)
		)
	if "!FetchedWrapper!"=="ALAudio" (IF !SampleRate! GTR 48000 (CALL :AdjustSampleRate Wrapper))
	if "!FetchedWrapper!"=="Creative ALchemy" (
		if /I "!DefaultSoundCardNameWASAPI!"=="!DefaultSoundCardNameWASAPI:X-Fi=!" (
			if /I "!DefaultSoundCardNameWASAPI!"=="!DefaultSoundCardNameWASAPI:Audigy=!" (
				if /I "!DefaultSoundCardNameWASAPI!"=="!DefaultSoundCardNameWASAPI:Blaster=!" (
					CALL :PrintAndLog "[91m ERROR: !DefaultSoundCardNameWASAPI! is not compatible with Creative ALchemy.[0m"
					CALL :PrintAndLog "[1m Alternatively, you can select a DirectSound3D wrapper like[0m [96mDSOAL[0m [1mor switch to a Creative sound card if available.[0m"
					CALL :SelectBinauralSoftware Wrapper
					)
				)
			)
		)


	::Info
	SET FullTitle=!ScriptNameVersion!
	IF DEFINED FetchedWrapper 			(IF NOT !FetchedWrapper!=="" 			(Set FullTitle=!FullTitle! - !FetchedWrapper!))
	IF DEFINED FetchedWrapperVersion	(IF NOT !FetchedWrapperVersion!=="" 	(Set FullTitle=!FullTitle! !FetchedWrapperVersion!))
	IF DEFINED FetchedRenderer			(IF NOT !FetchedRenderer!=="" 			(Set FullTitle=!FullTitle! - !FetchedRenderer!))
	IF DEFINED FetchedRendererVersion	(IF NOT !FetchedRendererVersion!==""	(Set FullTitle=!FullTitle! !FetchedRendererVersion!))
	IF NOT DEFINED FetchedRenderer		(SET "FetchedRenderer=HeSuVi")
	IF NOT DEFINED FetchedHRTF			(SET "FetchedHRTF=!HRTF!")
	title !FullTitle!
	CALL :SplashInfoInstall
	CALL :SetBackupPaths
	CALL :CreateRestorePoint
	CALL :IndexInstallationFiles
	CALL :SearchExistingInstallation
	:StartFromUninstall
	CALL :Uninstall
	CALL :Restore
	:StartFromBackup
	CALL :Backup
	:StartFromInstall
	CALL :Install
	CALL :AutomaticConfiguration
	CALL :Complete


::Functions - General

:Notes
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[0m Notes:[0m"
	if not "!LogLevel!"=="Off" (
		CALL :PrintAndLog "[90m - [0mLog.txt [90mis located in the [0mUser [90mfolder for troubleshooting.[0m"
		)
	if exist "!BackupPath!" (
		CALL :PrintAndLog "[90m - Backup is located in [0m!BackupPath!"
		) ELSE (
		CALL :PrintAndLog "[90m - Backup is located in [0mUser\Backup"
		)
	IF "!FetchedRenderer!"=="HeSuVi" (for /f "delims=" %%s in ("!SampleRateAdjusted!") do IF "!SampleRateAdjusted%%s!"=="True" (CALL :PrintAndLog "[93m - !Fetched%%s! does not support the specified sample rate, so it has been set to !SampleRate! Hz.[0m"))
	IF "!FetchedRenderer!"=="HeSuVi" (IF !SampleRate! GTR 48000 (CALL :AdjustSampleRate Renderer))
	IF NOT exist "!ManualStepsFile!" (IF DEFINED ExecutableFilePath (if NOT "!FetchedConfiguration!"=="!FetchedConfiguration:Surround=!" (CALL :PrintAndLog "[93m - If applicable, set in-game speaker configuration to 4.0, 5.1 or 7.1 Surround (preferably highest available).[0m")))
	IF NOT "!FetchedConfiguration!"=="!FetchedConfiguration:Virtual Surround=!" (IF "!FlexASIOWASAPIExclusive!"=="false" (CALL :PrintAndLog "[93m - To disable virtualization for specific apps, set their output device to !DefaultSoundCardNameWASAPI!.[0m"))
	IF NOT exist "!ManualStepsFile!" (IF DEFINED ExecutableFilePath (if "!FetchedWrapper!"=="DSOAL" (CALL :PrintAndLog "[93m - Enable DirectSound3D/EAX/Hardware acceleration if the game has any of those audio options.[0m")))
	IF NOT exist "!ManualStepsFile!" (IF DEFINED ExecutableFilePath (if "!FetchedWrapper!"=="EAXEFX" (CALL :PrintAndLog "[93m - Enable EAX/Hardware acceleration if the game has any of those audio options.[0m")))
	if exist "!USERPROFILE!\Desktop\!ShortcutFilename! MetaAudio.lnk" (if "!FetchedWrapper!"=="MetaAudio" (CALL :PrintAndLog "[93m - Avoid Valve Anti-Cheat servers and always run the new desktop shortcut: !ShortcutFilename! MetaAudio[0m"))
	CALL :PrintManualSteps

	IF "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (
		CALL :PrintAndLog "[93m - If you don't hear anything:"
		IF "!ASIODevice!"=="!ASIO4ALLName!" (CALL :PrintAndLog "[93m   - Click the ASIO4ALL tray icon (green play button) and select !DefaultSoundCardName!.[0m")
		CALL :PrintAndLog "[93m   - Click the ASIO Bridge tray icon, then click the button to turn ASIO off and back on."
		CALL :PrintAndLog "[93m   - Uninstall, restart your computer and reinstall."
		)
	CALL :PrintAndLog "[96m - To uninstall, run the script the same way again.[0m"
	CALL :AnonyimizeUserFiles
	EXIT /B


:SplashInfoSmall
	cls
	CALL :PrintAndLog "[90m                                    [0m[91m  /[0m[96m::::::::: [0m        [96m:::::   [0m   [91m /[0m[96m::::     ::::[0m [90m[0m"
	CALL :PrintAndLog "[90m::::::::::::::::::::::::::::::::::::[0m[91m | [0m[96m:+:    :+:[0m   [91m  /[0m [96m:+: :+:  [0m   [91m|[0m [96m+:+:+: :+:+:+[0m [90m::::::::::::::::::::::::::::::::::::[0m"
	CALL :PrintAndLog "[90m:+::+::+::+::+::+::+::+::+::+::+::+:[0m[91m | [0m[96m+:+    +:+[0m   [91m /[0m [96m+:+   +:+ [0m   [91m|[0m [96m+:+ +:+:+ +:+[0m [90m:+::+::+::+::+::+::+::+::+::+::+::+:[0m"
	CALL :PrintAndLog "[90m+:++:++:++:++:++:++:++:++:++:++:++:+[0m[91m | [0m[96m+#++:++#+ [0m   [91m|[0m [96m+#++:++#++:[0m   [91m|[0m [96m+#+  +:+  +#+[0m [90m+:++:++:++:++:++:++:++:++:++:++:++:+[0m"
	CALL :PrintAndLog "[90m+#++:++#++#++:++#++#++:++#++#++:++#+[0m[91m | [0m[96m+#+    +#+[0m   [91m|[0m [96m+#+     +#+[0m   [91m|[0m [96m+#+       +#+[0m [90m+#++:++#++#++:++#++#++:++#++#++:++#+[0m"
	CALL :PrintAndLog "[90m+#++#++#++#++#++#++#++#++#++#++#++#+[0m[91m | [0m[96m#+#    #+#[0m   [91m|[0m [96m#+#     #+#[0m   [91m|[0m [96m#+#[0m       [96m#+#[0m [90m+#++#++#++#++#++#++#++#++#++#++#++#+[0m"
	CALL :PrintAndLog "[90m#+##+##+##+##+##+##+##+##+##+##+##+#[0m[91m | [0m[96m######### [0m   [91m|[0m [96m###     ###[0m   [91m|[0m [96m###[0m       [96m###[0m [90m#+##+##+##+##+##+##+##+##+##+##+##+#[0m"
	CALL :PrintAndLog "[90m####################################[0m[91m |_________/    |___/    ___/   |___/      ___/[0m [90m####################################[0m"
	CALL :PrintAndLog "[90m:::::::::::::::::::::::::::::::::::::::::: [0mMore info:[0m [1mkutt.it/BinauralSoftware[0m [90m:::::::::::::::::::::::::::::::::::::::::[0m"
	EXIT /B

:SplashInfo
	CALL :PrintAndLog "[90m:::::::::::::::::::::::::::::::::::::::::::::[0m[1m    !ScriptName!   [0m[90m::::::::::::::::::::::::::::::::::::::::::::::[0m"
	CALL :PrintAndLog "[90m::::::::::::::::::::::::: Binaural audio (2D Virtual Surround and 3D Spatial Audio) installer ::::::::::::::::::::::::::[0m"
	CALL :PrintAndLog "[90m:::::::::::::::::::::::::: By 3DJ - github.com/ThreeDeeJay | Support: kutt.it/BinauralDiscord ::::::::::::::::::::::::::[0m"
	CALL :PrintAndLog ""
	EXIT /B

:SplashInfoInstall
	cls
	CALL :PrintAndLog "[90m:::::::::::::::::::::::::::::::::::::::[0m[7m !FetchedRenderer! [0m[90m:::::::::::::::::::::::::::::::::::::::[0m"
	CALL :SplashInfoTitle
	EXIT /B

:SplashInfoTitle
	IF DEFINED FetchedTitle (
		IF NOT !FetchedTitle!=="" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[0m [4;1m!FetchedTitle![0m "
			)
		)
	EXIT /B

:SetBackupPaths
	CALL :LoadConfiguration
	IF DEFINED ExecutableParentFolder (
		IF DEFINED FetchedBase (
			IF NOT !FetchedBase!=="" (
				SET BackupPath=User\Backup\Backup_!ExecutableParentFolder!_!FetchedBase!
				)
			) ELSE (
			SET BackupPath=User\Backup\Backup_!ExecutableParentFolder!_!ExecutableFilename!_!RANDOM!
			)
		) ELSE (
		SET BackupPath=User\Backup\Backup_!FetchedRenderer!_!ExecutableFilename!_!RANDOM!
		)
	SET "RegistryBackup-SpeakerConfiguration=User\Backup\Common\SpeakerConfiguration.reg"
	SET "RegistryBackup-Enhancements=User\Backup\Common\Enhancements.reg"
	SET "RegistryBackup-DirectSound=User\Backup\Common\DirectSound"
	CALL :SaveConfigurationSetting "BackupPath" "!BackupPath!"
	CALL :LoadConfiguration
	EXIT /B

::Functions - Option selection

:SelectBinauralSoftwareVersion
	CALL :SetBinauralSoftwareTypes
	for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
		if %1==%%T (if !Fetched%%T!=="" (GOTO :SelectBinauralSoftware %1))
		)
	SET VersionSelect=
	for /f "delims=" %%T in ("%1") do (
		SET count=0
		SET choice[!count!]=!%%AVersion!
		IF EXIST "Resources\%%T\!Fetched%%T!\Version" (
			CD /D "Resources\%%T\!Fetched%%T!\Version"
			for /d %%x in (*) do (
				SET /a count=count+1
				SET choice[!count!]=%%x
				)
			for /f "delims=" %%v in ("!Fetched%%T: =!") do (
				SET choice[0]=!%%vVersion!
				)
			CALL :ResetWorkingDirectory
			) ELSE (
			cls
			GOTO :SelectBinauralSoftware %1
			)
		IF !count! GTR 1 (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[1m!Fetched%%T![0m [90mversion selection. When in doubt, select the newest one.[0m"
			::Print list of versions/folders
			for /f "delims=" %%A in ("!Fetched%%T: =!") do CALL :PrintAndLog " [1m0[0m [1m!%%AVersion![0m [90m(current)[0m"
			for /l %%x in (1,1,!count!) do (
				if %%x LEQ 9 (
					IF NOT "!choice[%%x]!"=="!choice[%%x]:[Win32]=!" (ECHO  [1m%%x[0m [92m!choice[%%x]![0m
						) ELSE (
					IF NOT "!choice[%%x]!"=="!choice[%%x]:[Win64]=!" (ECHO  [1m%%x[0m [36m!choice[%%x]![0m
						) ELSE (
																	  ECHO  [1m%%x[0m [1;36m!choice[%%x]![0m
							)
						)
			   		) ELSE (
					IF NOT "!choice[%%x]!"=="!choice[%%x]:[Win32]=!" (ECHO [1m%%x[0m [92m!choice[%%x]![0m
						) ELSE (
					IF NOT "!choice[%%x]!"=="!choice[%%x]:[Win64]=!" (ECHO [1m%%x[0m [36m!choice[%%x]![0m
						) ELSE (
																	  ECHO [1m%%x[0m [1;36m!choice[%%x]![0m
							)
						)
			   		)
				)
			CALL :PrintAndLog "[90mTo change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m[90m.[0m"
			CALL :PrintAndLog "[90mTo change %%T, just press[0m [0mEnter[0m[90m.[0m"
			::Prompt user selection
			CALL :PromptInput VersionSelect
			CALL :PrintAndLog "[1m"
			IF NOT DEFINED VersionSelect (
				cls
				GOTO :SelectBinauralSoftware %1
				)
			if !VersionSelect! LSS 0		(GOTO :MainScreen)
			if !VersionSelect! GTR !count!	(GOTO :MainScreen)
		) ELSE (
		SET VersionSelect=1
		)
		IF DEFINED VersionSelect (
			for /f "delims=" %%A in ("!VersionSelect!") do (
				SET "%%TVersion=!choice[%%A]!"
				SET "!Fetched%%T: =!Version=!choice[%%A]!"
				SET %%T=!Fetched%%T!
				SET Fetched%%T=""
				SET Fetched%%TVersion=""
				set BaseVersion=
				set FetchedBaseVersion=
				CALL :SaveConfigurationSetting "BaseVersion" "!FetchedBaseVersion!"
				CALL :SavePreferenceSetting "!%%T: =!Version" "!choice[%%A]!"
				if "!%%T!"=="HeSuVi" (
					SET Wrapper=
					SET FetchedWrapper=""
					SET WrapperVersion=
					SET FetchedWrapperVersion=""
					SET RendererVersion=!HeSuViVersion!
					SET "FetchedConfiguration=Headphone Virtual Surround"
					)
				if "!%%T!"=="OpenAL Soft" (
					SET Wrapper=
					SET FetchedWrapper=""
					SET WrapperVersion=
					SET FetchedWrapperVersion=""
					SET RendererVersion=!OpenALSoftVersion!
					SET "FetchedConfiguration=Headphone Spatial Audio"
					)
				if "!%%T!"=="Rapture3D" (
					SET Wrapper=
					SET FetchedWrapper=""
					SET WrapperVersion=
					SET FetchedWrapperVersion=""
					SET RendererVersion=!Rapture3DVersion!
					SET "FetchedConfiguration=Headphone Spatial Audio"
					)
				if "!%%T!"=="X3DAudio HRTF" (
					SET Wrapper=
					SET FetchedWrapper=""
					SET WrapperVersion=
					SET FetchedWrapperVersion=""
					SET RendererVersion=!X3DAudioHRTFVersion!
					SET "FetchedConfiguration=Headphone Spatial Audio"
					)
				if "!%%T!"=="DSOAL" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					SET "FetchedConfiguration=Headphone Spatial Audio"
					)
				if "!%%T!"=="ALAudio" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					SET "FetchedConfiguration=Headphone Spatial Audio"
					)
				if "!%%T!"=="MetaAudio" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					SET "FetchedConfiguration=Headphone Spatial Audio"
					)
				if "!%%T!"=="EAX Unified" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="EAXEFX" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="DSOAL" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="Crystal Mixer" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					SET "ASIODevice=!CrystalMixerASIOName!"
					CALL :SavePreferenceSetting "ASIODevice" "!CrystalMixerASIOName!"
					)
				if "!%%T!"=="A2D" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="A3D-Live" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="A3D Alchemy" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="a3dx5" (
					SET Renderer=
					SET "FetchedRenderer=OpenAL Soft"
					SET RendererVersion=!OpenALSoftVersion!
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="Creative ALchemy" (
					SET Renderer=
					SET "FetchedRenderer=CMSS-3D Headphone"
					SET RendererVersion=
					SET FetchedRendererVersion=""
					)
				if "!%%T!"=="IndirectSound" (
					SET Renderer=
					SET "FetchedRenderer=HeSuVi"
					SET RendererVersion=!HeSuViVersion!
					SET FetchedRendererVersion=""
					SET "FetchedConfiguration=Headphone Virtual Surround"
					)
REM CALL :CheckElevatedPrivileges
REM IF !ERRORLEVEL! EQU 0 (
REM 	IF !FetchedWrapper!=="" 		(IF DEFINED Wrapper 		(SET FetchedWrapper=!Wrapper!))
REM 	IF !FetchedWrapperVersion!==""	(IF DEFINED WrapperVersion	(SET FetchedWrapperVersion=!WrapperVersion!))
REM 	IF !FetchedRenderer!=="" 		(IF DEFINED Renderer 		(SET FetchedRenderer=!Renderer!))
REM 	IF !FetchedRendererVersion!=="" (IF DEFINED RendererVersion (SET FetchedRendererVersion=!RendererVersion!))
REM 	CALL :SaveConfiguration
REM )
				)
			)
		)
	EXIT /B

:SelectBinauralSoftware
	SET VersionSelect=
	for /f "delims=" %%T in ("%1") do (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[90m%%Ts available to install:[0m"
		SET count=0
		CD /D "Resources\%%T"
		for /d %%x in (*) do (
			SET /a count=count+1
			SET choice[!count!]=%%x
			)
		CALL :ResetWorkingDirectory
		::Print list of versions/folders
		for /l %%x in (1,1,!count!) do (
		   if %%x LEQ 9 (CALL :PrintAndLog "  [1m%%x[0m [1;36m!choice[%%x]![0m"
		   		) else 	(CALL :PrintAndLog " [1m%%x[0m [1;36m!choice[%%x]![0m")
			)
		CALL :PrintAndLog "[90mTo change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m[90m.[0m"
		CALL :PrintAndLog "[90mTo keep current version, type[0m [1m0[0m [90mthen press[0m [0mEnter[0m[90m.[0m"
		::Prompt user selection
		CALL :PromptInput VersionSelect
		CALL :PrintAndLog "[1m"
		if "!VersionSelect!"==""		(GOTO :MainScreen)
		if !VersionSelect! LEQ 0		(GOTO :MainScreen)
		if !VersionSelect! GTR !count!	(GOTO :MainScreen)
		IF DEFINED VersionSelect (
			for /f "delims=" %%A in ("!VersionSelect!") do (
				SET "Fetched%%T=!choice[%%A]!"
				for /f "delims=" %%A in ("!Fetched%%T!") do SET "Fetched%%TVersion=!%%AVersion!"
				)
			)
		)
	CLS
	GOTO :SelectBinauralSoftwareVersion %1
	EXIT /B

:SelectEqualizerPreset
	CALL :DeleteFolder "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq"
	CALL :CreateFolder "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq"
	CALL :Extract "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq.extract" "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq"
	CALL :ListArchiveFiles "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq\*" /D EqualizerPresetBrand Brand
	IF DEFINED EqualizerPresetBrand (
		CALL :ListArchiveFiles "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq\!EqualizerPresetBrand!\*.txt" /R EqualizerPresetModel Model
		IF DEFINED EqualizerPresetBrand (
			IF NOT "!EqualizerPresetBrand!"=="" (
				IF DEFINED EqualizerPresetModel (
					IF NOT "!EqualizerPresetModel!"=="" (
						SET "EqualizerPreset=!EqualizerPresetBrand! !EqualizerPresetModel!"
						CALL :SavePreferenceSetting "EqualizerPresetBrand" "!EqualizerPresetBrand!"
						CALL :SavePreferenceSetting "EqualizerPresetModel" "!EqualizerPresetModel!"
						)
					)
				)
			)
		)
	CALL :DeleteFolder "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq"
	EXIT /B


:ListArchiveFiles
	cls
	IF DEFINED ArchiveItemName (for /F "tokens=2 delims==" %%s in ('set ArchiveItemName[') do (set %%s=))
	SET ArchiveItemName[#]=
	SET ArchiveFilePathCount=
	SET ArchiveFilePathCount=-1
	SET ArchiveFolderPath=%1
	FOR %2 %%D in (!ArchiveFolderPath!) do (
		SET "ArchiveItem=%%~nxD"
		SET "ArchiveItem=!ArchiveItem:.txt=!"
		SET /A ArchiveFilePathCount=ArchiveFilePathCount+1
		SET "ArchiveItemName[!ArchiveFilePathCount!]=!ArchiveItem!"
		SET "ArchiveItemName[#]=!ArchiveFilePathCount!"
		)
	CALL :PrintAndLog "%4 selection"
	CALL :GridChoose ArchiveItemName %3 %4
	EXIT /B

::Based on code by Grub4K
:GridChoose  <valueArray>
	SET TableLength=50
	SET TableCount=1
	SET "pad="
	for /l %%l in ( 1 1 %TableLength% ) do SET "pad=!pad! "
	SET /a "lines=(%~1[#] + TableCount - 1) / TableCount"
	SET "counter=0"
	for %%p in ("%TableLength%") do (
	    for /L %%. in ( 1 1 %lines% ) do (
	        SET "line="
	        for /l %%l in ( 1 1 %TableCount% ) do (
	            set /a "counter+=1"
	            IF DEFINED %~1[!counter!] (
	                for %%c in ("!counter!") do SET "padded=!%~1[%%~c]!!pad!"
	                SET "counter=   !counter!"
	                if "!padded!" neq "!padded:[Win32]=!" (
	                    SET "disp=[92m!padded:~0,%%~p![0m"
	                ) else if "!padded!" neq "!padded:[Win32]=!" (
	                    SET "disp=[36m!padded:~0,%%~p![0m"
	                ) else SET "disp=[1;36m!padded:~0,%%~p![0m"
	                SET "line=!line! [1m!counter:~-3![0m !disp!"
	            )
	        )
		echo !line:~1!
	    )
	)
	SET ListSelectionIndex=
	:ListSelectionIndexPrompt
	CALL :PromptInput ListSelectionIndex "[90mType the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m. [1m"
	IF DEFINED ListSelectionIndex (
		if !ListSelectionIndex! LEQ !ArchiveItemName[#]! (
			if !ListSelectionIndex! GTR 0 (
				for /F "tokens=2 delims==" %%s in ('set ArchiveItemName[') do (
					if !ArchiveItemName[%ListSelectionIndex%]!==%%s (
						SET %2=%%s
						)
					)
				) ELSE (
				CALL :PrintAndLog "[91mInvalid selection, try again.[1m"
				GOTO :ListSelectionIndexPrompt
				)
			) ELSE (
			CALL :PrintAndLog "[91mInvalid selection, try again.[1m"
			GOTO :ListSelectionIndexPrompt
			)
		) ELSE (
		GOTO :ExecutableMenu
		)
	exit /B

:SelectHRTF
	cls
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90mCurrently selected HRTF:[0m [0m!HRTF![0m"
	CALL :PrintAndLog ""
	SET "choice[0]=oal_dflt"
	SET "choice[+01]=SADIE_D01"
	SET "choice[+02]=SADIE_D02"
	SET "choice[+03]=SADIE_H03"
	SET "choice[+04]=SADIE_H04"
	SET "choice[+05]=SADIE_H05"
	SET "choice[+06]=SADIE_H06"
	SET "choice[+07]=SADIE_H07"
	SET "choice[+08]=SADIE_H08"
	SET "choice[+09]=SADIE_H09"
	SET "choice[+10]=SADIE_H10"
	SET "choice[+11]=SADIE_H11"
	SET "choice[+12]=SADIE_H12"
	SET "choice[+13]=SADIE_H13"
	SET "choice[+14]=SADIE_H14"
	SET "choice[+15]=SADIE_H15"
	SET "choice[+16]=SADIE_H16"
	SET "choice[+17]=SADIE_H17"
	SET "choice[+18]=SADIE_H18"
	SET "choice[+19]=SADIE_H19"
	SET "choice[+20]=SADIE_H20"
	SET "choice[-01]=SADIE_D01_(Inverted_Y)"
	SET "choice[-02]=SADIE_D02_(Inverted_Y)"
	SET "choice[-03]=SADIE_H03_(Inverted_Y)"
	SET "choice[-04]=SADIE_H04_(Inverted_Y)"
	SET "choice[-05]=SADIE_H05_(Inverted_Y)"
	SET "choice[-06]=SADIE_H06_(Inverted_Y)"
	SET "choice[-07]=SADIE_H07_(Inverted_Y)"
	SET "choice[-08]=SADIE_H08_(Inverted_Y)"
	SET "choice[-09]=SADIE_H09_(Inverted_Y)"
	SET "choice[-10]=SADIE_H10_(Inverted_Y)"
	SET "choice[-11]=SADIE_H11_(Inverted_Y)"
	SET "choice[-12]=SADIE_H12_(Inverted_Y)"
	SET "choice[-13]=SADIE_H13_(Inverted_Y)"
	SET "choice[-14]=SADIE_H14_(Inverted_Y)"
	SET "choice[-15]=SADIE_H15_(Inverted_Y)"
	SET "choice[-16]=SADIE_H16_(Inverted_Y)"
	SET "choice[-17]=SADIE_H17_(Inverted_Y)"
	SET "choice[-18]=SADIE_H18_(Inverted_Y)"
	SET "choice[-19]=SADIE_H19_(Inverted_Y)"
	SET "choice[-20]=SADIE_H20_(Inverted_Y)"
	SET "choice[02]=irc02"
	SET "choice[03]=irc03"
	SET "choice[04]=irc04"
	SET "choice[05]=irc05"
	SET "choice[06]=irc06"
	SET "choice[07]=irc07"
	SET "choice[08]=irc08"
	SET "choice[09]=irc09"
	SET "choice[12]=irc12"
	SET "choice[13]=irc13"
	SET "choice[14]=irc14"
	SET "choice[15]=irc15"
	SET "choice[16]=irc16"
	SET "choice[17]=irc17"
	SET "choice[18]=irc18"
	SET "choice[20]=irc20"
	SET "choice[21]=irc21"
	SET "choice[22]=irc22"
	SET "choice[23]=irc23"
	SET "choice[25]=irc25"
	SET "choice[26]=irc26"
	SET "choice[28]=irc28"
	SET "choice[29]=irc29"
	SET "choice[30]=irc30"
	SET "choice[31]=irc31"
	SET "choice[32]=irc32"
	SET "choice[33]=irc33"
	SET "choice[34]=irc34"
	SET "choice[37]=irc37"
	SET "choice[38]=irc38"
	SET "choice[39]=irc39"
	SET "choice[40]=irc40"
	SET "choice[41]=irc41"
	SET "choice[42]=irc42"
	SET "choice[43]=irc43"
	SET "choice[44]=irc44"
	SET "choice[45]=irc45"
	SET "choice[46]=irc46"
	SET "choice[47]=irc47"
	SET "choice[48]=irc48"
	SET "choice[49]=irc49"
	SET "choice[50]=irc50"
	SET "choice[51]=irc51"
	SET "choice[52]=irc52"
	SET "choice[53]=irc53"
	SET "choice[54]=irc54"
	SET "choice[55]=irc55"
	SET "choice[56]=irc56"
	SET "choice[57]=irc57"
	SET "choice[58]=irc58"
	SET "choice[59]=irc59"
	ECHO    [1m0[0m [92moal_dflt[0m                 [1m02[0m [1;36mirc02[0m                [1m28[0m [1;36mirc28[0m                [1m51[0m [1;36mirc51[0m
	ECHO  [1m+01[0m [36mSADIE_D01[0m                [1m03[0m [1;36mirc03[0m                [1m29[0m [1;36mirc29[0m                [1m52[0m [1;36mirc52[0m
	ECHO  [1m+02[0m [36mSADIE_D02[0m                [1m04[0m [1;36mirc04[0m                [1m30[0m [1;36mirc30[0m                [1m53[0m [1;36mirc53[0m
	ECHO  [1m+03[0m [36mSADIE_H03[0m                [1m05[0m [1;36mirc05[0m                [1m31[0m [1;36mirc31[0m                [1m54[0m [1;36mirc54[0m
	ECHO  [1m+04[0m [36mSADIE_H04[0m                [1m06[0m [1;36mirc06[0m                [1m32[0m [1;36mirc32[0m                [1m55[0m [1;36mirc55[0m
	ECHO  [1m+05[0m [36mSADIE_H05[0m                [1m07[0m [1;36mirc07[0m                [1m33[0m [1;36mirc33[0m                [1m56[0m [1;36mirc56[0m
	ECHO  [1m+06[0m [36mSADIE_H06[0m                [1m08[0m [1;36mirc08[0m                [1m34[0m [1;36mirc34[0m                [1m57[0m [1;36mirc57[0m
	ECHO  [1m+07[0m [36mSADIE_H07[0m                [1m09[0m [1;36mirc09[0m                [1m37[0m [1;36mirc37[0m                [1m58[0m [1;36mirc58[0m
	ECHO  [1m+08[0m [36mSADIE_H08[0m                [1m12[0m [1;36mirc12[0m                [1m38[0m [1;36mirc38[0m                [1m59[0m [1;36mirc59[0m
	ECHO  [1m+09[0m [36mSADIE_H09[0m                [1m13[0m [1;36mirc13[0m                [1m39[0m [1;36mirc39[0m                
	ECHO  [1m+10[0m [36mSADIE_H10[0m                [1m14[0m [1;36mirc14[0m                [1m40[0m [1;36mirc40[0m                
	ECHO  [1m+11[0m [36mSADIE_H11[0m                [1m15[0m [1;36mirc15[0m                [1m41[0m [1;36mirc41[0m                
	ECHO  [1m+12[0m [36mSADIE_H12[0m                [1m16[0m [1;36mirc16[0m                [1m42[0m [1;36mirc42[0m                
	ECHO  [1m+13[0m [36mSADIE_H13[0m                [1m17[0m [1;36mirc17[0m                [1m43[0m [1;36mirc43[0m                
	ECHO  [1m+14[0m [36mSADIE_H14[0m                [1m18[0m [1;36mirc18[0m                [1m44[0m [1;36mirc44[0m                
	ECHO  [1m+15[0m [36mSADIE_H15[0m                [1m20[0m [1;36mirc20[0m                [1m45[0m [1;36mirc45[0m                
	ECHO  [1m+16[0m [36mSADIE_H16[0m                [1m21[0m [1;36mirc21[0m                [1m46[0m [1;36mirc46[0m                
	ECHO  [1m+17[0m [36mSADIE_H17[0m                [1m22[0m [1;36mirc22[0m                [1m47[0m [1;36mirc47[0m                
	ECHO  [1m+18[0m [36mSADIE_H18[0m                [1m23[0m [1;36mirc23[0m                [1m48[0m [1;36mirc48[0m                
	ECHO  [1m+19[0m [36mSADIE_H19[0m                [1m25[0m [1;36mirc25[0m                [1m49[0m [1;36mirc49[0m                
	ECHO  [1m+20[0m [36mSADIE_H20[0m                [1m26[0m [1;36mirc26[0m                [1m50[0m [1;36mirc50[0m                
	CALL :ResetWorkingDirectory
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90mTo find the best one for you, press[0m [1mC[0m [90m+[0m [0mEnter[0m [90mto to compare audio samples in your browser.[0m"
	CALL :PromptInput HRTFSelect "[90mTo change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m. [1m"
	CALL :PrintAndLog "[0m"
	if "!HRTFSelect!"=="" (
		GOTO :MainScreen
		) ELSE (
		if /I "!HRTFSelect!"=="C" (
			CALL :BrowseURL "!HRTFComparisonURL!"
			GOTO :SelectHRTF
			)
		)
	SET HRTFSelect=!HRTFSelect: =!
	for /F "tokens=2 delims==" %%s in ('set choice[') do (
		::ID
		if !choice[%HRTFSelect%]!==%%s (
			SET FetchedHRTF=""
			SET HRTF=%%s
			CALL :SavePreferenceSetting "HRTF" "!HRTF!"
			)
		)
	EXIT /B

:SelectFormat
	cls
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m Currently selected format:[0m [1m!BitDepth!-bit, !SampleRate! Hz[0m"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[1m 1[0m [92m16 bit, 44100 Hz[0m"
	CALL :PrintAndLog "[1m 2[0m [92m16 bit, 48000 Hz[0m"
	CALL :PrintAndLog "[1m 3[0m [92m16 bit, 96000 Hz[0m"
	CALL :PrintAndLog "[1m 4[0m [96m24 bit, 44100 Hz[0m"
	CALL :PrintAndLog "[1m 5[0m [96m24 bit, 48000 Hz[0m"
	CALL :PrintAndLog "[1m 6[0m [96m24 bit, 96000 Hz[0m"
	CALL :PrintAndLog "[1m 7[0m [36m32 bit, 44100 Hz[0m"
	CALL :PrintAndLog "[1m 8[0m [36m32 bit, 48000 Hz[0m"
	CALL :PrintAndLog "[1m 9[0m [36m32 bit, 96000 Hz[0m"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m To change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m. [1m"
	CALL :PromptInput SampleRateSelect
	if "!SampleRateSelect!"=="" (
		GOTO :MainScreen
		)
	SET SampleRateSelect=!SampleRateSelect: =!
	IF !SampleRateSelect!==1 (
		SET SampleRate=44100
		SET BitDepth=16
		) ELSE (
		IF !SampleRateSelect!==2 (
			SET SampleRate=48000
			SET BitDepth=16
			) ELSE (
			IF !SampleRateSelect!==3 (
				SET SampleRate=96000
				SET BitDepth=16
				) ELSE (
				IF !SampleRateSelect!==4 (
					SET SampleRate=44100
					SET BitDepth=24
					) ELSE (
					IF !SampleRateSelect!==5 (
						SET SampleRate=48000
						SET BitDepth=24
						) ELSE (
						IF !SampleRateSelect!==6 (
							SET SampleRate=96000
							SET BitDepth=24
							) ELSE (
							IF !SampleRateSelect!==7 (
								SET SampleRate=44100
								SET BitDepth=32
								) ELSE (
								IF !SampleRateSelect!==8 (
									SET SampleRate=48000
									SET BitDepth=32
									) ELSE (
									IF !SampleRateSelect!==9 (
										SET SampleRate=96000
										SET BitDepth=32
										) ELSE (
										GOTO :SelectFormat 
										)
									)
								)
							)
						)
					)
				)
			)
		)
	CALL :SavePreferenceSetting "SampleRate" "!SampleRate!"
	CALL :SavePreferenceSetting "BitDepth" "!BitDepth!"
	EXIT /B

:SelectDatabaseBranch
	cls
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m Currently selected database branch:[0m [1m!DatabaseBranch![0m"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[1m 0[0m [93mOffline - Use built-in configuration profiles instead of downloading up-to-date ones.[0m"
	CALL :PrintAndLog "[1m 1[0m [36mStable  - Use only manually tested profiles (few). Unverified (most) will default to offline auto-detection.[0m"
	CALL :PrintAndLog "[1m 2[0m [92mBeta    - Use profiles from trusted sources/that will likely work, though no guarantees. (RECOMMENDED)[0m"
	CALL :PrintAndLog "[1m 3[0m [91mNightly - Use the most recent database profiles, including unverified ones. (NOT RECOMMENDED)[0m"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m To change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m. [1m"
	CALL :PromptInput DatabaseBranchSelect
	if "!DatabaseBranchSelect!"=="" (
		GOTO :MainScreen
		) ELSE (
		SET DatabaseBranchSelect=!DatabaseBranchSelect: =!
		IF !DatabaseBranchSelect!==0 (
			SET DatabaseBranch=Offline
			SET DatabaseDownloaded=
			) ELSE (
			IF !DatabaseBranchSelect!==1 (
				SET DatabaseBranch=Stable
				SET DatabaseDownloaded=False
				) ELSE (
				IF !DatabaseBranchSelect!==2 (
					SET DatabaseBranch=Beta
					SET DatabaseDownloaded=False
					) ELSE (
					IF !DatabaseBranchSelect!==3 (
						SET DatabaseBranch=Nightly
						SET DatabaseDownloaded=False
						) ELSE (
						GOTO :SelectDatabaseBranch 
						)
					)
				)
			)
		)
	CALL :SetDatabasePath
	CALL :SetURLs
	CALL :SavePreferenceSetting "DatabaseBranch" "!DatabaseBranch!"
	SET DatabaseDownloaded=False
	IF DEFINED ExecutableFilePath (GOTO :Reinstall)
	EXIT /B

:SelectConfiguration
	cls
	CALL :PrintAndLog ""
	IF DEFINED Configuration (IF NOT !Configuration!=="" (CALL :PrintAndLog "[90m Currently selected configuration:[0m [1m!Configuration![0m"))
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[1m 1[0m [36mHeadphone Spatial Audio[0m"
	CALL :PrintAndLog "[1m 2[0m [1;32mHeadphone Virtual Surround[0m"
	CALL :PrintAndLog "[1m 3[0m [1;35mSpeaker Spatial Audio[0m Stereo"
	CALL :PrintAndLog "[1m 4[0m [1;35mSpeaker Spatial Audio[0m 3D7.1"
	CALL :PrintAndLog "[1m 5[0m [33mSpeaker Virtual Surround[0m"
	CALL :PrintAndLog "[1m 6[0m [90mSpeaker Surround[0m Square"
	CALL :PrintAndLog "[1m 7[0m [90mSpeaker Surround[0m Rectangle"
	CALL :PrintAndLog "[1m 8[0m [90mSpeaker Surround[0m Hexagon"
	CALL :PrintAndLog "[1m 9[0m [90mSpeaker Surround[0m ITU5.1"
	CALL :PrintAndLog "[1m 0[0m [90mSpeaker Surround[0m ITU5.1-NoCenter"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90mTo change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m. [1m"
	CALL :PromptInput ConfigurationSelect
	if "!ConfigurationSelect!"=="" (
		GOTO :MainScreen
		)
	SET ConfigurationSelect=!ConfigurationSelect: =!
	IF !ConfigurationSelect!==1 (
		SET "Configuration=Headphone Spatial Audio"
		SET FetchedConfiguration=""
		) ELSE (
		IF !ConfigurationSelect!==2 (
			SET "Configuration=Headphone Virtual Surround"
			SET FetchedConfiguration=""
			) ELSE (
			IF !ConfigurationSelect!==3 (
				SET "Configuration=Speaker Spatial Audio,Stereo"
				SET FetchedConfiguration=""
				) ELSE (
				IF !ConfigurationSelect!==4 (
					SET "Configuration=Speaker Spatial Audio,3D7.1"
					SET FetchedConfiguration=""
					CALL :BrowseURL "!OpenALSoft3D7.1URL!"
					) ELSE (
					IF !ConfigurationSelect!==5 (
						SET "Configuration=Speaker Virtual Surround"
						SET FetchedConfiguration=""
						) ELSE (
						IF !ConfigurationSelect!==6 (
							SET "Configuration=Speaker Surround,Square"
							SET FetchedConfiguration=""
							) ELSE (
							IF !ConfigurationSelect!==7 (
								SET "Configuration=Speaker Surround,Rectangle"
								SET FetchedConfiguration=""
								) ELSE (
								IF !ConfigurationSelect!==8 (
									SET "Configuration=Speaker Surround,Hexagon"
									SET FetchedConfiguration=""
									) ELSE (
									IF !ConfigurationSelect!==9 (
										SET "Configuration=Speaker Surround,ITU5.1"
										SET FetchedConfiguration=""
										) ELSE (
										IF !ConfigurationSelect!==0 (
											SET "Configuration=Speaker Surround,ITU5.1-NoCenter"
											SET FetchedConfiguration=""
											) ELSE (
											GOTO :SelectConfiguration
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)
	IF NOT "!Configuration!"=="!Configuration:Speaker Surround,=!" (CALL :SavePreferenceSetting "Configuration" "!Configuration!")
	EXIT /B

::Functions - Configuration

:RegisterDirectSound
	if !OSVersion! GEQ 628102 (
		CALL :PrintActionAndLog "[0m Registering DirectSound references ([1mdsound.dll[0m)..."
		SET RegistryDirectSoundCount=1
		SET RegistryHive[0]=HKEY_CURRENT_USER
		SET RegistryHive[1]=HKEY_LOCAL_MACHINE
		for /F "tokens=2 delims==" %%H in ('set RegistryHive[') do (
			SET RegistryClass[0]=Classes
			SET RegistryClass[1]=Classes\WOW6432Node
			for /F "tokens=2 delims==" %%C in ('set RegistryClass[') do (
				SET RegistryID[0]={3901CC3F-84B5-4FA4-BA35-AA8172B8A09B}
				SET RegistryID[1]={47D4D946-62E8-11CF-93BC-444553540000}
				for /F "tokens=2 delims==" %%I in ('set RegistryID[') do (
					IF NOT exist "!RegistryBackup-DirectSound!-!RegistryDirectSoundCount!.reg" (
						CALL :CreateFolder "User\Backup\Common"
						CALL :RegistryKeyExport "%%H\SOFTWARE\%%C\CLSID\%%I\InprocServer32" "!RegistryBackup-DirectSound!-!RegistryDirectSoundCount!.reg"
						)
					CALL :RegistryKeyAdd "%%H\SOFTWARE\%%C\CLSID\%%I\InprocServer32" "" "REG_SZ" "dsound.dll"
					CALL :Log ""
					CALL :Log "!RegistryDirectSoundCount!/8 - %%H\SOFTWARE\%%C\CLSID\%%I\InprocServer32"
					SET /a RegistryDirectSoundCount=RegistryDirectSoundCount+1
					)
				)
			)
		SET RegistryDirectSoundCount=1
		SET RegistryHive[0]=HKEY_CURRENT_USER
		SET RegistryHive[1]=HKEY_LOCAL_MACHINE
		for /F "tokens=2 delims==" %%H in ('set RegistryHive[') do (
			SET RegistryClass[0]=Classes
			SET RegistryClass[1]=Classes\WOW6432Node
			for /F "tokens=2 delims==" %%C in ('set RegistryClass[') do (
				SET RegistryID[0]={3901CC3F-84B5-4FA4-BA35-AA8172B8A09B}
				SET RegistryID[1]={47D4D946-62E8-11CF-93BC-444553540000}
				for /F "tokens=2 delims==" %%I in ('set RegistryID[') do (
					reg query %%H\SOFTWARE\%%C\CLSID\%%I\InprocServer32 1>> !LogFilePath! 2>&1
					CALL :Log ""
					CALL :Log "!RegistryDirectSoundCount!/8 - %%H\SOFTWARE\%%C\CLSID\%%I\InprocServer32"
					SET /a RegistryDirectSoundCount=RegistryDirectSoundCount+1
					)
				)
			)
		CALL :ErrorCheck
		)
	EXIT /B
::Required by DSOAL on Windows 8+ for games to load dsound.dll from their own folder.
::Source: https://www.indirectsound.com/registryIssues.html

:GetDevice
	CALL :StopAllSounds
	CALL :PlayBackgroundMusic
	:GetDeviceCheckPlayback
	CALL :CheckIfRunning "openmpt123.exe"
	IF NOT "!ERRORLEVEL!"=="0" (
		CALL :CheckElevatedPrivileges
		IF !ERRORLEVEL! EQU 0 (
			CALL :ForceCloseIfRunning "VolumeLinker!WindowsArchitectureBits!.exe"
			CALL :CloseIfRunning "VBCABLE_AsioBridge.exe"
			CALL :RestartAudioEndpointBuilder
			) ELSE (
			IF NOT "!RestartAsAdminWarned!"=="True" (
				CALL :PrintAndLog "[91m Audio service isn't running.[0m"
				CALL :PrintAndLog "[96m Close this window then right-click[0m [1m!ScriptName!.cmd[0m [96m>[0m [1mRun as administrator[0m [96monce to start...[0m"
				pause
				exit
REM				CALL :PrintAndLog "[96m Press any key to Run as administrator to restart the service.[0m"
REM				pause
REM				GOTO :Elevate
				)
			SET RestartAsAdminWarned=True
			)
		GOTO :GetDeviceCheckPlayback
		) ELSE (
		TIMEOUT /T 3 >NUL
		)

	CALL :SetBackupPaths
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /scomma "User\AudioDevices.csv"



	::Virtual Audio Device
	CALL :DatabaseSearch "Device Name" "!VirtualAudioDeviceName!"			"User\AudioDevices.csv"						"User\AudioDevices-Matching.csv"
	CALL :DatabaseSearch "Type" "Device"									"User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+Device.csv"
	CALL :DatabaseSearch "Direction" "Render"								"User\AudioDevices-Matching+Device.csv"		"User\AudioDevices-Matching+Render.csv"
	CALL :DatabaseSelect 19					"User\AudioDevices-Matching+Render.csv"		"User\AudioDevices-Matching+RenderCLFID.csv" "VirtualAudioDeviceRenderCLFID"
	CALL :DatabaseSearch "Direction" "Capture"								"User\AudioDevices-Matching+Device.csv"		"User\AudioDevices-Matching+Capture.csv"
	CALL :DatabaseSelect 19					"User\AudioDevices-Matching+Capture.csv"	"User\AudioDevices-Matching+CaptureCLFID.csv" "VirtualAudioDeviceCaptureCLFID"
	CALL :DatabaseSelect "Name"				"User\AudioDevices-Matching+Capture.csv"	"User\AudioDevices-Matching+Name.csv" "VirtualAudioDeviceCaptureName"
	CALL :DatabaseSelect "Registry Key"     "User\AudioDevices-Matching+Render.csv"		"User\AudioDevices-Matching+DeviceID.csv" "VirtualAudioDeviceID"
	IF NOT "!VirtualAudioDeviceID!"=="" (set VirtualAudioDeviceID=!VirtualAudioDeviceID:HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\=!)

	::Sound Card
	CALL :DatabaseSearch "Default" "Render" 								"User\AudioDevices.csv"						"User\AudioDevices-Matching.csv"
	CALL :DatabaseSelect "Device Name" 		"User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+DeviceName.csv" "DefaultPlaybackDeviceCaptureName"
	CALL :DatabaseSelect "Device Name" 		"User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+DeviceName.csv" "DefaultPlaybackDeviceName"
	CALL :DatabaseSelect 19					"User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+RenderCLFID.csv" "DefaultPlaybackDeviceRenderCLFID"


	CALL :EmptyDevice


	CALL :DatabaseSelect "Registry Key"     "User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+DeviceID.csv" "DefaultPlaybackDeviceID"
	SET "DefaultPlaybackDeviceID=!DefaultPlaybackDeviceID:HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\=!"

	CALL :DatabaseSearch "Device Name" "!DefaultPlaybackDeviceCaptureName!"	"User\AudioDevices.csv"						"User\AudioDevices-Matching.csv"
	CALL :DatabaseSearch "Type" "Device"									"User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+Device.csv"
	CALL :DatabaseSearch "Direction" "Capture"								"User\AudioDevices-Matching+Device.csv"		"User\AudioDevices-Matching+Capture.csv"
	CALL :DatabaseSearch "Device State" "Active"							"User\AudioDevices-Matching+Capture.csv"	"User\AudioDevices-Matching+Active.csv"
	CALL :DatabaseSelect 19					"User\AudioDevices-Matching+Active.csv"		"User\AudioDevices-Matching+CaptureCLFID.csv" "DefaultPlaybackDeviceCaptureCLFID"

	SET DefaultPlaybackDeviceChannelCount=
	SET SpeakerConfigurationChannelCount=Unknown
	SET SpeakerConfigurationChannelCountName=Unknown
	CALL :DatabaseSearch "Name" "libopenmpt"								"User\AudioDevices.csv"						"User\AudioDevices+libopenmpt.csv"
	CALL :DatabaseSelect "Channels Count"   "User\AudioDevices+libopenmpt.csv"			"User\AudioDevices+ChannelsCount.csv" "DefaultPlaybackDeviceChannelCount"
	IF !DefaultPlaybackDeviceChannelCount! EQU 8 (
		SET SpeakerConfigurationChannelCount=7.1
		SET SpeakerConfigurationChannelCountName=7.1 Surround
		)
	IF !DefaultPlaybackDeviceChannelCount! EQU 6 (
		SET SpeakerConfigurationChannelCount=5.1
		SET SpeakerConfigurationChannelCountName=5.1 Surround
		)
	IF !DefaultPlaybackDeviceChannelCount! EQU 4 (
		SET SpeakerConfigurationChannelCount=4.0
		SET SpeakerConfigurationChannelCountName=Quadraphonic
		)
	IF !DefaultPlaybackDeviceChannelCount! EQU 2 (
		SET SpeakerConfigurationChannelCount=2.0
		SET SpeakerConfigurationChannelCountName=Stereo
		)
	IF !DefaultPlaybackDeviceChannelCount! EQU 1 (
		SET SpeakerConfigurationChannelCount=1.0
		SET SpeakerConfigurationChannelCountName=Mono
		)

	::Recording
	CALL :DatabaseSearch "Default" "Capture" 								"User\AudioDevices.csv"						"User\AudioDevices-Matching.csv"
	CALL :DatabaseSelect "Item ID"     	  	"User\AudioDevices-Matching.csv"			"User\AudioDevices-Matching+CaptureItemID.csv" "DefaultRecordingDeviceItemID"

	for /F "tokens=3,1 delims=\" %%a in ("!DefaultPlaybackDeviceRenderCLFID!") do SET "DefaultPlaybackDeviceNameWASAPI=%%b (%%a)"
	IF NOT "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (
		SET DefaultSoundCardChannelCount=!DefaultPlaybackDeviceChannelCount!
		for /F "tokens=3,1 delims=\" %%a in ("!DefaultPlaybackDeviceRenderCLFID!") do SET "DefaultSoundCardNameWASAPI=%%b (%%a)"
		CALL :SavePreferenceSetting "DefaultSoundCardNameWASAPI" "!DefaultSoundCardNameWASAPI!"
		CALL :SavePreferenceSetting "DefaultSoundCardName" "!DefaultPlaybackDeviceName!"
		CALL :SavePreferenceSetting "DefaultSoundCardID" "!DefaultPlaybackDeviceID!"
		CALL :SavePreferenceSetting "DefaultSoundCardID" "!DefaultPlaybackDeviceID!"
		)
	::Cleanup
	if exist "User\*.csv" (del "User\*.csv")
	EXIT /B

:GetASIODevice
	IF DEFINED ASIODevice (
		CALL :RegistryKeyQueryValue "!SystemASIORegistryPath!\!ASIODevice!" "Description" "ASIODeviceName"
		CALL :RegistryKeyQueryValue "!SystemASIORegistryPath!\!ASIODevice!" "CLSID" "ASIODeviceCLSID"
		)
	EXIT /B

:SetAudioDeviceTypes
	SET AudioDeviceType[0]=DefaultPlaybackDevice
	SET AudioDeviceType[1]=VirtualAudioDevice
	EXIT /B

:ToggleMicrosoftSpatialSound
	if !OSVersion! GEQ !Windows10OSVersion! (
		IF NOT DEFINED %1 (
			IF "!FetchedRenderer!"=="HeSuVi" (
				if !DefaultPlaybackDeviceChannelCount! LEQ 2 (
					CALL :DisableMicrosoftSpatialSound
					)
				) ELSE (
				IF "!FetchedWrapper!"=="Crystal Mixer" (
					if !DefaultPlaybackDeviceChannelCount! LEQ 2 (
						CALL :DisableMicrosoftSpatialSound
						)
					) ELSE (
					CALL :DisableMicrosoftSpatialSound
					)
				)
			) ELSE (
			CALL :EnableMicrosoftSpatialSound %1
			)
		)
	EXIT /B

:EnableMicrosoftSpatialSound
	CALL :PrintActionAndLog "[0m - Microsoft Spatial Sound:          "
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetSpatial "DefaultRenderDevice" %1
	CALL :PrintAndLog "[36mEnabled[0m"
	EXIT /B

:DisableMicrosoftSpatialSound
	CALL :PrintActionAndLog "[0m - Microsoft Spatial Sound:          "
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetSpatial "DefaultRenderDevice" ""
	CALL :PrintAndLog "[36mDisabled[0m"
	EXIT /B

:DisableEnhancements
	CALL :PrintActionAndLog "[0m - Enhancements:                     "
	CALL :SetAudioDeviceTypes
	for /F "tokens=2 delims==" %%D in ('set AudioDeviceType[') do (
		REM CALL :GetDevice
		IF NOT exist "!RegistryBackup-Enhancements!" (
			CALL :CreateFolder "User\Backup\Common"
			CALL :RegistryKeyExport "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!" "!RegistryBackup-Enhancements!"
			)
		CALL :RegistryKeySetPermissions "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties"
		CALL :RegistryKeyQuery "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5"
		IF !RegistryKeyQueryErrorLevel! EQU 0 (
			CALL :RegistryKeyAdd "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5" "REG_DWORD" "0x00000001"
			)
		CALL :RegistryKeySetPermissions "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties"
		CALL :RegistryKeyQuery "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5"
		IF !RegistryKeyQueryErrorLevel! EQU 0 (
			CALL :RegistryKeyAdd "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5" "REG_DWORD" "0x00000001"
			)
		)
	CALL :PrintAndLog "[36mDisabled[0m"
	EXIT /B

:EnableEnhancements
	CALL :PrintActionAndLog "[0m - Enhancements:                     "
	CALL :SetAudioDeviceTypes
	for /F "tokens=2 delims==" %%D in ('set AudioDeviceType[') do (
		REM CALL :GetDevice
		IF NOT exist "!RegistryBackup-Enhancements!" (
			CALL :CreateFolder "User\Backup\Common"
			CALL :RegistryKeyExport "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!" "!RegistryBackup-Enhancements!"
			)
		CALL :RegistryKeySetPermissions "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties"
		CALL :RegistryKeyQuery "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5"
		IF !RegistryKeyQueryErrorLevel! EQU 0 (
			CALL :RegistryKeyAdd "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5" "REG_DWORD" "0x00000000"
			)
		CALL :RegistryKeySetPermissions "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties"
		CALL :RegistryKeyQuery "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5"
		IF !RegistryKeyQueryErrorLevel! EQU 0 (
			CALL :RegistryKeyAdd "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},5" "REG_DWORD" "0x00000000"
			)
		)
	CALL :PrintAndLog "[36mEnabled[0m"
	EXIT /B

:DisableCommunicationsDucking
	CALL :PrintActionAndLog "[0m - Communications ducking:           "
	CALL :RegistryKeyAdd "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Multimedia\Audio" "UserDuckingPreference" "REG_DWORD" "0x00000003"
	CALL :PrintAndLog "[36mDisabled[0m"
	EXIT /B

:SetFormat
	CALL :PrintActionAndLog "[0m - Format:                           "
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetDefaultFormat "DefaultRenderDevice" !BitDepth! !SampleRate!
	CALL :PrintAndLog "[1m!BitDepth!-bit, !SampleRate! Hz[0m"
	EXIT /B

:SetVolume
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /GetPercent "DefaultRenderDevice"
	SET /A CurrentVolume=!errorlevel!/10
	IF NOT !CurrentVolume! LEQ %1 ("Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetVolume "DefaultRenderDevice" %1)
	EXIT /B

:SetExclusiveMode
	CALL :PrintActionAndLog "[0m - Exclusive mode:                   "
	IF "!FetchedWrapper!"=="DSOAL" (
		IF "!OpenALSoftVersion!"=="!OpenALSoftVersion:WASAPI=!" (
			SET ExclusiveModeState=Disabled
			CALL :ToggleExclusiveMode 0
			) ELSE (
			SET ExclusiveModeState=Enabled
			CALL :ToggleExclusiveMode 1
			)
		) ELSE (
		SET ExclusiveModeState=Enabled
		CALL :ToggleExclusiveMode 1
		)
	CALL :PrintAndLog "[36m!ExclusiveModeState![0m"
	EXIT /B

:ToggleExclusiveMode
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetAllowExclusive "DefaultRenderDevice" %1
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetExclusivePriority "DefaultRenderDevice" %1
	IF "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (
		"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetAllowExclusive "!DefaultSoundCardName!" %1
		"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetExclusivePriority "!DefaultSoundCardName!" %1
		)
	EXIT /B

:ToggleHeSuVi
	IF NOT DEFINED DefaultPlaybackDeviceID (CALL :GetDevice)
	IF EXIST "!HeSuViExeFullPath!" (
		CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},1"
		if !RegistryKeyQueryErrorLevel! EQU 0 (
			CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},2"
			if !RegistryKeyQueryErrorLevel! EQU 0 (
				IF "!FetchedWrapper!"=="Crystal Mixer" (
					"!HeSuViExeFullPath!" -deactivateeverything 1
					CALL :PrintAndLog " - HeSuVi:                           [36m%2[0m"
					) ELSE (
					"!HeSuViExeFullPath!" -deactivateeverything 0 -virtualization !HRTFHeSuViFile! -autodeactivatevirtualization 1 -virtualizationvolume 100;100;100;100;100;200 -position ;0;0;0 -matrixupmix 2 -connection %1 -globalattenuation 0;0; -equalizer "!EqualizerPresetBrand!\!EqualizerPresetModel!.txt;" -r "!FetchedPrelude!"
					CALL :PrintAndLog " - HeSuVi Virtualization:            [36m%2[0m"
					)
				if "!FetchedRenderer!"=="HeSuVi" (
					CALL :PrintAndLog " - Front Connection Type:            [36m%3 %4[0m"
					CALL :PrintAndLog " - Virtualization Volume Adjustment: [36mReset[0m"
					CALL :PrintAndLog " - Global attenuation:               [36mReset[0m"
					CALL :PrintAndLog " - Speaker Position Adjustment:      [36mReset[0m"
					CALL :PrintAndLog " - Upmix Stereo to 7.1:              [36mDisabled[0m"
					CALL :PrintAndLog " - Upmix 5.1 to 7.1:                 [36mEnabled[0m"
					CALL :PrintAndLog " - Content Format:                   [36mAutomatic[0m"
					)
				)
			)
		)
	EXIT /B

::Functions - Logging & Error report

:ErrorCheck
	SET ErrorCode=!ERRORLEVEL!
	IF !ErrorCode! EQU 0 (
		CALL :ErrorCheckSuccess
		) ELSE (
		IF !ErrorCode! EQU 1638 (
			CALL :ErrorCheckAlreadyInstalled
			EXIT /B
			) ELSE (
			CALL :ErrorCheckFailure
			CALL :PrintAndLog "[93mError level: !ErrorCode![0m"
			CALL :Failure
			CALL :AnonyimizeUserFiles
			)
		)
	EXIT /B

:ErrorCheckSuccess
	CALL :PrintAndLog "[92m [Success][0m"
	EXIT /B

:ErrorCheckFailure
	CALL :PrintAndLog "[91m [Fail][0m"
	EXIT /B

:ErrorCheckWarning
	CALL :PrintAndLog "[93m [Warning][0m"
	EXIT /B

:ErrorCheckAlreadyInstalled
	CALL :PrintAndLog "[92m [Already installed][0m"
	EXIT /B

:Failure
	CALL :PrintAndLog "Logging has been enabled until next successful installation, so please run the script again, or ignore error and continue."
	CALL :SavePreferenceSetting "LogLevel" "Debug"
	IF NOT "!LogLevel!"=="Off" (CALL :ReportLog)
	pause
	EXIT /B

:ReportLog
	CALL :AnonyimizeUserFiles
	CALL :PrintAndLog "If the issue persists, please report the [1mLog.txt[0m from the [1mUser[0m folder to our chat: [1m!SupportURL![0m"
	pause
	EXIT /B

:AnonyimizeUserFiles
	CALL :Anonyimize !LogFilePath!
	CALL :Anonyimize !UserConfigurationFilePath!
	CALL :Anonyimize !UserPreferencesFile!
	EXIT /B 

:Anonyimize
	if exist "%1" (Resources\Tools\sed\sed.exe -i -e s/%USERNAME%/USERNAME/g %1)
	CALL :DeleteFile sed*
	EXIT /B

:JustPrint
	SET "PrintLineStylized=%~1"
	CALL :RemoveLineColor
	if NOT !OSVersion! GEQ !Windows10OSVersion! (
		CALL :RemoveLineColor
		echo !PrintLineStylized!
		) ELSE (
		echo %~1
		)
	EXIT /B

:PrintAndLog
	SET "PrintLineStylized=%~1"
	CALL :RemoveLineColor
	if "!PrintLineStylized!"=="" (
		echo.
		CALL :Log ""
		CALL :Log ""
		) ELSE (
		if !OSVersion! GEQ !Windows10OSVersion! (echo !PrintLineStylized!) ELSE (echo !PrintLinePlain!)
		CALL :Log ""
		CALL :Log "!PrintLinePlain!"
		)
	EXIT /B

:PrintActionAndLog
	SET PrintLineStylized=%~1
	CALL :RemoveLineColor
	if "!PrintLineStylized!"=="" (
		echo.
		CALL :Log ""
		CALL :Log ""
		) ELSE (
		if !OSVersion! GEQ !Windows10OSVersion! (SET /P var=!PrintLineStylized!<NUL) ELSE (SET /P var=!PrintLinePlain!<NUL)
		CALL :Log ""
		CALL :Log "!PrintLinePlain!"
		)
	EXIT /B 0

:RemoveLineColor
	SET PrintLinePlain=!PrintLineStylized:[0m=!
	SET PrintLinePlain=!PrintLinePlain:[1m=!
	SET PrintLinePlain=!PrintLinePlain:[1;1m=!
	SET PrintLinePlain=!PrintLinePlain:[4;1m=!
	SET PrintLinePlain=!PrintLinePlain:[7m=!
	SET PrintLinePlain=!PrintLinePlain:[33m=!
	SET PrintLinePlain=!PrintLinePlain:[36m=!
	SET PrintLinePlain=!PrintLinePlain:[1;36m=!
	SET PrintLinePlain=!PrintLinePlain:[1;42m=!
	SET PrintLinePlain=!PrintLinePlain:[1;43m=!
	SET PrintLinePlain=!PrintLinePlain:[1;45m=!
	SET PrintLinePlain=!PrintLinePlain:[44m=!
	SET PrintLinePlain=!PrintLinePlain:[90m=!
	SET PrintLinePlain=!PrintLinePlain:[91m=!
	SET PrintLinePlain=!PrintLinePlain:[92m=!
	SET PrintLinePlain=!PrintLinePlain:[93m=!
	SET PrintLinePlain=!PrintLinePlain:[95m=!
	SET PrintLinePlain=!PrintLinePlain:[1;95m=!
	SET PrintLinePlain=!PrintLinePlain:[96m=!
	EXIT /B

:Log
	IF NOT "!LogLevel!"=="Off" (
		CALL :ResetWorkingDirectory
		SET "PrintLineStylized=%~1"
		CALL :RemoveLineColor
		if "!PrintLineStylized!"=="" (
			IF NOT "!LogLevel!"=="Info" (
				echo.>>!LogFilePath!
				)
			) ELSE (
			IF NOT "!LogLevel!"=="Info" (
				echo !PrintLinePlain!>>!LogFilePath!
				)
			)
		)
	EXIT /B

:ShareResults
	CALL :SavePreferenceSetting "LogLevel" "Off"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m Press[0m Enter [90mto run the[0m Executable ([1m!ExecutableFile![0m)[90m.[0m"
	CALL :PrintAndLog "[90m Press[0m [1mS[0m [90m+[0m Enter[90m to also[0m [1mShare[0m [90myour configuration and results with the community database after testing.[0m"
	SET MenuSelection=
	CALL :PromptInput MenuSelection
	CALL :StopAllSounds
	if /I "!MenuSelection!"=="S" (
		IF !FetchedBackend!=="" (SET "FetchedBackend=[Unknown]")
		IF "!FetchedWrapper!"=="A3D-Live" (SET "FetchedWrapper=A3D-Live!")
		IF "!FetchedRenderer!"=="X3DAudio HRTF" (SET "BitDepth=16")
		if !FetchedConfiguration!=="" (set FetchedConfiguration=!Configuration!)
		IF "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (IF DEFINED DefaultSoundCardName (SET "DefaultPlaybackDeviceName=!DefaultSoundCardName!, !VirtualAudioDeviceName!"))
		SET "FormURLQueryNewConfiguration=https://airtable.com/shrDpNmekxxwpAyQ1?prefill_Title=!FetchedTitle!^&prefill_Title+(add+new)=!FetchedTitle!^&prefill_Type=Base+game^&prefill_Platform=Windows,!WindowsVersion!,!WindowsArchitecturexBits!^&prefill_Backend=!FetchedBackend!^&prefill_Wrapper=!FetchedWrapper!^&prefill_Wrapper+version=!FetchedWrapperVersion!^&prefill_Renderer=!FetchedRenderer!^&prefill_Renderer+version=!FetchedRendererVersion!^&prefill_Configuration=!FetchedConfiguration!,!SpeakerConfigurationChannelCount!^&prefill_Path=!ExecutableFilePath!^&prefill_Advanced=true^&prefill_Installer+version=!ScriptVersion!^&prefill_HRTF=!FetchedHRTF!^&prefill_Sample+rate=!SampleRate!+Hz^&prefill_Bit+depth=!BitDepth!-bit^&prefill_Steam+ID=!SteamID!^&prefill_Playback+device=!DefaultPlaybackDeviceName!^&prefill_Hardware+output=!EqualizerPreset!^&prefill_alsoft.ini=!ALSoftINI!^&prefill_dsound.ini=!DSoundINI!^&prefill_Username=Anonymous"
		SET "FormURLQueryExistingConfiguration=https://airtable.com/shrQApsbMsUVMHjt4?prefill_Profile=rec!FetchedBaseVersion!^&prefill_Username=Anonymous^&prefill_Advanced=true^&prefill_Installer+version=!ScriptVersion!^&prefill_Path=!ExecutableFilePath!^&prefill_Steam+ID=!SteamID!^&prefill_HRTF=!FetchedHRTF!^&prefill_Sample+rate=!SampleRate!+Hz^&prefill_Bit+depth=!BitDepth!-bit^&prefill_Playback+device=!DefaultPlaybackDeviceName!^&prefill_Hardware+output=!EqualizerPreset!^&prefill_alsoft.ini=!ALSoftINI!^&prefill_dsound.ini=!DSoundINI!"
		IF DEFINED FetchedBaseVersion (
			IF NOT !FetchedBaseVersion!=="" (
				SET "FormURLQuery=!FormURLQueryExistingConfiguration!"
				) ELSE (
				SET "FormURLQuery=!FormURLQueryNewConfiguration!"
				)
			) ELSE (
			SET "FormURLQuery=!FormURLQueryNewConfiguration!"
			)
		SET "FormURLQuery=!FormURLQuery:%USERNAME%=USERNAME!"
		SET "FormURLQuery=!FormURLQuery:+&+=+%%26+!"
		SET "FormURLQuery=!FormURLQuery: =+!"
		SET "FormURLQuery=!FormURLQuery:"=!"
		CALL :BrowseURL "!FormURLQuery!"
		)

	::Run input
	CD /D "!ExecutableFolderPath!"
	if exist "!InputFilePath!\*" (
		start "" "!ExecutableFilePath!"
		CALL :ResetWorkingDirectory
		CALL :Log ""
		CALL :Log "Launching !ExecutableFilePath!"
		) ELSE (
		if exist "!USERPROFILE!\Desktop\!ShortcutFilename! MetaAudio.lnk" (
			start "" "!USERPROFILE!\Desktop\!ShortcutFilename! MetaAudio.lnk"
			CALL :ResetWorkingDirectory
			CALL :Log ""
			CALL :Log "Launching !USERPROFILE!\Desktop\!ShortcutFilename! MetaAudio.lnk"
			) ELSE (
			IF DEFINED MetaAudioShortcutFilePath[1] (
				start "" "!MetaAudioShortcutFilePath[1]!"
				CALL :ResetWorkingDirectory
				CALL :Log ""
				CALL :Log "Launching !MetaAudioShortcutFilePath[1]!"
				) ELSE (
				if exist "%tmp%\InputShortcut.url" (
					start "" "%tmp%\InputShortcut.url"
					CALL :DeleteFile "%tmp%\InputShortcut.url"
					) ELSE (
					start "" "!InputFilePath!"
					CALL :ResetWorkingDirectory
					CALL :Log ""
					CALL :Log "Launching !InputFilePath!"
					)
				)
			)
		)
	CALL :AnonyimizeUserFiles
	exit

::Functions - Update variables

:SetPaths
	IF DEFINED ExecutableFilePath (
		SET ExecutableFilePath=!ExecutableFilePath:/=\!
		SET ExecutableFilePath=!ExecutableFilePath:"=!
		)
	For %%A in ("!ExecutableFilePath!") do (
	    SET "ExecutableFolderPath=%%~dpA"
	    IF !ExecutableFolderPath:~-1!==\ SET ExecutableFolderPath=!ExecutableFolderPath:~0,-1!
	    SET "ExecutableFilename=%%~nA"
	    SET "ExecutableFile=%%~nxA"
	    SET "ExecutableExtension=%%~xA"
	    )
	for %%a in ("!ExecutableFilePath!") do for %%b in ("%%~dpa\.") do (
		SET "ExecutableParentFolder=%%~nxb"
		IF NOT DEFINED GameSteamTitle (
			IF /I not "!ExecutableParentFolder!"=="Binaries" (
				IF /I not "!ExecutableParentFolder!"=="System" (
					IF /I not "!ExecutableParentFolder!"=="Win32" (
						IF /I not "!ExecutableParentFolder!"=="Win64" (
							SET FetchedTitle=!ExecutableParentFolder!
							) ELSE (
							SET FetchedTitle=!ExecutableFilename!
							)
						)
					)
				)
			)
		)
	FOR %%i IN ("!ExecutableFilePath!") DO IF EXIST %%~si\NUL (
		SET "FetchedTitle=!ExecutableFilename!"
		IF NOT "!DatabaseDownloaded!"=="True" (
			CALL :Fetch Database
			)
		if exist "!DatabaseFile!" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[0m Scanning folders for compatible executables..."







			::Lookup profile in database using ParentFolder\*.exe
			for /f "usebackq delims=" %%G in (`dir /b /S "!ExecutableFilePath!\*.exe"2^>^>!LogFilePath!`) do (
				for %%a in ("%%G") do for %%b in ("%%~dpa\.") do (
					SET "ExecutableParentFolder=%%~nxb"
					)
				CALL :DatabaseSearch "Path" "!ExecutableParentFolder!\\%%~nxG"			"!DatabaseFile!" 							"User\Database-Matching.csv"
				CALL :DatabaseSlice "User\Database-Matching.csv" "User\Database-Matching+TopResult.csv"
				::Filter by Title
				CALL :DatabaseSelect "Title" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Title.csv"
				for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Title.csv) do (
					IF NOT %%i=="" (
						IF NOT "%%i"=="[Unknown]" (
							IF NOT DEFINED GameSteamTitle (
								SET FetchedTitle=%%i
								)
							CALL :PrintAndLog "[0m Path found in the database: [1m!ExecutableParentFolder!\%%~nxG[0m"
							SET ExecutableFilePath=%%G
							CALL :SetPaths
							GOTO :FolderScanFound
							)
						)
					)
				)



			::Lookup profile in database using *.exe
			for /f "usebackq delims=" %%G in (`dir /b /S "!ExecutableFilePath!\*.exe"2^>^>!LogFilePath!`) do (
				for %%a in ("%%G") do for %%b in ("%%~dpa\.") do (
					SET "ExecutableParentFolder=%%~nxb"
					)
				CALL :DatabaseSearch "Executable" "%%~nxG"								"!DatabaseFile!" 							"User\Database-Matching.csv"
				CALL :DatabaseSlice "User\Database-Matching.csv" "User\Database-Matching+TopResult.csv"
				::Filter by Title
				CALL :DatabaseSelect "Title" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Title.csv"
				for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Title.csv) do (
					IF NOT %%i=="" (
						IF NOT "%%i"=="[Unknown]" (
							IF NOT DEFINED GameSteamTitle (
								SET FetchedTitle=%%i
								)
							CALL :PrintAndLog "[0m Executable found in the database: [1m%%~nxG[0m"
							SET ExecutableFilePath=%%G
							CALL :SetPaths
							GOTO :FolderScanFound
							)
						)
					)
				)






			)
		)
	::Search for .exe files down 1 level
	for %%x in ("!ExecutableFolderPath!\*.exe"2^>^>!LogFilePath!`) do (
		IF NOT "%%~nxx"=="unins000.exe" (
			IF NOT "%%~nxx"=="UNWISE.exe" (
				IF NOT "%%~nxx"=="Setup.exe" (
					SET ExecutableFilePath=%%x
					CALL :SetPaths
					GOTO :FolderScanFound
					)
				)
			)
		)
	::Search for .exe files in all sub-folders
	FOR %%i IN ("!ExecutableFilePath!") DO IF EXIST %%~si\NUL (
		for /f "usebackq delims=" %%G in (`dir /b /S "!ExecutableFilePath!\*.exe"2^>^>!LogFilePath!`) do (
			IF NOT "%%~nxG"=="unins000.exe" (
				IF NOT "%%~nxG"=="UNWISE.exe" (
					IF NOT "%%~nxG"=="Setup.exe" (
						SET ExecutableFilePath=%%G
						CALL :SetPaths
						GOTO :FolderScanFound
						)
					)
				)
			)
		)
	:FolderScanFound
	::Cleanup
	CALL :DeleteFile "User\*.csv"
	::Get script parent folder
	for %%a in ("%~dp0") do for %%b in ("%%~dpa\.") do (
		SET "ScriptParentFolder=%%~nxb"
		)
	SET "PortableConfigurationFilePath=!ExecutableFolderPath!\!ScriptName: =!.cfg"
	SET "DSoundINIPath=!ExecutableFolderPath!\dsound.ini"
	EXIT /B

::Functions - Utility

:RestartAudioService
	SET ToggleServiceLabel=%1
	IF DEFINED ToggleServiceLabel (CALL :PrintActionAndLog %1)
	CALL :ToggleService stop audiosrv
	CALL :ToggleService start audiosrv  %2
	EXIT /B

:EmptyDevice
	if !DefaultPlaybackDeviceName!=="" (
		IF NOT "!AudioServiceRestarted!"=="True" (CALL :RestartAudioService)
		SET "AudioServiceRestarted=True"
		GOTO :EmptyDevice
		CALL :PrintAndLog "[91m Default playback device not found.[0m"
		CALL :PrintAndLog "[96m Press any key to open the Sound settings and (right-click your sound card, then)[0m [921mSet as Default Device.[0m"
		pause >NUL
		CALL :SpeakerConfiguration
		)
	EXIT /B

:ToggleService
	net %1 %2 /y 1>> !LogFilePath! 2>&1
	SET ToggleServiceStatus=%3
	IF DEFINED ToggleServiceStatus (CALL :PrintAndLog %3)
	EXIT /B

:RestartAudioEndpointBuilder
	CALL :PrintActionAndLog "[0m Resetting [1mPlayback[0m devices...              "
	CALL :RegistryKeyDelete "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render"
	CALL :ToggleService stop audiosrv
	CALL :ToggleService stop AudioEndpointBuilder
	CALL :ToggleService start audiosrv
	CALL :ToggleService start AudioEndpointBuilder
	CALL :LoadPreferences
	IF DEFINED DefaultSoundCardName (CALL :SetDefaultPlaybackDevice "!DefaultSoundCardName!")
	CALL :GetDevice
	EXIT /B


:SetDefaultPlaybackDevice
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetDefault %1 1
	"Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /SetDefault %1 2
	EXIT /B

:UpdateHRTFHeSuViFilePath
	SET "HRTF=!HRTF:_(Inverted_Y)=!"
	IF EXIST "!HeSuViFolderPath!\hrir\more\!HRTF!.wav" (
		SET HRTFHeSuViFile=more\!HRTF!
		) ELSE (
		SET HRTFHeSuViFile=!HRTF!
		)
	EXIT /B

::Functions - Store and load variables

:SavePreferenceSetting
	IF NOT exist "!UserPreferencesFile!" (echo. 2>"!UserPreferencesFile!") >NUL 2>&1
	CALL :Log ""
	CALL :Log "Saving preference: %~1=%~2"
	CALL :UpdateINI "!UserPreferencesFile!" "Preferences" %1 %2
	CHCP 1252 >NUL
	EXIT /B

:LoadPreferences
	if exist "!UserPreferencesFile!" (
		for /F "tokens=1,2 skip=2 delims==" %%a in (!UserPreferencesFile!) do (
			IF NOT DEFINED Fetched%%a (
				IF NOT !Fetched%%a!=="" (
					SET "%%a=%%b"
					SET Fetched%%a=""
					CALL :Log ""
					CALL :Log "Loaded preference: %%a=%%b"
					)
				) ELSE (
				if !Fetched%%a!=="" (
					SET "%%a=%%b"
					CALL :Log ""
					CALL :Log "Loaded preference: %%a=%%b"
					)
				)
			)
		)
	IF DEFINED EqualizerPresetBrand (IF DEFINED EqualizerPresetModel (SET "EqualizerPreset=!EqualizerPresetBrand! !EqualizerPresetModel!"))
	EXIT /B

:SetFetchedConfigurationVariables
	SET FetchedConfigurationVariables[0]=Title
	SET FetchedConfigurationVariables[1]=Backend
	SET FetchedConfigurationVariables[2]=Wrapper
	SET FetchedConfigurationVariables[3]=WrapperVersion
	SET FetchedConfigurationVariables[4]=Renderer
	SET FetchedConfigurationVariables[5]=RendererVersion
	SET FetchedConfigurationVariables[6]=Configuration
	SET FetchedConfigurationVariables[7]=HRTF
	SET FetchedConfigurationVariables[8]=DatabaseFile
	SET FetchedConfigurationVariables[9]=ShortcutFilename
	SET FetchedConfigurationVariables[10]=InputFilePath
	SET FetchedConfigurationVariables[11]=SteamID
	SET FetchedConfigurationVariables[12]=Base
	SET FetchedConfigurationVariables[13]=BaseVersion
	SET FetchedConfigurationVariables[14]=GuideURL
	SET FetchedConfigurationVariables[15]=Prelude
	SET FetchedConfigurationVariables[16]=BackupPath
	EXIT /B

:MergeConfiguration
	CALL :SetFetchedConfigurationVariables
	for /F "tokens=2 delims==" %%s in ('set FetchedConfigurationVariables[') do (
		IF DEFINED %%s (
			IF NOT !%%s!=="" (
				SET Fetched%%s=!%%s!
				)
			)
		)
	EXIT /B

:ClearConfiguration
	CALL :SetFetchedConfigurationVariables
	for /F "tokens=2 delims==" %%s in ('set FetchedConfigurationVariables[') do (
		IF NOT DEFINED Fetched%%s (
			SET Fetched%%s=""
			)
		)
	EXIT /B

:SaveConfiguration
	CALL :DeleteFile "!UserConfigurationFilePath!"
	CALL :SetFetchedConfigurationVariables
	for /F "tokens=2 delims==" %%s in ('set FetchedConfigurationVariables[') do (
		IF DEFINED Fetched%%s (
			IF NOT !Fetched%%s!=="" (
				IF "%%s"=="HRTF" (
					IF NOT "!%%s!"=="!Fetched%%s!" (
						CALL :SaveConfigurationSetting "%%s" "!Fetched%%s!"
						)
					) ELSE (
					CALL :SaveConfigurationSetting "%%s" "!Fetched%%s!"
					)
				) ELSE (
				IF DEFINED %%s (
					IF NOT !%%s!=="" (
						CALL :SaveConfigurationSetting "%%s" "!%%s!"
						) ELSE (
						CALL :SaveConfigurationSetting "%%s" """"""""
						)
					)
				)
			) ELSE (
			IF DEFINED %%s (
				CALL :SaveConfigurationSetting "%%s" "!%%s!"
				)
			)
		)
	CHCP 1252 >NUL
	EXIT /B

:SaveConfigurationSetting
	IF NOT exist "!UserConfigurationFilePath!" (echo. 2>"!UserConfigurationFilePath!") >NUL 2>&1
	CALL :Log ""
	CALL :Log "Saving configuration: %~1=%~2"
	CALL :UpdateINI "!UserConfigurationFilePath!" "Configuration" %1 %2
	EXIT /B

:LoadConfiguration
	For %%C in ("!UserConfigurationFilePath!") do (
	CD /D "%%~dpC"
		if exist "%%~nxC" (
			for /F "tokens=1,2 skip=2 delims==" %%a in (%%~nxC) do (
				IF NOT %%b=="" (
					SET "%%a=%%b"
					SET "Fetched%%a=%%b"
					CALL :Log ""
					CALL :Log "Loaded configuration: %%a=%%b"
					)
				)
			)
		CALL :ResetWorkingDirectory
		)
	EXIT /B

:Fetch
	CALL :DeleteFile "!UserConfigurationFilePath!"
	CALL :SetDatabasePath
	IF NOT "!DatabaseBranch!"=="Offline" (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[0m Attempting to download online profile %~1 file from[0m [1m!%~1URL![0m..."
		CALL :CreateFolder "User/Database"
		SET pythonioencoding=UTF-8
		"Resources\Tools\Python\python.exe" "Resources\Tools\AirScraper\airscraper.py" !%~1URL! >"!%~1File!" 2>> !LogFilePath!
		if !ERRORLEVEL! == 1 (
			for /f %%F in ("!%~1File!") do set %~1Filesize=%%~zF
			if !%~1Filesize! equ 0 (
				CALL :DeleteFolder "User/Database"
				)
			CALL :PrintAndLog "[91m Failed to download most recent %~1 from !%~1URL! into !%~1File!.[0m"
			CALL :PrintAndLog "[93m Your firewall might be blocking Resources\Tools\Python\python.exe[0m"
			CALL :PrintAndLog "[0m Attempting to download backup from[0m [1m!%~1MirrorURL![0m...[0m"
			CALL :CreateFolder "User/Database"
			"Resources\Tools\curl\curl.exe" --silent  --location "!%~1MirrorURL!"  >"!%~1File!"
			)
		if !ERRORLEVEL! == 1 (
			for /f %%F in ("!%~1File!") do set %~1Filesize=%%~zF
			if !%~1Filesize! equ 0 (CALL :DeleteFolder "User/Database")
			)
		IF NOT exist "!%~1File!" (
			CALL :SetOfflineDatabasePath
			CALL :PrintAndLog "[93m Falling back to built-in %~1 (!%~1File!)...[0m"
			) ELSE (
			for /f %%F in ("!%~1File!") do set %~1Filesize=%%~zF
			if !%~1Filesize! equ 0 (
				CALL :PrintAndLog "[91m Failed to download most recent %~1 from !%~1MirrorURL! into !%~1File!.[0m"
				CALL :PrintAndLog "[93m Your firewall might be blocking Resources\Tools\curl\curl.exe[0m"
				CALL :DeleteFolder "User/Database"
				CALL :OfflineDatabase
				) ELSE (
				CALL :PrintAndLog "[92m %~1 file downloaded into !%~1File!.[0m"
				SET DatabaseDownloaded=True
				)
			)
		CALL :SaveConfiguration
		) ELSE (
		CALL :OfflineDatabase
		)
	EXIT /B






:OfflineDatabase
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[93m Falling back to built-in (outdated) database...[0m"
	if exist "!DatabaseFile!" (
		CALL :PrintAndLog "[92m Database file found.[0m"
		) ELSE (
		CALL :PrintAndLog "[91m Built-in database file not found.[0m"
		CALL :PrintAndLog "[93m Falling back to auto-detection.[0m"
		)
	EXIT /B

:SetDatabasePath
	if "!DatabaseBranch!"=="Offline" (
		CALL :SetOfflineDatabasePath
		) ELSE (
		CALL :SetOnlineDatabasePath
		)
	EXIT /B

:SetOfflineDatabasePath
	SET "DatabaseFile=Resources\Database\Database.csv"
	SET "BaseFile=Resources\Database\Base.csv"
	EXIT /B

:SetOnlineDatabasePath
	SET "DatabaseFile=User\Database\Database.csv"
	SET "BaseFile=User\Database\Base.csv"
	EXIT /B

:ValidateFilename
	SET %~1=!%~1::=!
	SET %~1=!%~1:"=!
	EXIT /B

:UpdateALSoft
	SET "UpdateALSoftSection=%~1"
	SET "UpdateALSoftSection=!UpdateALSoftSection:"=!"
	SET "UpdateALSoftKey=%~2"
	SET "UpdateALSoftKey=!UpdateALSoftKey:"=!"
	SET "UpdateALSoftValue=%~3"
	SET "UpdateALSoftValue=!UpdateALSoftValue:"=!"
	CALL :UpdateINI "!OpenALSoftINIPath!" "!UpdateALSoftSection!" "!UpdateALSoftKey!" "!UpdateALSoftValue!"
	CALL :Log ""
	CALL :Log "!OpenALSoftINIPath! - [!UpdateALSoftSection!] !UpdateALSoftKey!=!UpdateALSoftValue!"
	EXIT /B

:RunSeparately
	For %%A in (%1) do (
		CALL :CloseIfRunning "%%~nxA"
		START /B /SEPARATE "" %* >NUL
		)
	EXIT /B

:ForceCloseIfRunning
	CALL :CloseIfRunning %1
	CALL :CheckIfRunning %1
	if "!ProcessIsRunning!"=="True" (
		CALL :Log ""
		CALL :Log "Force closing %~1..."
		taskkill /im %1 /T /F 1>> !LogFilePath! 2>&1
		wmic process where name=%1 call terminate 1>> !LogFilePath! 2>&1
		)
	EXIT /B 0

:CloseIfRunning
	CALL :CheckIfRunning %1
	if "!ProcessIsRunning!"=="True" (
		CALL :Log ""
		CALL :Log "Closing %~1..."
		taskkill /im %1 1>> !LogFilePath! 2>&1
		)
	EXIT /B 0

:CheckIfRunning
	SET ProcessIsRunning=False
	tasklist /fi "ImageName eq %~1" /fo csv 2>NUL | find /I %1 1>> !LogFilePath! 2>&1
	if "%ERRORLEVEL%"=="0" Set "ProcessIsRunning=True"
	EXIT /B



:GetEqualizerAPOPath
	CALL :RegistryKeyQueryValue "HKEY_LOCAL_MACHINE\SOFTWARE\EqualizerAPO" "InstallPath" "EqualizerAPOInstallationPath"
	SET "EqualizerAPOUninstallerPath=!EqualizerAPOInstallationPath!/Uninstall.exe"
	EXIT /B

:GetHeSuViPath
	SET "HeSuViFolderPath=!EqualizerAPOInstallationPath!\Config\HeSuVi"
	SET "HeSuViExeFullPath=!HeSuViFolderPath!\HeSuVi.exe"
	EXIT /B

:ManualSpeakerConfiguration
	SET "SpeakerConfigurationName=%~1"
CALL :PrintAndLog " - [96m[Sound window] Right-click !DefaultPlaybackDeviceNameWASAPI! > Configure Speakers:[0m"
	CALL :PrintAndLog "    - Audio channels:                [36m!SpeakerConfigurationName![0m"
	CALL :PrintAndLog "    - Optional speakers:             [36mCheck all available[0m"
	CALL :PrintAndLog "    - Full-range speakers:           [36mCheck all available[0m"
	CALL :PrintAndLog "    - Close Sound window."
	CALL :SpeakerConfiguration
	EXIT /B

:SpeakerConfiguration
	CALL :ForceCloseIfRunning "rundll32.exe"
	CALL :RunAndWait "rundll32.exe" Shell32.dll,Control_RunDLL Mmsys.cpl,,0
	CALL :PlayBackgroundMusic
	EXIT /B

:CheckSpeakerConfiguration
:ReCheckSpeakerConfiguration
	CALL :GetDevice
	if "!FetchedRenderer!"=="HeSuVi" (
		CALL :SpeakerConfigurationTargetSurround
		) ELSE (
		if "!FetchedWrapper!"=="Crystal Mixer" (
			CALL :SpeakerConfigurationTargetSurround
			) ELSE (
			CALL :SpeakerConfigurationTargetStereo
			)
	EXIT /B




:SpeakerConfigurationTargetStereo
	if !DefaultPlaybackDeviceChannelCount! NEQ 2 (
		cls
		CALL :PrintAndLog "[93m WARNING: Speaker configuration not set to Stereo.[0m"
		CALL :ManualSpeakerConfiguration "[92mStereo[0m"
		GOTO :ReCheckSpeakerConfiguration
		) ELSE (
		CALL :PrintAndLog "[0m - System audio channels:            [36mStereo[0m"
		)
	EXIT /B

:SpeakerConfigurationTargetSurround
	if !DefaultPlaybackDeviceChannelCount! LEQ 2 (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[93m WARNING: Speaker configuration not set to Surround.[0m"
		CALL :ManualSpeakerConfiguration "[36m7.1 Surround[0m or [92m5.1 Surround[0m/[93mQuadraphonic[0m."
		GOTO :ReCheckSpeakerConfiguration
		) ELSE (
		if !DefaultPlaybackDeviceChannelCount! EQU 8 (
			CALL :PrintAndLog "[0m - System audio channels:            [36m7.1 Surround[0m"
			) ELSE (
			if !DefaultPlaybackDeviceChannelCount! EQU 6 (
				CALL :PrintAndLog "[0m - System audio channels:            [92m5.1 Surround[0m"
				) ELSE (
				if !DefaultPlaybackDeviceChannelCount! EQU 4 (
					CALL :PrintAndLog "[0m - System audio channels:            [93mQuadraphonic[0m"
					) ELSE (
					CALL :PrintAndLog "[0m - System audio channels:            [90mUnknown[0m"
					)
				)
			)
		)
	EXIT /B







:PlayBackgroundMusic
	CALL :StopAllSounds
rem	if not defined MusicFileNowPlayingPath (
	SET MusicFileCount=0
	CD /D "%~dp0Resources\Music"
	for /r %%i in (*.*) do (
		SET /A MusicFileCount=MusicFileCount+1
		SET "MusicFilePath[!MusicFileCount!]=%%i"
		SET "MusicFileName[!MusicFileCount!]=%%~ni"
		)
	CALL :ResetWorkingDirectory
	SET /a MusicFileNowPlayingIndex=%random% %%!MusicFileCount! +1
	for /f "delims=" %%A in ("!MusicFileNowPlayingIndex!") do (set MusicFileNowPlayingPath=!MusicFilePath[%%A]!)
rem		)
	if "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (
		IF !DefaultPlaybackDeviceChannelCount! EQU 4 (
			SET MPTChannels=4
			) ELSE (
			SET MPTChannels=2
			)
		) ELSE (
		if !DefaultPlaybackDeviceChannelCount! GEQ 4 (
			SET MPTChannels=4
			) ELSE (
			SET MPTChannels=2
			)
		)
	IF NOT "!BackgroundMusic!"=="Off" (
rem		CALL :SetVolume 20
	 	start /B /SEPARATE "" "%~dp0Resources\Tools\OpenMPT\openmpt123.exe" -q --gain +9   --repeat -1 --channels !MPTChannels! "!MusicFileNowPlayingPath!" 1>> NUL 2>&1
	 	for /f "delims=" %%A in ("!MusicFileNowPlayingIndex!") do (title !ScriptNameVersion! ^| Music: !MusicFileName[%%A]!)
		) ELSE (
	 	start /B /SEPARATE "" "%~dp0Resources\Tools\OpenMPT\openmpt123.exe" -q --gain -100 --repeat -1 --channels !MPTChannels! "!MusicFileNowPlayingPath!" 1>> NUL 2>&1
	 	for /f "delims=" %%A in ("!MusicFileNowPlayingIndex!") do (title !ScriptNameVersion!)
		)
 	EXIT /B 0

 	::StopAllSounds
:StopAllSounds
	CALL :ForceCloseIfRunning "openmpt123.exe"
	CALL :ForceCloseIfRunning "mpv.exe"
	CALL :SetTitle
	EXIT /B

:PlaySound
	CALL :StopAllSounds
	if "%2"=="--loop" (
		start /B /SEPARATE "" "Resources\Tools\mpv\mpv.com" --really-quiet --gapless-audio --loop %1<NUL
		) ELSE (
		start /B /SEPARATE "" "Resources\Tools\mpv\mpv.com" --really-quiet %1<NUL
		)
	EXIT /B

:SetTitle
	title !ScriptNameVersion!
	EXIT /B

:SetBinauralSoftwareTypes
	SET BinauralSoftwareType[0]=Wrapper
	SET BinauralSoftwareType[1]=Renderer
	SET BinauralSoftwareType[2]=Base
	EXIT /B


:CreateRestorePoint
	IF NOT DEFINED SystemRestorePoint (
		CALL :LineBreak
		CALL :PrintActionAndLog "[0m Creating system restore point...   "
		net start srservice /y	1>> !LogFilePath! 2>&1
		wmic.exe /namespace:\\root\default Path SystemRestore Call enable “!SystemDrive!\”	1>> !LogFilePath! 2>&1
		wmic.exe /Namespace:\\root\default Path SystemRestore Call CreateRestorePoint "Before !ScriptNameVersion! installation [!CurrentDate!]", 100, 7	1>> !LogFilePath! 2>&1
		if !ERRORLEVEL! EQU 0 (CALL :SavePreferenceSetting "SystemRestorePoint" "!CurrentDate!")
		CALL :ErrorCheck
		)
	EXIT /B

:LineBreak
	CALL :PrintActionAndLog ""
	EXIT /B

:InstallASIO4ALL
	IF NOT exist "!ProgramFiles32BitPath!\!ASIO4ALLName!\uninstall.exe" (
		CALL :PrintActionAndLog "[0m Installing [1mASIO4ALL !ASIO4ALLVersion![0m... "
		CALL :InstallSilent "Resources\Dependencies\ASIO4ALL\Version\!ASIO4ALLVersion!\InstallSilent\ASIO4ALL.exe"
		CALL :ErrorCheck
		CALL :DeleteFile "!USERPROFILE!\Desktop\!ASIO4ALLName! Instruction Manual.lnk"
		CALL :DeleteFile "!USERPROFILE!\Desktop\ASIO4ALL Web Site.lnk"
		)
	EXIT /B

:InstallFlexASIO
	IF NOT exist "!ProgramFilesPath!\FlexASIO\unins000.exe" (
		CALL :PrintActionAndLog "[0m Installing [1mFlexASIO !FlexASIOVersion![0m...        "
		CALL :InstallSilentInno "Resources\Dependencies\FlexASIO\FlexASIO.exe"
		CALL :ErrorCheck
		)
	copy "Resources\Dependencies\FlexASIO\FlexASIO.toml" "!USERPROFILE!\FlexASIO.toml"	1>> !LogFilePath! 2>&1
	IF !SampleRate! GTR 48000 (CALL :AdjustSampleRate Renderer)
	set /a FlexASIOBufferSize=!SampleRate!/100
rem	IF !SampleRate! EQU 44100 (set /a FlexASIOBufferSize=441) ELSE (set /a FlexASIOBufferSize=480)
	CALL :UpdateINI "!USERPROFILE!\FlexASIO.toml" "" bufferSizeSamples !FlexASIOBufferSize!
	CALL :UpdateFile "!USERPROFILE!\FlexASIO.toml" output device "!DefaultSoundCardNameWASAPI!"
	CALL :UpdateINI "!USERPROFILE!\FlexASIO.toml" output wasapiExclusiveMode !FlexASIOWASAPIExclusive!
	EXIT /B

:InstallHiFiCable
	IF NOT exist "!HiFiCableFullPath!" (
		CALL :PrintActionAndLog "[0m Installing [1mHi-Fi Cable !HiFiCableVersion![0m...  "
		"Resources\Dependencies\Hi-Fi Cable\Hi-Fi Cable.exe" -i -h
		CALL :SetVolume 25
		CALL :ErrorCheck
		)
	EXIT /B

:InstallVolumeLinker
	for /f "delims=" %%A in ("!WindowsArchitectureBits!") do (
		IF NOT EXIST "!VolumeLinkerStartupShortcutPath!" (
			CALL :CreateFolder "!VolumeLinkerInstallationFolderPath!"
			IF NOT EXIST "!VolumeLinkerInstallationFullPath!" (
				REM CALL :GetDevice
				CALL :PrintActionAndLog "[0m Installing [1mVolumeLinker !VolumeLinkerVersion![0m... "
				copy "Resources\Dependencies\VolumeLinker\VolumeLinker%%A.exe" "!VolumeLinkerInstallationFullPath!"			1>> !LogFilePath! 2>&1
				CALL :ErrorCheck
				)
			)
		)
	if exist "!VolumeLinkerInstallationFolderPath!" (
	CALL :LoadPreferences
		CALL :RegistryKeyAdd "!VolumeLinkerRegistryPath!" "LinkActive" "REG_DWORD" "1"
		CALL :RegistryKeyAdd "!VolumeLinkerRegistryPath!" "MasterDevice" "REG_SZ" "{0.0.0.00000000}.!VirtualAudioDeviceID!"
		CALL :RegistryKeyAdd "!VolumeLinkerRegistryPath!" "SlaveDevice" "REG_SZ" "{0.0.0.00000000}.!DefaultSoundCardID!"
		)
	EXIT /B

:InstallEqualizerAPO
	CALL :GetEqualizerAPOPath
	SET HeSuViInstallationStarted=True
	CALL :RestartAudioService
	CALL :GetDevice
	CALL :SetAudioDeviceTypes
	for /f "delims=" %%D in ("!AudioDeviceType[0]!") do (
		IF DEFINED %%DID (
			CALL :RegistryKeySetPermissions "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render"

REM			CALL :RegistryKeyAdd "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{b725f130-47ef-101a-a5f1-02608c9eebac},10" "REG_SZ" "Equalizer APO"
REM			CALL :RegistryKeyAdd "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},1" "REG_SZ" "{EACD2258-FCAC-4FF4-B36D-419E924A6D79}"
REM			CALL :RegistryKeyAdd "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},2" "REG_SZ" "{EC1CC9CE-FAED-4822-828A-82A81A6F018F}"

			CALL :RegistryKeyAdd "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties" "{a45c254e-df1c-4efd-8020-67d146a850e0},2" "REG_SZ" "!VirtualSurroundDeviceLabel!"
			CALL :RegistryKeyAdd "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!%%DID!\Properties" "{259abffc-50a7-47ce-af08-68c9a7d73366},12" "REG_SZ" "!HeSuViExeFullPath:\=\\!,0"
			)
		)
	IF NOT exist "!EqualizerAPOInstallationPath!\Configurator.exe" (
		CALL :PrintActionAndLog "[0m Installing [1mEqualizer APO !EqualizerAPOVersion![0m...   "
		CALL :PrintActionAndLog "[96m[Configurator window] Check[0m [92mVirtual Surround[0m [96mthen click OK[0m"
		"Resources\Dependencies\Equalizer APO\EqualizerAPO!WindowsArchitectureBits!.exe" /S
		SET ExecutedEqualizerAPOConfigurator=True
		CALL :GetEqualizerAPOPath
		if exist "!EqualizerAPOInstallationPath!" (
			CALL :CheckEqualizerAPODeviceInstallationState
			GOTO :InstallHeSuVi
			) ELSE (
			CALL :ErrorCheckFailure
			CALL :PrintAndLog "[93m Equalizer APO failed to install![0m"
			CALL :PrintAndLog "[96mPress any key to retry installation.[0m"
			pause >NUL
			GOTO :InstallEqualizerAPO
			)
		) ELSE (
		CALL :CheckEqualizerAPODeviceInstallationState
		)


		REM	::Register device
		REM	for /f "delims=" %%D in ("!AudioDeviceType[0]!") do (
		REM	REM for /F "tokens=2 delims==" %%D in ('set AudioDeviceType[') do (
		REM		IF DEFINED %%DID (
		REM			CALL :RegistryKeySetPermissions "HKEY_LOCAL_MACHINE\SOFTWARE\EqualizerAPO\Child APOs"
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},1"	/d "^!KEY"										/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},2"	/d "^!KEY"										/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},5"	/d "^!KEY"										/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},6"	/d "^!KEY"										/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},7"	/d "^!KEY"										/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "PreMixChild"								/d ""											/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "PostMixChild"								/d ""											/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "AllowSilentBufferModification"				/d "false"										/f	1>> !LogFilePath! 2>&1
		REM			reg add "HKLM\SOFTWARE\EqualizerAPO\Child APOs\!%%DID!" 												/t REG_SZ	/v "Version"									/d "2"											/f	1>> !LogFilePath! 2>&1
		REM			)
		REM		)


	EXIT /B

:InstallHeSuVi
	title !ScriptNameVersion! - HeSuVi !HeSuViVersion!
	CALL :GetHeSuViPath
	IF NOT exist "!HeSuViExeFullPath!" (
		title !ScriptNameVersion! - HeSuVi !HeSuViVersion!
		CALL :PrintActionAndLog "[0m Installing [1mHeSuVi !HeSuViVersion![0m...       "
		::Copy main files
		CALL :Extract "Resources\Renderer\HeSuVi\Version\!HeSuViVersion!\EqualizerAPOInstallationPath\HeSuVi.extract" "!HeSuViFolderPath!"
		::HRTF
		CALL :Extract "Resources\Renderer\HeSuVi\HRTF\EqualizerAPOInstallationPath\HeSuVi\hrir.extract" "!HeSuViFolderPath!\hrir"
		::EQ
		CALL :Extract "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\HeSuVi\eq.extract" "!HeSuViFolderPath!\eq"
		::Config
		CALL :Extract "Resources\Renderer\HeSuVi\Common\EqualizerAPOInstallationPath\config.extract" "!EqualizerAPOInstallationPath!\config"
		::Create shortcut
		CALL :CreateShortcut "!APPDATA!\Microsoft\Windows\Start Menu\Programs\HeSuVi\HeSuVi.lnk" "!HeSuViExeFullPath!" "" "" ""
		CALL :ErrorCheck
		)
	EXIT /B

:EnableHeSuVi
	title !ScriptNameVersion! - HeSuVi !HeSuViVersion!
	IF NOT "!HeSuViInstallationStarted!"=="True" (CALL :SplashInfoInstall)
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[0m Enabling [1mHeSuVi[0m..."
	IF DEFINED HRTF (SET "HRTF=!HRTF:_(Inverted_Y)=!")
	IF DEFINED FetchedHRTF (SET "FetchedHRTF=!FetchedHRTF:_(Inverted_Y)=!")
	IF !SampleRate! GTR 48000 (CALL :AdjustSampleRate Renderer)
	CALL :UpdateHRTFHeSuViFilePath
	CALL :ToggleHeSuVi 0000 Enabled Virtualization
	CALL :ToggleMicrosoftSpatialSound "" Disabled
	CALL :SetExclusiveMode
	CALL :EnableEnhancements
	CALL :DisableCommunicationsDucking
	CALL :SetFormat
	CALL :PrintAndLog " - HRTF:                             [1m!FetchedHRTF![0m"
	IF DEFINED EqualizerPreset (CALL :PrintAndLog " - Equalizer preset:                 [1m!EqualizerPreset![0m")
	CALL :CheckSpeakerConfiguration
	CALL :RestartAudioService "[0m - Audio service:                    " "[36mRestarted[0m"
	CALL :ToggleHiFiCable
	CALL :PlayBackgroundMusic
	CALL :ClearConfigurationFile
REM	CALL :DeleteFolder "Resources\Base"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m::::::::::::::::::::[0m [92mInstallation has completed successfully.[0m [90m::::::::::::::::::::[0m"
	CALL :PrintCredits
	CALL :Notes
	


	IF DEFINED FetchedTitle (IF NOT !FetchedTitle!=="" (CALL :ShareResults))
	SET FetchedWrapper=""
	SET FetchedWrapperVersion=""
	SET "FetchedRenderer=HeSuVi"
	SET "FetchedRendererVersion=!HeSuViVersion!"
	IF DEFINED ExecutableFilePath (
		GOTO :ShareResults
		)
		CALL :PrintAndLog "[96m Press any key to test Virtual Surround. Otherwise, you can close this window.[0m"
		PAUSE>NUL
		CALL :PlaySound "%~dp0Resources\Tests\Channel Identification.wav"
		CALL :PrintAndLog ""
		CALL :RunSeparately "!HeSuViExeFullPath!"
		:PlayTestSoundMainMenu
		CLS
		CALL :PrintAndLog "[92mIn HeSuVi, you can fine-tune HRTF by selecting the HRIR where sounds are always at the same height as your ears.[0m"
		CALL :PrintAndLog "[90mPress[0m [1mletter[0m/[1mnumber[0m [90mto play respective Surround Sound audio sample.[0m"
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[1m5[0m Channel Identification"
		CALL :PrintAndLog "[1m7[0m   Front Left"
		CALL :PrintAndLog "[1m8[0m   Front Center"
		CALL :PrintAndLog "[1m9[0m   Front Right"
		CALL :PrintAndLog "[1m6[0m   Side Right"
		CALL :PrintAndLog "[1m3[0m   Rear Right"
		CALL :PrintAndLog "[1m1[0m   Rear Left"
		CALL :PrintAndLog "[1m4[0m   Side Left"
		CALL :PrintAndLog "[1m2[0m   Subwoofer"
		CALL :PrintAndLog "[1m0[0m Matrix Surround"
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[1mA[0m Noise"
		CALL :PrintAndLog "[1mB[0m Water"
		CALL :PrintAndLog "[1mC[0m Beach"
		CALL :PrintAndLog "[1mD[0m Horror"
		CALL :PrintAndLog "[1mE[0m War"
		CALL :PrintAndLog "[1mF[0m Music"
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[1mS[0m Stop all sounds"

		:PlayTestSoundPrompt
		Choice /C 1234567890SABCDEF /N 1>> !LogFilePath! 2>&1
		SET TestSoundPlaybackSelection=!ERRORLEVEL!
		"!HeSuViExeFullPath!" -r
		If !TestSoundPlaybackSelection!==1	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Rear Left.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==2	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Subwoofer.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==3	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Rear Right.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==4	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Side Left.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==5	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification.wav" --loop 					<NUL)
		If !TestSoundPlaybackSelection!==6	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Side Right.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==7	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Front Left.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==8	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Front Center.wav" --loop	<NUL)
		If !TestSoundPlaybackSelection!==9	(CALL :PlaySound "%~dp0Resources\Tests\Channel Identification - Front Right.wav" --loop		<NUL)
		If !TestSoundPlaybackSelection!==10	(
			set "Prelude=Matrix Surround Decoder [!BitDepth!-bit, !SampleRate! Hz].txt"
rem			CD /D "!HeSuViFolderPath!"
rem			for /F "tokens=*" %%p in (prelude.txt) do (
rem				SET "PreludeLine=%%p"
rem				for /f "delims=" %%A in ("!Prelude!") do (
rem					IF NOT "!PreludeLine!"=="!PreludeLine:%%A=!" (
rem						SET "PreludeMatrixSurround=True"
rem						) ELSE (
rem						SET "PreludeMatrixSurround=False"
rem						)
rem					)
rem				)
rem			CALL :ResetWorkingDirectory
rem			IF NOT "!PreludeMatrixSurround!"=="True" (
rem				IF NOT "!PreludeMatrixSurroundWarned!"=="True" (
rem					SET PreludeMatrixSurroundWarned=True
rem					)
rem				)
			"!HeSuViExeFullPath!" -r "!Prelude!"
			if not defined PreludeSet (
			CALL :RunSeparately "!HeSuViExeFullPath!"
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[1mIn HeSuVi > Additional tab > Beforehand has been set to:[0m"
			CALL :PrintAndLog "[36m!Prelude! [0m"
			CALL :PrintAndLog "[93mWARNING: Use ONLY with matrix-encoded content like Dolby Surround/Pro Logic (II), etc., otherwise, disable it.[0m"
			)
			SET PreludeSet=True
			CALL :PlaySound "%~dp0Resources\Tests\Matrix Surround.wav" --loop															<NUL
			GOTO :PlayTestSoundPrompt
			) 
		If !TestSoundPlaybackSelection!==11	(CALL :StopAllSounds)
		If !TestSoundPlaybackSelection!==12	(CALL :PlaySound "%~dp0Resources\Tests\Noise.wav" --loop 									<NUL)
		If !TestSoundPlaybackSelection!==13	(CALL :PlaySound "%~dp0Resources\Tests\Water.wav" --loop 									<NUL)
		If !TestSoundPlaybackSelection!==14	(CALL :PlaySound "%~dp0Resources\Tests\Beach.wav" --loop 									<NUL)
		If !TestSoundPlaybackSelection!==15	(CALL :PlaySound "%~dp0Resources\Tests\Horror.wav" --loop 									<NUL)
		If !TestSoundPlaybackSelection!==16	(CALL :PlaySound "%~dp0Resources\Tests\War.wav" --loop 										<NUL)
		If !TestSoundPlaybackSelection!==17 (
			if "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (
					IF NOT !DefaultPlaybackDeviceChannelCount! EQU 4 (
						CALL :PrintAndLog "[93mWARNING: Virtual Audio Device requires speaker configuration set to 4.0/Quadraphonic for proper Quad Surround playback.[0m"
						CALL :RunAndWait "rundll32.exe" Shell32.dll,Control_RunDLL Mmsys.cpl,,0
						SET "BackgroundMusic=Off"
						CALL :GetDevice
						SET "BackgroundMusic=On"
						CALL :PlayBackgroundMusic
						GOTO :PlayTestSoundMainMenu
						) ELSE (
						CALL :GetDevice
						)
				) ELSE (
				SET "BackgroundMusic=On"
				CALL :PlayBackgroundMusic
				)
			GOTO :PlayTestSoundPrompt
			)
			SET PreludeSet=
			GOTO :PlayTestSoundMainMenu
		exit
		)
	EXIT /B

:CheckEqualizerAPODeviceInstallationState
CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},1"
if !RegistryKeyQueryErrorLevel! NEQ 0 (
	CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},2"
	if !RegistryKeyQueryErrorLevel! NEQ 0 (
		CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},5"
		if !RegistryKeyQueryErrorLevel! NEQ 0 (
			CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},6"
			if !RegistryKeyQueryErrorLevel! NEQ 0 (
				CALL :RegistryKeyQuery "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render\!DefaultPlaybackDeviceID!\FxProperties" "{d04e05a6-594b-4fb6-a80d-01af5eed7d1d},7"
				if !RegistryKeyQueryErrorLevel! NEQ 0 (
					if "!ExecutedEqualizerAPOConfigurator!"=="True" (
						CALL :ErrorCheckFailure
						)
						CALL :PrintAndLog "[93m WARNING: Equalizer APO not enabled for the Default device.[0m"
						CALL :RunEqualizerAPOConfigurator
						) ELSE (
						if "!ExecutedEqualizerAPOConfigurator!"=="True" (
							CALL :ErrorCheckSuccess
							)
						GOTO :InstallHeSuVi
						)
					) ELSE (
					if "!ExecutedEqualizerAPOConfigurator!"=="True" (
						CALL :ErrorCheckSuccess
						)
					GOTO :InstallHeSuVi
					)
				) ELSE (
				if "!ExecutedEqualizerAPOConfigurator!"=="True" (
					CALL :ErrorCheckSuccess
					)
				GOTO :InstallHeSuVi
				)
			) ELSE (
			if "!ExecutedEqualizerAPOConfigurator!"=="True" (
				CALL :ErrorCheckSuccess
				)
			GOTO :InstallHeSuVi
			)
		) ELSE (
		if "!ExecutedEqualizerAPOConfigurator!"=="True" (
			CALL :ErrorCheckSuccess
			)
		GOTO :InstallHeSuVi
		)
	EXIT /B

:RunEqualizerAPOConfigurator
	CALL :PrintAndLog ""
	CALL :PrintActionAndLog "[0m Enabling [1mEqualizer APO[0m..."
	CALL :PrintActionAndLog "[96m[Configurator window] Check[0m [92mVirtual Surround[0m [96mthen click OK/Close[0m [93m(DO NOT REBOOT)[0m[96m][0m"
	CD /D "!EqualizerAPOInstallationPath!"
	"!EqualizerAPOInstallationPath!\Configurator.exe"
	CD /D "%~dp0"
	SET ExecutedEqualizerAPOConfigurator=True
	CALL :CheckEqualizerAPODeviceInstallationState
	EXIT /B

:DisableHiFiCable 
	CALL :DeleteFile "!HiFiCableStartupShortcutPath!"
	CALL :DeleteFile "!VolumeLinkerStartupShortcutPath!"
	CALL :ForceCloseIfRunning "VolumeLinker!WindowsArchitectureBits!.exe"
	CALL :CloseIfRunning "VBCABLE_AsioBridge.exe"
	EXIT /B

:EnableHiFiCable
	CALL :CreateShortcut "!StartupShortcutFolderPath!\ASIO Bridge (VB-Audio).lnk" "!HiFiCableFullPath!" "" "!HiFiCableInstallationFolderPath!" ""
	CALL :CreateShortcut "!StartupShortcutFolderPath!\VolumeLinker.lnk" "!VolumeLinkerInstallationFullPath!" "/l /m" "!VolumeLinkerInstallationFolderPath!" ""
	CALL :GetASIODevice 
	CALL :RegistryKeyAdd "!HiFiCableRegistryPath!" "AsioDevice" "REG_SZ" "!ASIODeviceCLSID!"
	CALL :RegistryKeyAdd "!HiFiCableRegistryPath!" "AsioBridge" "REG_DWORD" "1"
	IF "!ASIODeviceName!"=="Crystal Mixer!" ( 
		CALL :RegistryKeyAdd "!HiFiCableRegistryPath!" "route_in" "REG_DWORD" "0x12345678"
		) ELSE ( 
		CALL :RegistryKeyAdd "!HiFiCableRegistryPath!" "route_in" "REG_DWORD" "0x12000000"
		) 
	CALL :RegistryKeyAdd "!HiFiCableRegistryPath!" "route_out" "REG_DWORD" "0x00000000"
	CALL :RegistryKeyAdd "!HiFiCableRegistryPath!" "SysTray" "REG_DWORD" "1"
	IF NOT !DefaultSoundCardChannelCount! GTR 2 ( 
		IF DEFINED VirtualAudioDeviceName ( 
			CALL :SetDefaultPlaybackDevice "!VirtualAudioDeviceName!" 
			CALL :GetDevice 
			) 
		) 
	IF DEFINED VirtualAudioDeviceCaptureCLFID ("Resources\Tools\SoundVolumeView\SoundVolumeView.exe" /Disable "!VirtualAudioDeviceCaptureCLFID!") 
	CALL :RunSeparately "!HiFiCableFullPath!" "" 
	CALL :RunSeparately "!VolumeLinkerInstallationFullPath!" /l /m
	EXIT /B

:ToggleHiFiCable 
	CALL :DisableHiFiCable 
	if "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" ( 
		if "!FetchedRenderer!"=="HeSuVi" ( 
			CALL :InstallVolumeLinker 
			CALL :InstallHiFiCable 
			CALL :EnableHiFiCable 
			) 
		if "!FetchedWrapper!"=="Crystal Mixer" ( 
			CALL :InstallVolumeLinker 
			CALL :InstallHiFiCable 
			CALL :EnableHiFiCable
			)
		)
	EXIT /B

:DeleteFolder
	if exist %1 (rmdir /s /q %1) 1>> !LogFilePath! 2>&1
	EXIT /B 0

:RunSilently
	if exist %1 (%1 /S) 1>> !LogFilePath! 2>&1
	EXIT /B 0

:UninstallDriver
	CALL :PrintActionAndLog "[0m Uninstalling [1m!VirtualAudioDeviceName![0m driver..."
	"Resources\Tools\DevManView\DevManView!WindowsArchitectureBits!.exe" /Uninstall %1 1>> !LogFilePath! 2>&1
	CALL :ErrorCheck
	EXIT /B 0

:UninstallASIO4ALL
	if "!ASIODevice!"=="!ASIO4ALLName!" (
		if exist "!ProgramFiles32BitPath!\!ASIO4ALLName!" (
			CALL :PrintActionAndLog "[0m Uninstalling [1mASIO4ALL[0m...                   "
			CALL :RunSilently "!ProgramFiles32BitPath!\!ASIO4ALLName!\uninstall.exe"
			CALL :DeleteFolder "!ProgramFiles32BitPath!\!ASIO4ALLName!"
			CALL :ErrorCheck
			)
		)
	EXIT /B

:UninstallFlexASIO
	if "!ASIODevice!"=="FlexASIO" (
		if exist "!ProgramFilesPath!\FlexASIO" (
			CALL :PrintActionAndLog "[0m Uninstalling [1mFlexASIO[0m...                   "
			CALL :InstallSilentInno "!ProgramFilesPath!\FlexASIO\unins000.exe"
			CALL :DeleteFolder "!ProgramFilesPath!\FlexASIO"
			CALL :DeleteFile "!USERPROFILE!\FlexASIO.toml"
			CALL :ErrorCheck
			)
		)
	EXIT /B

:UninstallHiFiCable
	if exist "!HiFiCableInstallationFolderPath!" (
		CALL :PrintActionAndLog "[0m Uninstalling [1mHi-Fi Cable[0m...                "
		CALL :DisableHiFiCable
		if exist "!HiFiCableUninstallerFullPath!" (call "!HiFiCableUninstallerFullPath!" -u -h) 1>> !LogFilePath! 2>&1
		CALL :DeleteFolder "!HiFiCableInstallationFolderPath!"
		CALL :RegistryKeyDelete "!HiFiCableRegistryPath!"
		CALL :SavePreferenceSetting "ASIODevice" ""
		CALL :ErrorCheck
		CALL :UninstallDriver "!VirtualAudioDeviceName!"
		)
	EXIT /B

:UninstallVolumeLinker
	if exist "!VolumeLinkerInstallationFolderPath!" (
		CALL :PrintActionAndLog "[0m Uninstalling [1mVolume Linker[0m...              "
		CALL :ForceCloseIfRunning "VolumeLinker!WindowsArchitectureBits!.exe"
		CALL :DeleteFolder "!VolumeLinkerInstallationFolderPath!"
		CALL :RegistryKeyDelete "!VolumeLinkerRegistryPath!"
		CALL :ErrorCheck
		)
	EXIT /B

:UninstallEqualizerAPO
	CALL :PrintActionAndLog "[0m Uninstalling [1mEqualizer APO[0m...              "
	CALL :ForceCloseIfRunning "Configurator.exe"
	CALL :ForceCloseIfRunning "Editor.exe"
	CALL :RestartAudioService
	CALL :GetEqualizerAPOPath
	CALL :RunSilently "!EqualizerAPOUninstallerPath!"
CALL :ToggleService stop audiosrv
	CALL :DeleteFolder "!EqualizerAPOInstallationPath!"
	CALL :ErrorCheck
	EXIT /B

:UninstallHeSuVi
	CALL :PrintActionAndLog "[0m Uninstalling [1mHeSuVi[0m...                     "
	CALL :ForceCloseIfRunning "HeSuVi.exe"
	CALL :GetEqualizerAPOPath 
	CALL :DeleteFolder "!HeSuViFolderPath!"
	CALL :ErrorCheck
	EXIT /B


:IndexInstallationFiles
	SET InstallationFileCount=0
	CALL :SetBinauralSoftwareTypes
	for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
		IF "%%T"=="Base" (
			SET "%%TLabel=!FetchedTitle! settings [!Fetched%%TVersion!]"
			) ELSE (
			IF DEFINED Fetched%%TVersion (SET "%%TLabel=!Fetched%%T! !Fetched%%TVersion!") ELSE (SET "%%TLabel=!Fetched%%T!") 
			)
		SET %%TBinauralSoftwareCommonVersion[0]=Common
		SET %%TBinauralSoftwareCommonVersion[1]=Version\!Fetched%%TVersion!
		SET %%TBinauralSoftwareCommonVersion[2]=HRTF
		for /F "tokens=2 delims==" %%V in ('set %%TBinauralSoftwareCommonVersion[') do (
			IF EXIST "Resources\%%T\!Fetched%%T!\%%V" (
				for /f "usebackq delims=" %%G in (`dir /b /s /a-d "Resources\%%T\!Fetched%%T!\%%V\*"2^>^>NUL`) do (
					SET /a InstallationFileCount=InstallationFileCount+1
					for /f "delims=" %%C in ("!InstallationFileCount!") do (
						::Parse
						SET "InstallationFileSourceFullPath[%%C]=%%G"
						For %%A in ("!InstallationFileSourceFullPath[%%C]!") do (
						if !HalfLifeModCount! EQU 0 (SET HalfLifeModCount=1)
						for /l %%m in (1,1,!HalfLifeModCount!) do (
						for %%a in ("%%A") do for %%b in ("%%~dpa\.") do (SET "InstallationFileSourceParentFolder[%%C]=%%~nxb")
							SET "InstallationFile[%%C]=%%~nxA"
							SET "InstallationFileSoftwareType[%%C]=%%T"
							SET "InstallationFileName[%%C]=%%~nA"
							SET "InstallationFileExtension[%%C]=%%~xA"
							SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceFullPath[%%C]:%~dp0=!"
							for /f "delims=" %%F in ("!Fetched%%T!") do SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:Resources\%%T\%%F\%%V\=!"
							if "!FetchedWrapper!"=="MetaAudio" (
								set "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:[#]=[%%m]!"
								)
							for /f "tokens=1 delims=\" %%r in ("!InstallationFileSourceRelativePath[%%C]!") do (
								SET "InstallationFileSourceRootFolderPath[%%C]=!%%r!"
								SET "InstallationFileSourceRootRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:%%r\=!"
								)
							SET "InstallationFileDestinationFullPath[%%C]=!InstallationFileSourceRootFolderPath[%%C]!\!InstallationFileSourceRootRelativePath[%%C]!"
							::Redirect local file paths
							if "!FetchedRenderer!"=="OpenAL Soft" (
								if "!FetchedWrapper!"=="EAXEFX" (
									if "!InstallationFileSourceFullPath[%%C]!"=="!InstallationFileSourceFullPath[%%C]:EAXEFX=!" (
										if /I "!InstallationFile[%%C]!"=="OpenAL32.dll" (
											SET "InstallationFile[%%C]=eaxefx_driver.dll"
											SET "InstallationFileDestinationFullPath[%%C]=!ExecutableFolderPath!\!InstallationFile[%%C]!"
											SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:DLLFolderPath32\OpenAL32.dll=ExecutableFolderPath\eaxefx_driver.dll!"
											SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:DLLFolderPath64\OpenAL32.dll=ExecutableFolderPath\eaxefx_driver.dll!"
											)
										)
									) ELSE (
									if "!FetchedWrapper!"=="DSOAL" (
										if /I "!InstallationFile[%%C]!"=="alsoft.ini" (
											SET "OpenALSoftINIPath=!ExecutableFolderPath!\alsoft.ini"
											SET "InstallationFileDestinationFullPath[%%C]=!OpenALSoftINIPath!"
											SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:APPDATA\alsoft.ini=ExecutableFolderPath\alsoft.ini!"
											)
										if /I "!InstallationFile[%%C]!"=="OpenAL32.dll" (
											SET "InstallationFile[%%C]=dsoal-aldrv.dll"
											SET "InstallationFileDestinationFullPath[%%C]=!ExecutableFolderPath!\!InstallationFile[%%C]!"
											SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:DLLFolderPath32\OpenAL32.dll=ExecutableFolderPath\dsoal-aldrv.dll!"
											SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:DLLFolderPath64\OpenAL32.dll=ExecutableFolderPath\dsoal-aldrv.dll!"
											)
										) ELSE (
										if /I "!InstallationFile[%%C]!"=="OpenAL32.dll" (
											IF EXIST "!ExecutableFolderPath!\OpenAL32.dll" (
												SET "InstallationFileDestinationFullPath[%%C]=!ExecutableFolderPath!\!InstallationFile[%%C]!"
												SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:DLLFolderPath32\OpenAL32.dll=ExecutableFolderPath\OpenAL32.dll!"
												SET "InstallationFileSourceRelativePath[%%C]=!InstallationFileSourceRelativePath[%%C]:DLLFolderPath64\OpenAL32.dll=ExecutableFolderPath\OpenAL32.dll!"
												)
											)
										)
									)
								)
								for /f "delims=" %%A in ("!InstallationFile[%%C]!") do SET "InstallationFileSourceRelativeFolderPath[%%C]=!InstallationFileSourceRelativePath[%%C]:\%%A=!"
								for /f "delims=" %%A in ("!InstallationFile[%%C]!") do SET "InstallationFolderSourcePath[%%C]=!InstallationFileSourceFullPath[%%C]:\%%A=!"
								for /f "delims=" %%A in ("!InstallationFile[%%C]!") do SET "InstallationFolderDestinationPath[%%C]=!InstallationFileDestinationFullPath[%%C]:\%%A=!"
								SET "InstallationFileDestinationFullPathEdit[%%C]=!InstallationFileDestinationFullPath[%%C]:\-_-_BIOGame_Config_BIOEngine.ini=!"
								SET "InstallationFileSourceRelativePathEdit[%%C]=!InstallationFileSourceRelativePath[%%C]:\-_-_BIOGame_Config_BIOEngine.ini=!"
								SET "InstallationFileSourceRelativeFolderPathEdit[%%C]=!InstallationFileSourceRelativeFolderPath[%%C]:\Coalesced_INT.bin=!"
								SET "InstallationFileDestinationFullPathEdit[%%C]=!InstallationFileDestinationFullPathEdit[%%C]:..folderup\=..\!"
								)
							)
						)
					)
				)
			)
		)
	EXIT /B

:SearchExistingInstallation
	IF DEFINED ExecutableFolderPath 										(if exist "!PortableConfigurationFilePath!"											(	SET "InstallationFound=True"	))
	If "!FetchedRenderer!"=="HeSuVi" (
		if "!ASIODevice!"=="!ASIO4ALLName!"									(if exist "!ProgramFiles32BitPath!\!ASIO4ALLName!\uninstall.exe"					(	SET "InstallationFound=True"	))
		if "!ASIODevice!"=="FlexASIO"										(if exist "!ProgramFilesPath!\FlexASIO\unins000.exe"								(	SET "InstallationFound=True"	))
		IF DEFINED ASIODevice 												(if exist "!HiFiCableFullPath!" 													(	SET "InstallationFound=True"	))
		if exist "!EqualizerAPOUninstallerPath!" 																												(	SET "InstallationFound=True"	)
		if exist "!EqualizerAPOInstallationPath!"																												(	SET "InstallationFound=True"	)
		IF DEFINED ASIODevice 												(if exist "!VolumeLinkerInstallationFolderPath!"									(	SET "InstallationFound=True"	))
		if "!ASIODevice!"=="!ASIO4ALLName!"									(if exist "!ProgramFiles32BitPath!\!ASIO4ALLName!"									(	SET "InstallationFound=True"	))
		if "!ASIODevice!"=="FlexASIO"										(if exist "!ProgramFilesPath!\FlexASIO"												(	SET "InstallationFound=True"	))
		if "!ASIODevice!"=="FlexASIO"										(if exist "!USERPROFILE!\FlexASIO.toml"												(	SET "InstallationFound=True"	))
		IF DEFINED ASIODevice 												(if exist "!HiFiCableInstallationFolderPath!"										(	SET "InstallationFound=True"	))
		if "!ASIODevice!"=="!ASIO4ALLName!"									(IF NOT exist "!ProgramFiles32BitPath!\!ASIO4ALLName!\uninstall.exe"				(	SET "InstallationFound=False"	))
		if "!ASIODevice!"=="FlexASIO"										(IF NOT exist "!ProgramFilesPath!\FlexASIO\unins000.exe"							(	SET "InstallationFound=False"	))
		IF DEFINED ASIODevice 												(IF NOT exist "!HiFiCableFullPath!" 												(	SET "InstallationFound=False"	))
		IF NOT exist "!EqualizerAPOUninstallerPath!" 																											(	SET "InstallationFound=False"	)
		IF NOT exist "!EqualizerAPOInstallationPath!"																											(	SET "InstallationFound=False"	)
		IF DEFINED ASIODevice 												(IF NOT exist "!VolumeLinkerInstallationFolderPath!"								(	SET "InstallationFound=False"	))
		if "!ASIODevice!"=="!ASIO4ALLName!"									(IF NOT exist "!ProgramFiles32BitPath!\!ASIO4ALLName!"								(	SET "InstallationFound=False"	))
		if "!ASIODevice!"=="FlexASIO"										(IF NOT exist "!ProgramFilesPath!\FlexASIO"											(	SET "InstallationFound=False"	))
		if "!ASIODevice!"=="FlexASIO"										(IF NOT exist "!USERPROFILE!\FlexASIO.toml"											(	SET "InstallationFound=False"	))
		IF DEFINED ASIODevice 												(IF NOT exist "!HiFiCableInstallationFolderPath!"									(	SET "InstallationFound=False"	))
		) ELSE (
		If "!FetchedRenderer!"=="OpenAL Soft" 								(If !FetchedWrapper!==""	(IF EXIST "!ProgramFiles32BitPath!\OpenAL\oalinst.exe"	(	SET "InstallationFound=True"	)))
		for /l %%C in (1,1,!InstallationFileCount!) do (
			if exist "!InstallationFileDestinationFullPath[%%C]!" (
				IF NOT "!InstallationFileExtension[%%C]!"==".edit" (
					IF NOT "!InstallationFileExtension[%%C]!"==".extract" (
						CALL :Log ""
						CALL :Log "Installation item found: !InstallationFileDestinationFullPath[%%C]!"
																																									SET "InstallationFound=True"
						)
					)
				) ELSE (
				if exist "!InstallationFileDestinationFullPath[%%C]:.extract=!" (
																																									SET "InstallationFound=True"
					CALL :Log ""
					CALL :Log "Installation item found: !InstallationFileDestinationFullPath[%%C]:.extract=!"
					)
				)
			)
		If "!FetchedRenderer!"=="OpenAL Soft" (If !FetchedWrapper!=="" 		(IF NOT EXIST "!ProgramFiles32BitPath!\OpenAL\oalinst.exe"							(	SET "InstallationFound=False"	)))
		for /l %%C in (1,1,!InstallationFileCount!) do (
			IF NOT exist "!InstallationFileDestinationFullPath[%%C]!" (
				IF NOT "!InstallationFileExtension[%%C]!"==".edit" (
					IF NOT "!InstallationFileExtension[%%C]!"==".extract" (
						IF NOT "!InstallationFileSourceParentFolder[%%C]!"=="InstallSilentInno" (
							CALL :Log ""
							CALL :Log "Installation item not found: !InstallationFileDestinationFullPath[%%C]!"
																																									SET "InstallationFound=False"
							)
						) ELSE (
						IF NOT exist "!InstallationFileDestinationFullPath[%%C]:.extract=!" (
																																									SET "InstallationFound=False"
							CALL :Log ""
							CALL :Log "Installation item not found: !InstallationFileDestinationFullPath[%%C]:.extract=!"
							)
						)
					)
				)
			)
		)	
	IF DEFINED ExecutableFolderPath 										(IF NOT exist "!PortableConfigurationFilePath!"										(	SET "InstallationFound=False"	))  
	if !InstallationFound!==True (
		:Installation
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[93m !FetchedRenderer! seems to be already installed.[0m"
		CALL :PrintAndLog "[90m Press[0m [1mU[90m [90m+[0m [0mEnter[0m [90mto[0m [96mUninstall[0m [90mor just[0m [0mEnter[0m [90mto[0m [96mRe-apply[0m [90mbinaural settings if you're having issues.[0m"
		CALL :PrintAndLog "[90m Otherwise, you can close this window.[0m"
		CALL :PromptInput InstallationFoundChoice
		CALL :SplashInfoInstall
		IF NOT DEFINED InstallationFoundChoice (
			GOTO :StartFromBackup
			) ELSE (
			if /I !InstallationFoundChoice!==U (
				GOTO :StartFromUninstall
				) ELSE (
				GOTO :Installation
				)
			)
		) ELSE (
		GOTO :StartFromBackup
		)
	EXIT /B

:Uninstall
	If "!FetchedRenderer!"=="HeSuVi" (
		CALL :SplashInfoInstall
		CALL :PrintAndLog ""
		CALL :UninstallHeSuVi
		CALL :UninstallEqualizerAPO
		CALL :UninstallVolumeLinker
		CALL :UninstallHiFiCable
		CALL :UninstallASIO4ALL
		CALL :UninstallFlexASIO
		CALL :RestartAudioEndpointBuilder
		CALL :ErrorCheck
		CALL :PlayBackgroundMusic
		CALL :DeleteFile "!UserConfigurationFilePath!"
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[92m Uninstallation complete.[0m"
		CALL :PrintAndLog "[93m WARNING: Virtual surround has been disabled ALL games/programs using surround sound.[0m"
		CALL :PrintAndLog "[96m To reinstall, press any key. Otherwise, you can close this window.[0m"
		pause >NUL
		)

	CALL :PrintAndLog ""
	CALL :SetBinauralSoftwareTypes
	for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
		SET "InstallationFilesPath%%T=!BackupPath!\%%TInstallationFiles.txt"
		if exist "!InstallationFilesPath%%T!" (
			SET UninstallationStarted=True
			CALL :PrintActionAndLog "[0m Uninstalling %%T: [1m!%%TLabel![0m..."

for /f "delims=" %%u in ("!FetchedWrapper: =!") do (
rem echo CALL :InstallSilentInno "!%%uUninstallerFullPath!"
		CALL :InstallSilentInno "!%%uUninstallerFullPath!"
	)
rem			if "!FetchedWrapper!"=="Crystal Mixer" (
rem				CALL :InstallSilentInno "!CrystalMixerUninstallerFullPath!"
rem				CALL :DeleteFolder "!APPDATA!\Crystal Mixer"
rem				)

			CD /D "!BackupPath!"
			for /F "tokens=*" %%A in (%%TInstallationFiles.txt) do (
				if exist "%%A" (
					CALL :ResetWorkingDirectory
					CALL :Log ""
					CALL :Log "Uninstalling %%A"
					if not exist "%%A\*" (
						CALL :DeleteFile "%%A"
						if exist "%%A" (
							CALL :ResetWorkingDirectory
							CALL :Log ""
							CALL :Log "ERROR: Failed to uninstall %%A"
							)
						)
					)
				)
			CALL :ResetWorkingDirectory
			CALL :ErrorCheck
			) ELSE (
			IF DEFINED Fetched%%T (
				IF NOT !Fetched%%T!=="" (
					IF NOT "!FetchedRenderer!"=="HeSuVi" (
						IF NOT "%%T"=="Base" (
							CALL :PrintAndLog "[91m %%TInstallationFiles.txt not found. !Fetched%%T! cannot be uninstalled.[0m"
							)
						)
					)
				)
			)
		)
	IF DEFINED UninstallationStarted (
		IF "!FetchedRenderer!"=="OpenAL Soft" (
			CALL :DeleteFolder "!OpenALSoftHRTFPath!"
			)
		IF "!FetchedRenderer!"=="X3DAudio HRTF" (
			CALL :DeleteFile "!ExecutableFolderPath!\log.txt"
			CALL :DeleteFolder "!ExecutableFolderPath!\HRTF"
			)
		if exist "!InstallationFilesPath%%T!" (
			CALL :DeleteFile "!InstallationFilesPath%%T!"
			)
		)
	EXIT /B

:Restore
	if exist "!BackupPath!" (
		CALL :SetBinauralSoftwareTypes
		for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
			for /l %%C in (1,1,!InstallationFileCount!) do (
REM				if "!InstallationFileSoftwareType[%%C]!"=="%%T" (
					if exist "!BackupPath!\!InstallationFileSourceRelativePathEdit[%%C]:.edit=!" (
						for /f "delims=" %%A in ("!Fetched%%T!") do (
							IF NOT "!InstallationFileSourceFullPath[%%C]!"=="!InstallationFileSourceFullPath[%%C]:%%A=!" (
								IF NOT DEFINED RestorationStarted (CALL :PrintAndLog "")
								IF NOT DEFINED %%TRestorationStarted (CALL :PrintActionAndLog "[0m Restoring files edited by %%T: [1m!%%TLabel![0m...")
								SET RestorationStarted=True
								SET %%TRestorationStarted=True
rem								CALL :Log ""
CALL :Log "Restoring !BackupPath!\!InstallationFileSourceRelativePathEdit[%%C]:.edit=! into !InstallationFileDestinationFullPathEdit[%%C]:.edit=!"
REM								move /y "!BackupPath!\!InstallationFileSourceRelativePathEdit[%%C]:.edit=!" "!InstallationFileDestinationFullPathEdit[%%C]:.edit=!" 1>> !LogFilePath! 2>&1
								CALL :MoveFile "!BackupPath!\!InstallationFileSourceRelativePathEdit[%%C]:.edit=!" "!InstallationFileDestinationFullPathEdit[%%C]:.edit=!"
								)
							)
						)
REM					)
				)
			CALL :Log ""
			CALL :Log ""
			IF DEFINED %%TRestorationStarted (
				CALL :ErrorCheck
				)
			)
		::Confirm
		IF DEFINED UninstallationStarted (
			:StartConfirmation
			CALL :DeleteFolder "!BackupPath!"
REM CALL :DeleteFolder "Resources\Base\!FetchedBase!\Version\!FetchedBaseVersion!"
			CALL :DeleteFile "!UserConfigurationFilePath!"
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Uninstallation complete.[0m"
			CALL :PrintAndLog "[96m Press any key to reinstall with same/different settings. Otherwise, you can close this window.[0m"
			pause >nul
			SET "UserConfigurationFilePath=!DefaultConfigurationFilePath!"
			CALL :PrintAndLog "[0m Loading system configuration..."
			CALL :GetDevice
			SET Reinstalling=True
			goto :Reinstall
			) ELSE (
			IF DEFINED RestorationStarted (
				GOTO :StartConfirmation
				)
			)
		) ELSE (
		IF NOT "!FetchedRenderer!"=="HeSuVi" (
			CALL :PrintAndLog "[91m !BackupPath! not found. Backup cannot be restored.[0m"
			CALL :PrintAndLog "[93m Please avoid deleting this folder to prevent future uninstallation/restoration errors.[0m"
			CALL :PrintAndLog "[93m Next time you uninstall, non-backed up files will be deleted so you might have to restore the original ones manually.[0m"
			CALL :PrintAndLog "[96m Press any key to reinstall. Otherwise, you can close this window.[0m"
			pause >nul
			GOTO :StartFromInstall
			)
		)
	EXIT /B

:Backup
	CALL :SetBinauralSoftwareTypes
	for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
		IF DEFINED Fetched%%T (
			for /l %%C in (1,1,!InstallationFileCount!) do (
			if "!InstallationFileSoftwareType[%%C]!"=="%%T" (
					if exist "!InstallationFileDestinationFullPathEdit[%%C]:.edit=!" (
						for /f "delims=" %%A in ("!Fetched%%T!") do IF NOT "!InstallationFileSourceFullPath[%%C]!"=="!InstallationFileSourceFullPath[%%C]:%%A=!" (
							IF NOT DEFINED BackupStarted (CALL :PrintAndLog "")
							IF NOT DEFINED %%TBackupStarted (CALL :PrintActionAndLog "[0m Backing up files to be replaced by %%T: [1m!%%TLabel![0m...")
							SET BackupStarted=True
							SET %%TBackupStarted=True
							CALL :CreateFolder "!BackupPath!\!InstallationFileSourceRelativeFolderPathEdit[%%C]!"
							CALL :Log ""
							CALL :Log "Backing up !InstallationFileDestinationFullPathEdit[%%C]:.edit=! into !BackupPath!\!InstallationFileSourceRelativePathEdit[%%C]:.edit=!"
							IF NOT exist "!BackupPath!\!InstallationFileSourceRelativePath[%%C]:.edit=!" (copy /y "!InstallationFileDestinationFullPathEdit[%%C]:.edit=!" "!BackupPath!\!InstallationFileSourceRelativePathEdit[%%C]:.edit=!") 1>> !LogFilePath! 2>&1
							)
						)
					)
				)
			echo.>>!LogFilePath!
			IF DEFINED %%TBackupStarted (CALL :ErrorCheck)
			)
		)
	EXIT /B

:Install
	CALL :CheckIfInputRunning
	if "!FetchedRenderer!"=="HeSuVi" (
		::Virtual Surround
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[0m Loading !FetchedRenderer! configuration..."
		IF NOT DEFINED DefaultSoundCardChannelCount (CALL :GetDevice)




		IF !DefaultSoundCardChannelCount! GTR 4 (
			CALL :DisableHiFiCable
			) ELSE (
			IF DEFINED VirtualAudioDeviceName (
				CALL :SetDefaultPlaybackDevice "!VirtualAudioDeviceName!"
				CALL :GetDevice
				)
			)
		IF NOT !DefaultPlaybackDeviceChannelCount! GTR 2 (
			IF NOT "!DefaultPlaybackDeviceName!"=="!VirtualAudioDeviceName!" (
				IF NOT DEFINED ASIODevice (
					CALL :PrintAndLog "[93m WARNING: Default playback device not set to surround. Please choose a virtual surround method:[0m"
					CALL :PrintAndLog "[1m 1[0m [36m- Default Sound Card   - Native (For sound cards capable of 7.1 Surround. Select to check.)[0m"
					CALL :PrintAndLog "[1m 2[0m [92m- Virtual Audio Device - WASAPI (For sound cards not capable of Surround.)[0m"
					CALL :PrintAndLog "[1m 3[0m [93m- Virtual Audio Device - ASIO   (For experts. Low latency under certain conditions.)[0m"
					Choice /C 123 /N 1>> !LogFilePath! 2>&1
					cls
					If !ERRORLEVEL!==1 (
						SET FlexASIOWASAPIExclusive=
						CALL :SavePreferenceSetting "FlexASIOWASAPIExclusive" "!FlexASIOWASAPIExclusive!"
						CALL :ManualSpeakerConfiguration "[36m7.1 Surround[0m or [92m5.1 Surround[0m/[93mQuadraphonic[0m"
						CALL :GetDevice
						CALL :SplashInfoInstall
						GOTO :Install
						)
					If !ERRORLEVEL!==2 (
						CALL :PrintAndLog " [96mPlease select the WASAPI mode:[0m"
						CALL :PrintAndLog "[1m 1[0m [92mShared[0m    - Allows using the sound card and virtual audio device simultaneously."
						CALL :PrintAndLog "[1m 2[0m [36mExclusive[0m - Bit-perfect and low latency, but might not work in some cases."
						Choice /C 12 /N 1>> !LogFilePath! 2>&1
						If !ERRORLEVEL!==1 (
							SET FlexASIOWASAPIExclusive=false
							CALL :SavePreferenceSetting "FlexASIOWASAPIExclusive" "!FlexASIOWASAPIExclusive!"
							)
						If !ERRORLEVEL!==2 (
							SET FlexASIOWASAPIExclusive=true
							CALL :SavePreferenceSetting "FlexASIOWASAPIExclusive" "!FlexASIOWASAPIExclusive!"
							)
						SET "ASIODevice=FlexASIO"
						CALL :SavePreferenceSetting "ASIODevice" "FlexASIO"
						CALL :SplashInfoInstall
						CALL :PrintAndLog ""
						)
					If !ERRORLEVEL!==3 (
						SET FlexASIOWASAPIExclusive=
						CALL :SavePreferenceSetting "FlexASIOWASAPIExclusive" "!FlexASIOWASAPIExclusive!"
						SET SystemASIODeviceCount=0
						for /f "delims=" %%A in ('REG QUERY !SystemASIORegistryPath!') do (
							if not "%%~nA"=="!ASIO4ALLName!" (
								if not "%%~nA"=="FlexASIO" (
									SET /A SystemASIODeviceCount=!SystemASIODeviceCount!+1
									SET "SystemASIODevice[!SystemASIODeviceCount!]=%%~nA"
									)
								)
							)
						if !SystemASIODeviceCount! GTR 0 (
							:ASIODevicesDetected
							CLS
								CALL :PrintAndLog "[92m ASIO devices have been found.[0m"
								CALL :PrintAndLog "[96m If available, select the one from your sound card.[0m"
								CALL :PrintAndLog "[90m When in doubt, just select ASIO4ALL.[0m"
								CALL :PrintAndLog ""
								SET SystemASIODeviceListCount=0
								CALL :PrintAndLog "[1m 0[0m [96m!ASIO4ALLName![0m"
								SET SystemASIODevice[0]=
								for /F "tokens=2 delims==" %%D in ('set SystemASIODevice[') do (
									SET /A SystemASIODeviceListCount=!SystemASIODeviceListCount!+1
									CALL :PrintAndLog "[1m !SystemASIODeviceListCount![0m [96m%%D[0m"
									)
								CALL :PromptInput SystemASIODeviceSelection
								IF DEFINED SystemASIODeviceSelection (
									SET "SystemASIODevice[0]=!ASIO4ALLName!"
									for /f "delims=" %%S in ("!SystemASIODeviceSelection!") do (
										IF DEFINED SystemASIODevice[%%S] (
											SET "ASIODevice=!SystemASIODevice[%%S]!"
											CALL :SavePreferenceSetting "ASIODevice" "!ASIODevice!"
											) ELSE (
											CALL :ASIODevicesDetected
											)
										)
									) ELSE (
									CALL :ASIODevicesDetected
									)
								) ELSE (
								SET "ASIODevice=!ASIO4ALLName!"
								CALL :SavePreferenceSetting "ASIODevice" "!ASIO4ALLName!"
								)
						CALL :GetASIODevice
						CALL :SplashInfoInstall
						CALL :PrintAndLog ""
						)
					)
				)
			)

		IF NOT !DefaultSoundCardChannelCount! GTR 2 (
			if "!ASIODevice!"=="FlexASIO" 		(CALL :InstallFlexASIO)
			if "!ASIODevice!"=="!ASIO4ALLName!"	(CALL :InstallASIO4ALL)
			CALL :InstallHiFiCable
			CALL :InstallVolumeLinker
			)

		CALL :InstallEqualizerAPO
		CALL :InstallHeSuVi
		CALL :EnableHeSuVi

		) ELSE (
		IF DEFINED DefaultSoundCardName (
			CALL :ToggleHiFiCable
			if not "!FetchedWrapper!"=="Crystal Mixer" (
				CALL :SetDefaultPlaybackDevice "!DefaultSoundCardName!"
				)
			IF NOT "!ASIODevice!"=="FlexASIO" (
				CALL :ForceCloseIfRunning "VolumeLinker!WindowsArchitectureBits!.exe"
				CALL :CloseIfRunning "VBCABLE_AsioBridge.exe"
				)
			) ELSE (
			CALL :EnableHiFiCable
			CALL :SetDefaultPlaybackDevice "!VirtualAudioDeviceName!"
			)
		::Spatial Audio
		IF "!FetchedBackend!"=="Microsoft Spatial Sound" (
			CALL :EnableMicrosoftSpatialSound ^"!FetchedRenderer!^"
			) ELSE (
			CALL :SetBinauralSoftwareTypes
			for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
				SET %%TBinauralSoftwareCommonVersion[0]=Common
				SET %%TBinauralSoftwareCommonVersion[1]=!Fetched%%TVersion!
				SET %%TBinauralSoftwareCommonVersion[2]=HRTF
				for /F "tokens=2 delims==" %%V in ('set %%TBinauralSoftwareCommonVersion[') do (
					for /l %%C in (1,1,!InstallationFileCount!) do (
						if "!InstallationFileSoftwareType[%%C]!"=="%%T" (
							if exist "!InstallationFileSourceFullPath[%%C]!" (
								for /f "delims=" %%A in ("!Fetched%%T!") do IF NOT "!InstallationFileSourceFullPath[%%C]!"=="!InstallationFileSourceFullPath[%%C]:%%A=!" (
									::Store installation file paths
									SET "InstallationFilesPath%%T=!BackupPath!\%%TInstallationFiles.txt"
									IF NOT DEFINED InstallationFilePath[%%C] (CALL :CreateFolder "!BackupPath!")
									SET "InstallationFilePath[%%C]=Copied"

									::Execute
									IF "!InstallationFileSourceParentFolder[%%C]!"=="Install" (
										CALL :RunAndWait "!InstallationFileSourceFullPath[%%C]!"
										) ELSE (

										IF "!InstallationFileSourceParentFolder[%%C]!"=="InstallSilentInno" (
											IF "!InstallationFileExtension[%%C]!"==".exe" (
												CALL :InstallSilentInno "!InstallationFileSourceFullPath[%%C]!"
												)
											) ELSE (
											::Extract
											if "!InstallationFileExtension[%%C]!"==".extract" (
												if "!InstallationFileName[%%C]!"=="HRTF" (
													CALL :Extract "!InstallationFileSourceFullPath[%%C]!" "!InstallationFolderDestinationPath[%%C]!\!InstallationFileName[%%C]!" "*!FetchedHRTF:_(Inverted_Y)=!*"
													) ELSE (
													CALL :Extract "!InstallationFileSourceFullPath[%%C]!" "!InstallationFolderDestinationPath[%%C]!\!InstallationFileName[%%C]!"
													)
												SET ArchiveFilePathSet=True
												) ELSE (
												::Edit
												if "!InstallationFileExtension[%%C]!"==".edit" (
													CD /D "!InstallationFolderSourcePath[%%C]!"
													for /f "delims=" %%E in (!InstallationFile[%%C]!) do (
														SET "FileEditLine[%%C]=%%E"
														IF NOT "!FileEditLine[%%C]!"=="!FileEditLine[%%C]:[=!" (
															SET "FileEditSection[%%C]=!FileEditLine[%%C]!"
															SET "FileEditSection[%%C]=!FileEditSection[%%C]:[=!"
															SET "FileEditSection[%%C]=!FileEditSection[%%C]:]=!"
															) ELSE (
															for /f "tokens=1 delims==" %%A in ("!FileEditLine[%%C]!") do SET "FileEditKey[%%C]=%%A"
															for /f "tokens=2 delims==" %%A in ("!FileEditLine[%%C]!") do SET "FileEditValue[%%C]=%%A"
																IF NOT "!InstallationFileDestinationFullPath[%%C]!"=="!InstallationFileDestinationFullPath[%%C]:Coalesced_INT.bin=!" (
																	SET "InstallationFileDestinationFullPath[%%C]=!ExecutableFolderPath!\..\..\BioGame\CookedPCConsole\Coalesced_INT.bin"
																	)
															if exist "!InstallationFileDestinationFullPath[%%C]:.edit=!" (
																CALL :ResetWorkingDirectory
																CALL :Log ""
																CALL :Log "Setting [!FileEditSection[%%C]!]!FileEditKey[%%C]!=!FileEditValue[%%C]! in !InstallationFileDestinationFullPath[%%C]:.edit=!"
																CALL :UpdateFileConfiguration "!InstallationFileDestinationFullPath[%%C]:.edit=!" "!FileEditSection[%%C]!" "!FileEditKey[%%C]!" "!FileEditValue[%%C]!" ""
																SET !FileEditKey[%%C]!Defined=True
																) ELSE (
																CALL :ResetWorkingDirectory
																CALL :ErrorCheckWarning
																CALL :PrintAndLog "[93m !InstallationFileDestinationFullPath[%%C]:.edit=! needs to be edited but does not exist.[0m"
																CALL :PrintAndLog "[96m Press any key to run !ExecutableFile! so it can generate a new !InstallationFile[%%C]:.edit=!, then close it.[0m"
																pause >NUL
																if exist "!InputFilePath!\*" (CALL :RunSeparately "!ExecutableFilePath!") ELSE (CALL :RunSeparately "!InputFilePath!")
																CALL :PrintActionAndLog "[0m Waiting for [1m!InstallationFile[%%C]:.edit=![0m to be generated or copied manually...[0m"
																CALL :CheckIfEditFileWasCreated %%C
																)
															)
														)
														CALL :ResetWorkingDirectory
													) ELSE (
													::Copy
													If "!FetchedRenderer!"=="OpenAL Soft" (
														::OpenAL
														IF NOT EXIST "!ProgramFiles32BitPath!\OpenAL\oalinst.exe" (
															SET InstallationStarted=True
															CALL :PrintAndLog ""
															CALL :PrintActionAndLog "[0m Installing [1mOpenAL !OpenALVersion![0m..."
															"Resources\Dependencies\OpenAL\oalinst.exe" /SILENT 1>> !LogFilePath! 2>&1
															CALL :ErrorCheck
															)
														)
													IF NOT DEFINED InstallationStarted (CALL :PrintAndLog "")
													IF NOT DEFINED %%TInstallationStarted (CALL :PrintActionAndLog "[0m Installing %%T: [1m!%%TLabel![0m...")
													SET InstallationStarted=True
													SET %%TInstallationStarted=True
													CALL :CreateFolder "!InstallationFolderDestinationPath[%%C]!"
													IF NOT DEFINED InstallationFilePathSet[%%C] (
														CALL :CopyFile "!InstallationFileSourceFullPath[%%C]!" "!InstallationFileDestinationFullPath[%%C]!"
														echo !InstallationFileDestinationFullPath[%%C]!>>"!InstallationFilesPath%%T!"
														)
													::MetaAudio
													if "!FetchedWrapper!"=="MetaAudio" (
														SET HalfLifeModCount=0
														SET HalfLifeModName=
														IF EXIST "Resources\%%T\!Fetched%%T!\Version\%%V\HalfLifeModFolder[#]" (
															for /d %%x in ("!ExecutableFolderPath!\*") do (
																IF EXIST "%%x\liblist.gam" (
																	SET /A HalfLifeModCount=HalfLifeModCount+1
																	for /l %%m in (1,1,!HalfLifeModCount!) do (
																		SET HalfLifeMod[%%m]=%%~nx
																		IF NOT DEFINED HalfLifeModFolder[%%m] (set HalfLifeModFolder[%%m]=%%~nx)
																		for /f "delims=" %%A in ('findstr game "%%x\liblist.gam"') do (
																			IF NOT DEFINED HalfLifeModName[%%m] (
																				SET HalfLifeModName[%%m]=%%A
																				SET HalfLifeModName[%%m]=!HalfLifeModName[%%m]:game =!
																				CALL :ValidateFilename "HalfLifeModName[%%m]"
																				)
																			)
																		IF DEFINED ShortcutFilename[%%m] (set MetaAudioShortcutFilename[%%m]=!ShortcutFilename[%%m]! MetaAudio) ELSE (set MetaAudioShortcutFilename[%%m]=!HalfLifeModName[%%m]! MetaAudio)
																		IF DEFINED MetaAudioShortcutFilename[%%m] (set MetaAudioShortcutFilePath[%%m]=!USERPROFILE!\Desktop\!MetaAudioShortcutFilename[%%m]!.lnk)
																		IF NOT DEFINED MetaAudioShortcutFilePathAdded[%%m] (echo !MetaAudioShortcutFilePath[%%m]!>>"!InstallationFilesPathWrapper!")
																		SET MetaAudioShortcutFilePathAdded[%%m]=True
																		chcp 437>NUL
																		if not defined HalfLifeModShortcutCreated[%%m] (
																			CALL :DeleteFile "!MetaAudioShortcutFilePath[%%m]!"
																			CALL :CreateShortcut "!MetaAudioShortcutFilePath[%%m]!" "!ExecutableFolderPath!\MetaHook.exe" "-steam -game !HalfLifeMod[%%m]! -insecure +al_doppler 0.3 +al_occluder 1" "!ExecutableFolderPath!" ""
																			SET HalfLifeModShortcutCreated[%%m]=True
																			)
																		chcp 1252>NUL
																		CALL :CopyFolder "Resources\%%T\!Fetched%%T!\Version\%%V\HalfLifeModFolder[#]\*" "!ExecutableFolderPath!\!HalfLifeMod[%%m]!"
																		CD /D "!ExecutableFolderPath!\!HalfLifeMod[%%m]!\metahook"
																		For /R %%O IN (*) do Echo %%O>>"%~dp0!InstallationFilesPath%%T!"
																		CALL :ResetWorkingDirectory
																		)
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						)
					SET InstallationFilePathSet[%%C]=True
					)
				CALL :Log ""
				CALL :Log ""
				IF DEFINED %%TInstallationStarted (CALL :ErrorCheck)
				)
			)
		if "!FetchedRenderer!"=="OpenAL Soft" (
			IF NOT "!FetchedRendererVersion!"=="!FetchedRendererVersion:WASAPI=!" (
				IF !SampleRate! EQU 44100 (SET PeriodSize=96)
				IF !SampleRate! EQU 48000 (SET PeriodSize=96)
				IF !SampleRate! EQU 96000 (SET PeriodSize=160)
				IF NOT DEFINED period_sizeDefined 								(CALL :UpdateALSoft "General" "period_size" "!PeriodSize!")
				IF NOT DEFINED periodsDefined 									(CALL :UpdateALSoft "General" "periods" "1")
				IF NOT DEFINED sample-typeDefined 								(CALL :UpdateALSoft "General" "sample-type" "int16")
				IF NOT DEFINED driversDefined 									(CALL :UpdateALSoft "General" "drivers" "WASAPI,")
				) ELSE (
				IF NOT DEFINED period_sizeDefined 								(CALL :UpdateALSoft "General" "period_size" "1024")
				IF NOT DEFINED periodsDefined 									(CALL :UpdateALSoft "General" "periods" "3")
				IF NOT DEFINED sample-typeDefined 								(CALL :UpdateALSoft "General" "sample-type" "float32")
				IF NOT DEFINED driversDefined 									(CALL :UpdateALSoft "General" "drivers" "-dsound,")
				)
				IF NOT DEFINED default-hrtfDefined 								(CALL :UpdateALSoft "General" "default-hrtf" "!FetchedHRTF!-!SampleRate!")
				IF NOT DEFINED frequencyDefined 								(CALL :UpdateALSoft "General" "frequency" "!SampleRate!")
			IF NOT DEFINED Configuration (set Configuration=!FetchedConfiguration!)
			IF NOT "!Configuration!"=="!Configuration:Speaker=!" (
																				 CALL :UpdateALSoft "General" "stereo-mode" "speakers"
																				 CALL :UpdateALSoft "General" "channels" "ambi3"
																				 CALL :UpdateALSoft "decoder" "hq-mode" "true"
				IF NOT "!Configuration!"=="!Configuration:Square=!" 			(CALL :UpdateALSoft "decoder" "quad" "!OpenALSoftPresetsPath:\=/!/square.ambdec")
				IF NOT "!Configuration!"=="!Configuration:Rectangle=!" 			(CALL :UpdateALSoft "decoder" "quad" "!OpenALSoftPresetsPath:\=/!/rectangle.ambdec")
				IF NOT "!Configuration!"=="!Configuration:itu5.1=!" 			(CALL :UpdateALSoft "decoder" "surround51" "!OpenALSoftPresetsPath:\=/!/itu5.1.ambdec")
				IF NOT "!Configuration!"=="!Configuration:itu5.1-nocenter=!"	(CALL :UpdateALSoft "decoder" "surround51" "!OpenALSoftPresetsPath:\=/!/itu5.1-nocenter.ambdec")
				IF NOT "!Configuration!"=="!Configuration:Hexagon=!" 			(CALL :UpdateALSoft "decoder" "surround71" "!OpenALSoftPresetsPath:\=/!/hexagon.ambdec")
				IF NOT "!Configuration!"=="!Configuration:3D7.1=!" 				(CALL :UpdateALSoft "decoder" "surround71" "!OpenALSoftPresetsPath:\=/!/3D7.1.ambdec")
				)
			::Parse alsoft.ini into a string
			SET ALSoftINICount=0
			for /l %%C in (1,1,!InstallationFileCount!) do (
				IF "!InstallationFile[%%C]!"=="alsoft.ini" (
					pushd "!InstallationFolderDestinationPath[%%C]!"
					for /F "tokens=*" %%A in (alsoft.ini) do (
						if exist "!InstallationFileDestinationFullPath[%%C]!" (
							SET /A ALSoftINICount=ALSoftINICount+1
							for /f "delims=" %%a in ("!ALSoftINICount!") do (
								SET "ALSoftINILine[%%a]=%%A%%0A"
								SET "ALSoftINI=!ALSoftINI!!ALSoftINILine[%%a]!"
								)
							)
						)
					CALL :ResetWorkingDirectory
					)
				)
			if "!FetchedWrapper!"=="DSOAL" (
				CALL :PrintAndLog ""
				CALL :RegisterDirectSound
				)
			)
		if "!FetchedWrapper!"=="Creative ALchemy" (
			CALL :SetUpDirectSound
			)
		if "!FetchedWrapper!"=="IndirectSound" (
			CALL :SetUpDirectSound
			)
		if "!FetchedWrapper!"=="Crystal Mixer" (
			CALL :UpdateINI "!CrystalMixerINIFullPath!" "Device" "OutputDev" "OpenAL Soft on !DefaultSoundCardNameWASAPI!"
			)
		)
	EXIT /B

:AutomaticConfiguration
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[0m Applying optimal settings..."
	CALL :ToggleHeSuVi 2222 Disabled Stereo Mix
	IF NOT "!FetchedBackend!"=="Microsoft Spatial Sound" (CALL :ToggleMicrosoftSpatialSound "" Disabled)
	CALL :CheckSpeakerConfiguration
	CALL :SetExclusiveMode
	CALL :DisableEnhancements
	CALL :DisableCommunicationsDucking
	CALL :SetFormat
	if "!FetchedBackend!"=="Microsoft Spatial Sound" (CALL :PrintAndLog " - HRTF:                             [1m!FetchedRenderer![0m") ELSE (CALL :PrintAndLog " - HRTF:                             [1m!FetchedHRTF![0m")
	CALL :ToggleHiFiCable
	EXIT /B

:Complete
	CALL :ClearConfigurationFile
REM	CALL :DeleteFolder "Resources\Base"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m::::::::::::::::::::[0m [92mInstallation has completed successfully.[0m [90m::::::::::::::::::::[0m"
	CALL :PrintCredits
	CALL :Notes



	IF DEFINED ExecutableFilePath (GOTO :ShareResults)
	pause
	exit
	EXIT /B

:UpdateFile
	SET FileEditSection=%~2
	SET FileEditKey=%~3
	"Resources\Tools\dasel\dasel.exe" put string --file %1 !FileEditSection!.!FileEditKey! %4
	EXIT /B

:UpdateINI
	SET INITemp=%~1.temp
	"Resources\Tools\initool\initool.exe" s %1 %2 %3 %4 > "!INITemp!"
	CALL :MoveFile "!INITemp!" %1
	EXIT /B

:UpdateCoalesced
	SET EditFilePath=%~1
	SET EditFileFlag=%5
	IF DEFINED EditFileFlag (
		if %5==".edit" (
			For %%A in ("!EditFilePath!") do (
				CALL :CreateFolder "%%~dpA"
				IF NOT exist "!EditFilePath!" (echo. 2>"!EditFilePath!") >NUL 2>&1
				CALL :UpdateINI "!EditFilePath!" %2 %3 %4
				)
			) ELSE (
			For %%A in (%1) do (
				"Resources\Tools\LECoal\LECoal.exe" unpack %%A "%%~dpA%%~nA" 1>> !LogFilePath! 2>&1
				CALL :UpdateINI "%%~dpA%%~nA\-_-_BIOGame_Config_BIOEngine.ini" %2 %3 %4
				"Resources\Tools\LECoal\LECoal.exe" pack "%%~dpA%%~nA" %%A 1>> !LogFilePath! 2>&1
				CALL :DeleteFolder "%%~dpA%%~nA"
				)
			)
		)
	EXIT /B

:UpdateFileConfiguration
	SET EditFilePath=%~1
	SET EditFileFlag=%~5
	IF DEFINED EditFileFlag (if "!EditFileFlag!"==".edit" (IF EXIST "!EditFilePath!.edit" (move /y "!EditFilePath!.edit" "!EditFilePath!" 1>> !LogFilePath! 2>&1)))
	For %%A in (%1) do (
    	CALL :Log ""
    	CALL :Log "Updating %1 %2%3=%4"
    	SET EditFolderPath=%%~dpA
		IF NOT "!EditFilePath!"=="!EditFilePath:Coalesced_INT.bin=!" (
    		CALL :UpdateCoalesced %1 %2 %3 %4 %5
    		) ELSE (
		    if "%%~xA"==".ini" (
		    	CALL :UpdateINI %1 %2 %3 %4
		    	) ELSE (
			    CALL :UpdateFile %1 %2 %3 %4
		    	)
    		)
	    )
	SET EditFileFlag=%~5
	IF DEFINED EditFileFlag (if "!EditFileFlag!"==".edit" (move /y "!EditFilePath!" "!EditFilePath!.edit" 1>> !LogFilePath! 2>&1))
	EXIT /B

:CreateFileConfiguration
	IF NOT DEFINED FileConfigurationCreated (CALL :DeleteFolder "%~dp0Resources\Base\!FetchedBase!\Version\!FetchedBaseVersion!")
	For %%A in (%1) do (
		SET "EditFilePath=%%A"
		SET "EditFilePath=!EditFilePath:"=!"
		SET "EditFilePath=%~dp0Resources\Base\!FetchedBase!\Version\!FetchedBaseVersion!\!EditFilePath!"
		For %%P in ("!EditFilePath!") do (CALL :CreateFolder "%%~dpP")
		IF NOT exist "!EditFilePath!" (echo. 2>"!EditFilePath!") >NUL 2>&1
		CALL :UpdateFileConfiguration "!EditFilePath!" %2 %3 %4 ".edit"
		)
	SET "FileConfigurationCreated=True"
	EXIT /B

:AutoDetectConfiguration
	::Fallback > HeSuVi
	SET FetchedBackend=""
	SET FetchedWrapper=""
	SET FetchedWrapperVersion=""
	SET "FetchedRenderer=HeSuVi"
	SET Renderer=""
	SET FetchedRendererVersion=""
	SET Configuration=Headphone Virtual Surround
	SET FetchedConfiguration=""
	SET FetchedHRTF=""
	::Unity
	SET ExeUnityFile[0]=!ExecutableFilename!_DATA
	SET ExeUnityFile[1]=UnityPlayer.dll
	for /F "tokens=2 delims==" %%s in ('set ExeUnityFile[') do (
		if exist "!ExecutableFolderPath!\%%s" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET "FetchedBackend=Unity"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			)
			)
	::FMOD
	SET ExeFMODFile[0]=fmod_event.dll
	SET ExeFMODFile[1]=fmodex.dll
	for /F "tokens=2 delims==" %%s in ('set ExeFMODFile[') do (
		if exist "!ExecutableFolderPath!\%%s" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET "FetchedBackend=FMOD"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			)
		)
	::Miles Sound System
	if exist "!ExecutableFolderPath!\mss32.dll" (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36mmss32.dll[0m"
		SET "FetchedBackend=Miles Sound System"
		CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
		)
	::XAudio 2.9 > Microsoft Spatial Sound
	SET XAudioFile[0]=XAudio2_9.dll
	SET XAudioFile[1]=xaudio2_9redist.dll
	for /F "tokens=2 delims==" %%s in ('set XAudioFile[') do (
		if exist "!ExecutableFolderPath!\%%s" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET "FetchedBackend=XAudio2"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			SET FetchedWrapper=""
			SET FetchedWrapperVersion=""
			SET "FetchedRenderer=Windows Sonic for Headphones"
			CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
			SET FetchedRendererVersion=""
			SET "FetchedConfiguration=Headphone Spatial Audio"
			CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
			)
		)
	::MSSpatial > Microsoft Spatial Sound
	if exist "!ExecutableFolderPath!\MSSpatial.dll" (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36mMSSpatial.dll[0m"
		SET "FetchedBackend=Microsoft Spatial Sound"
		CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
		SET FetchedWrapper=""
		SET FetchedWrapperVersion=""
		SET "FetchedRenderer=Windows Sonic for Headphones"
		CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
		SET FetchedRendererVersion=""
		SET "FetchedConfiguration=Headphone Spatial Audio"
		CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
		)
	::X3DAudio 1.7 > X3DAudio HRTF
	if exist "!ExecutableFolderPath!\x3daudio1_7.dll" (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36mx3daudio1_7.dll[0m"
		SET "FetchedBackend=X3DAudio"
		CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
		SET FetchedWrapper=""
		SET FetchedWrapperVersion=""
		SET "FetchedRenderer=X3DAudio HRTF"
		CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
		SET FetchedRendererVersion=""
		SET "FetchedConfiguration=Headphone Spatial Audio"
		CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
		)
	::Unreal Engine > ALAudio
	SET ExeUnrealFile[0]=Core.int
	SET ExeUnrealFile[1]=Core.u
	for /F "tokens=2 delims==" %%s in ('set ExeUnrealFile[') do (
		if exist "!ExecutableFolderPath!\%%s" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET "FetchedBackend=Galaxy Audio System"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			SET "FetchedWrapper=ALAudio"
			CALL :PrintAndLog "[1m Wrapper:[0m          [36m!FetchedWrapper![0m"
			SET FetchedWrapperVersion=""
			SET "FetchedRenderer=OpenAL Soft"
			CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
			SET FetchedRendererVersion=""
			SET "FetchedConfiguration=Headphone Spatial Audio"
			CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
			)
		)
	::OpenAL > OpenAL Soft
	SET OpenALFile[0]=OpenAL32.dll
	SET OpenALFile[1]=soft_oal.dll
	SET OpenALFile[2]=DefOpenAL32.dll
	for /F "tokens=2 delims==" %%s in ('set OpenALFile[') do (
		if exist "!ExecutableFolderPath!\%%s" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET Backend=
			SET "FetchedBackend=OpenAL"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			SET Wrapper=
			SET FetchedWrapper=""
			SET WrapperVersion=
			SET FetchedWrapperVersion=""
			SET "FetchedRenderer=OpenAL Soft"
			CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
			SET RendererVersion=
			SET FetchedRendererVersion=""
			SET "FetchedConfiguration=Headphone Spatial Audio"
			CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
			set "OpenALSoftDLLFullPath=!ExecutableFolderPath!\%%s"
			)
		)
	::Unreal Engine 4 > X3DAudio HRTF
	SET UnrealParentFolders[0]=-Shipping
	SET UnrealParentFolders[1]=Binaries\Win
	SET UnrealParentFolders[2]=Binaries/Win
	SET UnrealParentFolders[3]=Binaries\Retail
	SET UnrealParentFolders[4]=Binaries/Retail
	for /F "tokens=2 delims==" %%s in ('set UnrealParentFolders[') do (
		if /I not "!ExecutableFilePath!"=="!ExecutableFilePath:%%s=!" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET Backend=
			SET "FetchedBackend=X3DAudio"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			SET Wrapper=
			SET FetchedWrapper=""
			SET Renderer=
			SET "FetchedRenderer=X3DAudio HRTF"
			CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
			SET RendererVersion=
			SET FetchedRendererVersion=""
			SET Configuration=
			SET "FetchedConfiguration=Headphone Spatial Audio"
			CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
			IF NOT "!ExecutableFilePath!"=="!ExecutableFilePath:Win32=!" (set FetchedRendererVersion=!X3DAudioHRTFVersion:Win64=Win32!)
			IF NOT "!ExecutableFilePath!"=="!ExecutableFilePath:Win64=!" (set FetchedRendererVersion=!X3DAudioHRTFVersion:Win32=Win64!)
			)
		)
	::DirectSound/EAX > DSOAL
	SET DirectSoundFile[0]=dsound.dll
	SET DirectSoundFile[1]=eax.dll
	SET DirectSoundFile[2]=wrap_oal.dll
	for /F "tokens=2 delims==" %%s in ('set DirectSoundFile[') do (
		if exist "!ExecutableFolderPath!\%%s" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[92m Configuration auto-detection found[0m [36m%%s[0m"
			SET "FetchedBackend=DirectSound3D"
			CALL :PrintAndLog "[1m Backend:[0m          [36m!FetchedBackend![0m"
			SET "FetchedWrapper=DSOAL"
			CALL :PrintAndLog "[1m Wrapper:[0m          [36m!FetchedWrapper![0m"
			SET FetchedWrapperVersion=""
			SET "FetchedRenderer=OpenAL Soft"
			CALL :PrintAndLog "[1m Renderer:[0m         [36m!FetchedRenderer![0m"
			SET FetchedRendererVersion=""
			SET "FetchedConfiguration=Headphone Spatial Audio"
			CALL :PrintAndLog "[1m Configuration:[0m    [36m!FetchedConfiguration![0m"
			)
		)
	EXIT /B

:DetectDesktopShortcut
	if "!ExecutableExtension!"==".lnk" (
		for /f "delims=" %%I in ('wmic path win32_shortcutfile where "name='!ExecutableFilePath:\=\\!'" get target /value') do for /f "delims=" %%# in ("%%~I") do SET "%%~#"
		for /f "delims=" %%A in ('echo !target!') do SET "ExecutableFilePath=%%A"
		CALL :SetPaths
		if "!ExecutableFile!"=="!ExecutableFile:.exe=!" (
			SET FetchedTitle=!ExecutableFilename:.exe=!
			) ELSE (
			SET FetchedTitle=!ExecutableParentFolder!
			)
		)
	EXIT /B

:CorrectUnrealEngine4ExecutablePath
	IF NOT DEFINED InputFilePath (set "InputFilePath=!ExecutableFilePath!")
	for %%a in ("!ExecutableFilePath!") do for %%b in ("%%~dpa\.") do (SET "UnrealEngine4RootFolderName=%%~nxb")
	for /d %%x in ("!ExecutableFolderPath!\*") do (
		SET ExeUnrealArchitecture[0]=Win64
		SET ExeUnrealArchitecture[1]=Win32
		for /F "tokens=2 delims==" %%s in ('set ExeUnrealArchitecture[') do (
			if exist "%%x\Binaries\%%s\!ExecutableFilename!-%%s-Shipping.exe" (
				SET "UnrealEngine4TrueExecutablePath=%%x\Binaries\%%s\!ExecutableFilename!-%%s-Shipping.exe"
				SET "ExecutableFilePath=%%x\Binaries\%%s\!ExecutableFilename!-%%s-Shipping.exe"
				) ELSE (
				for /D %%d in ("!ExecutableFolderPath!\*") do (
					for %%f in ("%%d\Binaries\%%s\*.exe") do (
						SET "UnrealEngine4TrueExecutablePath=%%f"
						SET "ExecutableFilePath=%%f"
						)
					)
				)
			)
		)
	CALL :ResetWorkingDirectory
	IF DEFINED UnrealEngine4TrueExecutablePath (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[91m !InputFilePath![0m"
		CALL :PrintAndLog "[93m Incorrect Unreal Engine game path detected. Redirected path to true executable:[0m"
		CALL :PrintAndLog "[92m !UnrealEngine4TrueExecutablePath![0m"
REM		SET "InputFilePath=!ExecutableFilePath!"
		CALL :SetPaths
		IF NOT "!TitleSet!"=="True" (SET "FetchedTitle=!UnrealEngine4RootFolderName!")
		)
	EXIT /B

:DetectSteamShortcut
	SET "SteamID="
	if "!ExecutableExtension!"==".url" (
		SET ShortcutFilename=!ExecutableFilename!
		::Steam shortcut
		IF NOT DEFINED SteamID (
			CALL :DeleteFile "%tmp%\InputShortcut.url"
			COPY "!ExecutableFilePath:/=\!" "%tmp%\InputShortcut.url" 1>> !LogFilePath! 2>&1
			SET "ExecutableFilePath=%tmp%\InputShortcut.url"
			CALL :SetPaths
			CD /D "!ExecutableFolderPath!"
			for /f "tokens=2 delims==" %%i in ('findstr URL "!ExecutableFilePath!"') do (
				SET "GameSteamURL=%%i"
				SET "SteamID=!GameSteamURL:steam://rungameid/=!"
				CALL :Log ""
				CALL :Log "[92m Steam ID detected from desktop shortcut: !SteamID![0m"
				)
			)
		CALL :ResetWorkingDirectory
		::Get Game Steam ID
		:UseSteamID
		::Steam path
		SET "SteamInstallPath=C:\Program Files (x86)\Steam"
		SET SteamRegistry[0]=HKEY_LOCAL_MACHINE\SOFTWARE\Valve\Steam
		SET SteamRegistry[1]=HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Valve\Steam
		for /F "tokens=2 delims==" %%s in ('set SteamRegistry[') do (
			CALL :RegistryKeyQuery "%%s"
			if !RegistryKeyQueryErrorLevel! EQU 0 (CALL :RegistryKeyQueryValue "%%s" "InstallPath" "SteamInstallPath")
			)
		SET SteamLibraryPath=!SteamInstallPath!\steamapps\common
		SET "GameSteamTitleFirst="
		for /f "delims=" %%A in ('Resources\Tools\VDFParse\VDFParse.exe query "!SteamInstallPath!\appcache\appinfo.vdf" !SteamID! common.name 2^>^>!LogFilePath!') do (
		    IF NOT DEFINED GameSteamTitleFirst (
		        SET "GameSteamTitle=%%A"
		        SET "FetchedTitle=!GameSteamTitle!"
		        SET TitleSet=True
			    )
			)
		SET "GameSteamInstallDirFirst="
		for /f "delims=" %%A in ('Resources\Tools\VDFParse\VDFParse.exe query "!SteamInstallPath!\appcache\appinfo.vdf" !SteamID! installdir 2^>^>!LogFilePath!') do (
		    IF NOT DEFINED GameSteamInstallDirFirst (
		        SET "GameSteamInstallDirFirst=%%A"
		        set GameSteamInstallDir=!GameSteamInstallDirFirst!
			    )
			)
		SET "GameSteamExecutableFirst="
		for /f "delims=" %%A in ('Resources\Tools\VDFParse\VDFParse.exe query "!SteamInstallPath!\appcache\appinfo.vdf" !SteamID! executable 2^>^>!LogFilePath!') do (
			SET GameSteamExecutableInput=%%A
			IF NOT !GameSteamExecutableInput!==!GameSteamExecutableInput:.exe=! (
			    IF NOT DEFINED GameSteamExecutableFirst (
			        SET "GameSteamExecutableFirst=%%A"
			        set GameSteamExecutable=!GameSteamExecutableFirst!
				    )
				)
			)
		IF DEFINED GameSteamExecutable (
			SET "ExecutableFilePath=!SteamLibraryPath!\!GameSteamInstallDir!\!GameSteamExecutable!"
			) ELSE (
			IF DEFINED InputFilePath (
				SET "ExecutableFilePath=!InputFilePath:"=!"
				) ELSE (
				CALL :PrintAndLog "[91m Invalid input file/folder. Try using the executable (.exe) file instead.[0m"
				pause
				GOTO :MainScreen
				)
			)
		::Scan extra library folders
		IF NOT exist "!ExecutableFilePath!" (
			CALL :PrintAndLog ""
			CALL :PrintAndLog "[91m !ExecutableFilePath! does not exist[0m"
			CALL :PrintAndLog "[93m Searching other library folders...[0m"
			SET SteamLibraryFolderCount=0


			for /f "tokens=1 delims==" %%i in ('@findstr /i "\\" "!SteamInstallPath!\steamapps\libraryfolders.vdf"') do (
				SET /a SteamLibraryFolderCount=SteamLibraryFolderCount+1
				for /f "delims=" %%L in ("!SteamLibraryFolderCount!") do (
					SET SteamLibraryFolder[%%L]=%%i
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:	=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"path"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"1"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"2"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"3"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"4"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"5"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"6"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"7"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"8"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"9"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:"=!
					SET SteamLibraryFolder[%%L]=!SteamLibraryFolder[%%L]:\\=\!
					if exist "!SteamLibraryFolder[%%L]!\steamapps\common\!GameSteamInstallDir!\!GameSteamExecutable!" (
						SET "ExecutableFilePath=!SteamLibraryFolder[%%L]!\steamapps\common\!GameSteamInstallDir!\!GameSteamExecutable!"
						CALL :PrintAndLog "[92m Game executable found in: !ExecutableFilePath![0m"
						GOTO :SteamLibraryFolderFound
						) ELSE (
						CALL :PrintAndLog "[91m !GameSteamExecutable! not found in !SteamLibraryFolder[%%L]!\steamapps\common\!GameSteamInstallDir!\[0m"
						PAUSE
						)
					)
				)
			)
		:SteamLibraryFolderFound
		CALL :SetPaths
		)
	EXIT /B

:GetHalfLifeModTitle
	SET HalfLifeModCount=0
	SET HalfLifeModName=
	for /d %%x in ("!ExecutableFolderPath!\*") do (
		IF EXIST "%%x\liblist.gam" (
			SET /A HalfLifeModCount=HalfLifeModCount+1
			SET HalfLifeMod[!HalfLifeModCount!]=%%~nx
			for /f "delims=" %%m in ("!HalfLifeModCount!") do (
				IF NOT DEFINED HalfLifeModFolder[%%m] (set HalfLifeModFolder[%%m]=%%x)
				for /f "delims=" %%A in ('findstr game "%%x\liblist.gam"') do (
					IF NOT DEFINED HalfLifeModName[%%m] (
						SET HalfLifeModName[%%m]=%%A
						SET HalfLifeModName[%%m]=!HalfLifeModName[%%m]:game =!
						SET HalfLifeModName[%%m]=!HalfLifeModName[%%m]:"=!
						SET FetchedTitle=!HalfLifeModName[%%m]!
rem						SET Title=!HalfLifeModName[%%m]!
						SET HalfLifeModFolderName=%%~nx
						)
					)
				)
			)
		)
	::Get steam ID from file
	IF NOT DEFINED HalfLifeModName[!HalfLifeModCount!] (
		IF NOT DEFINED SteamID (
			CD /D "!ExecutableFolderPath!"
			for /R %%f in (steam_appid.txt) do (
				IF EXIST "%%f" (
					CD /D "%%~dpf"
					for /f "delims=" %%x in (%%~nxf) do (
						SET SteamID=%%x
						CALL :Log ""
						CALL :Log "[92m Steam ID detected from steam_appid.txt: !SteamID![0m"
						CD /D %~dp0
						IF NOT "!ExecutableFilePath!"=="!ExecutableFilePath:steamapps=!" (
							IF NOT !SteamID!=="" (
								GOTO :UseSteamID
								)
							) ELSE (
							GOTO :DoNotUseSteamID
							)
						)
					)
				)
			CALL :ResetWorkingDirectory
			)
		)
	EXIT /B


:DeleteFile
	if exist %1 (
		del %1 1>> !LogFilePath! 2>&1
		)
	EXIT /B

:SetDataFilePaths
	CALL :CreateFolder "User"
	SET "UserPreferencesFile=User\Preferences.ini"
	SET "DefaultConfigurationFilePath=User\!ScriptName: =!.cfg"
	SET "UserConfigurationFilePath=!DefaultConfigurationFilePath!"
	CALL :SetOnlineDatabasePath
	SET "ManualStepsFile=User\ManualSteps.txt"
	SET "CreditsFile=User\Credits.txt"
	SET "NotesFile=User\Notes.txt"
	CALL :LogLevelOff
	EXIT /B

:GetOSVersion
	SET Windows10OSVersion=649841
	for /f "tokens=4-6 delims=. " %%i in ('ver') do SET "OSVersion=%%i%%j%%k"
		if !OSVersion! GEQ 10022000 (
			SET WindowsVersion=11
			) ELSE (
			if !OSVersion! GEQ !Windows10OSVersion! (
				SET WindowsVersion=10
				) ELSE (
				if !OSVersion! GEQ 639200 (
					SET WindowsVersion=8.1
					) ELSE (
					if !OSVersion! GEQ 628102 (
						SET WindowsVersion=8
						) ELSE (
						if !OSVersion! GEQ 617600 (
							SET WindowsVersion=7
							) ELSE (
							if !OSVersion! GEQ 605048 (
								SET WindowsVersion=Vista
								) ELSE (
								SET WindowsVersion=XP
								)
							)
						)
					)
				)
			)
	EXIT /B

:GetSystemPaths
	reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && SET "WindowsArchitectureBits=32" || SET "WindowsArchitectureBits=64"
	SET "ProgramFilesPath=!ProgramFiles!"
	IF "!WindowsArchitectureBits!"=="32" (
		SET "ProgramFiles32BitPath=!ProgramFiles!"
		SET "DLLFolderPath32=!WINDIR!\System32"
		SET "WindowsArchitecture=32-bit"
		SET "WindowsArchitecturexBits=x86"
		SET "SystemASIORegistryPath=HKEY_LOCAL_MACHINE\SOFTWARE\ASIO"
		) ELSE (
		SET "ProgramFiles32BitPath=!ProgramFiles(x86)!"
		SET "ProgramFiles64BitPath=!ProgramFiles!"
		SET "DLLFolderPath32=!WINDIR!\SysWOW64"
		SET "DLLFolderPath64=!WINDIR!\System32"
		SET "WindowsArchitecture=64-bit"
		SET "WindowsArchitecturexBits=x64"
		SET "SystemASIORegistryPath=HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\ASIO"
		)
	SET "StartupShortcutFolderPath=!APPDATA!\Microsoft\Windows\Start Menu\Programs\Startup"
	EXIT /B

:SetLogTimestamp
	IF NOT "!LogLevel!"=="Off" (
		echo -------------------------------------------------- !CurrentDate! -------------------------------------------------- >User\Log.txt
		)
	CALL :Log ""
	CALL :Log ""
	EXIT /B

:GetDateAndTime
	SET CurrentDate=!date:~-4,4!-!date:~-10,2!-!date:~-7,2!_!time:~0,2!-!time:~3,2!
	SET CurrentDate=!CurrentDate: =0!
	EXIT /B

:SetURLs
	SET DatabaseURL=https://kutt.it/BAMDatabase!DatabaseBranch!
	SET DatabaseMirrorURL=https://kutt.it/BAMDatabase!DatabaseBranch!Mirror
	SET BaseURL=https://kutt.it/BAMBase
	SET BaseMirrorURL=https://kutt.it/BAMBaseMirror
	SET HRTFComparisonURL=https://kutt.it/BAMHRTF
	SET SupportURL=https://kutt.it/BAMSupport
	SET OpenALSoft3D7.1URL=https://github.com/kcat/openal-soft/blob/master/docs/3D7.1.txt
	SET SoftwareURL=https://kutt.it/BAMSoftware
	EXIT /B

:SearchDatabase
	CALL :SetDatabasePath
	SET ProfileFoundInDatabase=
	SET "SearchFile[0]=DatabaseFile"
	SET "SearchFile[1]=BaseFile"
	for /F "tokens=2 delims==" %%f in ('set SearchFile[') do (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[0m Looking up profile in !%%f!..."
		SET SteamIDQuery[0]=Steam ID
		SET SteamIDQuery[1]=Base Steam ID
		for /F "tokens=2 delims==" %%s in ('set SteamIDQuery[') do (
			IF DEFINED SteamID (
				CALL :DatabaseSearch "%%s" "(?:^^|\W)!SteamID!(?:$|\W)"					"!%%f!" 									"User\Database-Matching.csv"
				CALL :DatabaseSlice "User\Database-Matching.csv" "User\Database-Matching+TopResult.csv"
				::Filter by Title
				CALL :DatabaseSelect "Title" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Title.csv"
				for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Title.csv) do (
					IF NOT %%i=="" (
						IF NOT "%%i"=="[Unknown]" (
							SET FetchedTitle=%%i
							CALL :PrintAndLog "[0m Profile detected by Steam ID ([1m!SteamID![0m):"
							CALL :PrintAndLog "[36m !FetchedTitle![0m"
							if "%%f"=="BaseFile" (SET "ProfileFoundInDatabase=False") ELSE (SET "ProfileFoundInDatabase=True")
							GOTO :FilterField
							)
						) ELSE (
						SET FetchedTitle=!GameSteamTitle!
						)
					)
				)
			)
		SET "ExeSearchPath[0]=!ExecutableParentFolder!\!ExecutableFile!"
		SET "ExeSearchPath[1]=!ExecutableParentFolder!/!ExecutableFile!"
		SET "ExeSearchPath[2]=!ExecutableFile!"
		for /F "tokens=2 delims==" %%s in ('set ExeSearchPath[') do (
			SET "ExeSearchQuery[0]=Path"
			SET "ExeSearchQuery[1]=Relative path"
			SET "ExeSearchQuery[2]=Executable"
			for /F "tokens=2 delims==" %%q in ('set ExeSearchQuery[') do (
				SET ExeSearchPathEscaped=%%s
				CALL :EscapeRegExCharacters ExeSearchPathEscaped
				CALL :DatabaseSearch "%%q" "!ExeSearchPathEscaped!"						"!%%f!" 									"User\Database-Matching.csv"
				CALL :DatabaseSlice "User\Database-Matching.csv" "User\Database-Matching+TopResult.csv"
				CALL :DatabaseSelect "Title" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Title.csv"
				for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Title.csv) do (
					IF NOT %%i=="" (
						IF NOT "%%i"=="[Unknown]" (
							IF NOT DEFINED GameSteamTitle (
								SET FetchedTitle=%%i
								) ELSE (
								SET FetchedTitle=!GameSteamTitle!
								)
							CALL :PrintAndLog "[0m Profile detected by %%q ([1m%%s[0m) in !%%f!:"
							CALL :PrintAndLog "[36m !FetchedTitle![0m"
							if "%%f"=="BaseFile" (SET "ProfileFoundInDatabase=False") ELSE (SET "ProfileFoundInDatabase=True")
							GOTO :FilterField
							)
						)
					)
				)
			)
		IF NOT DEFINED ProfileFoundInDatabase (CALL :PrintAndLog "[93m No matching profiles found.[0m")
		)
	IF NOT DEFINED ProfileFoundInDatabase (
		IF NOT DEFINED ProfileSearchedInBase (
			SET ProfileSearchedInBase=True
			CALL :Fetch Base
			GOTO :SearchDatabase
			)
		)

	:FilterField
	::Filter Backend
	CALL :DatabaseSelect "Backend" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Backend.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Backend.csv) do (
		IF NOT %%i=="" (
			SET "FetchedBackend=%%i"
			SET "FetchedBackend=!FetchedBackend:"=!"
			)
		)
	::Filter Wrapper
	CALL :DatabaseSelect "Wrapper" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Wrapper.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Wrapper.csv) do (
		IF NOT "%%i"=="[None]" (
			IF NOT %%i=="" (
				SET FetchedWrapper=%%i
				SET FetchedWrapper=!FetchedWrapper:"=!
				) ELSE (
				SET FetchedWrapper=""
				)
			) ELSE (
			SET FetchedWrapper=""
			)
		)
	::Filter Wrapper version
	CALL :DatabaseSelect "Wrapper version" 	"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+WrapperVersion.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+WrapperVersion.csv) do (
		IF NOT %%i=="" (
			SET FetchedWrapperVersion=%%i
			SET FetchedWrapperVersion=!FetchedWrapperVersion:"=!
			)
		)
	::Filter Renderer
	CALL :DatabaseSelect "Renderer" 		"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Renderer.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Renderer.csv) do (
		IF NOT %%i=="" (SET FetchedRenderer=%%i)
		)
	::Filter Renderer version
	CALL :DatabaseSelect "Renderer version"	"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+RendererVersion.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+RendererVersion.csv) do (
		SET FetchedRendererVersion=%%i
		)
	::Filter by Configuration
	CALL :DatabaseSelect "Configuration" 	"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Configuration.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Configuration.csv) do (
		IF NOT %%i=="" (SET "FetchedConfiguration=%%i")
		SET FetchedConfiguration=!FetchedConfiguration:"=!
		)
	::Filter by HRTF
	CALL :DatabaseSelect "HRTF" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+HRTF.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+HRTF.csv) do (
		IF NOT %%i=="" (
			SET FetchedHRTF=%%i
			IF "!FetchedHRTF!"=="!FetchedHRTF:_(Inverted_Y)=!" (
				SET FetchedHRTF=""
				) ELSE (
				IF "!HRTF!"=="!HRTF:SADIE=!" (
					SET "FetchedHRTF=SADIE_D02_(Inverted_Y)"
					) ELSE (
					SET "FetchedHRTF=!HRTF!_(Inverted_Y)"
					)
				)
			) ELSE (
			SET FetchedHRTF=""
			)
		)
	::Filter by ID
	CALL :DatabaseSelect "ID" 				"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+ID.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+ID.csv) do (
		IF NOT %%i=="" (
			SET FetchedBaseVersion=%%i
			SET FetchedGuideURL=https://airtable.com/shrvkY8YGm9nfWL0d/tblNOTdmp5nHXfFGU/viwscbu7mNHSxavRs/rec%%i
			)
		)
	::Filter by Base ID
	CALL :DatabaseSelect "Base ID" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Base.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Base.csv) do (
		IF NOT %%i=="" (
			SET FetchedBase=%%i
			)
		)
	::Filter by Manual steps
	CALL :DatabaseSelect "Manual steps" 	"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+ManualSteps.csv"
	CALL :DeleteFile "!ManualStepsFile!"
	SET ManualStepsCount=0
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+ManualSteps.csv) do (
		IF NOT %%i=="" (
			SET ManualStep=%%i
			SET ManualStep=!ManualStep:"=!
			IF DEFINED ManualStep (
				SET ManualStep=!ManualStep:'-=-!
				echo !ManualStep!>>"!ManualStepsFile!"
				)
			)
		)
	::Filter by Notes
	CALL :DatabaseSelect "Notes" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Notes.csv"
	CALL :DeleteFile "!NotesFile!"
	SET NotesCount=0
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Notes.csv) do (
		IF NOT %%i=="" (
			SET ManualStep=%%i
			SET ManualStep=!ManualStep:"=!
			IF DEFINED ManualStep (
				SET ManualStep=!ManualStep:'-=-!
				echo !ManualStep!>>"!NotesFile!"
				)
			)
		)
	::Filter by Credits
	CALL :DatabaseSelect "Credits" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Credits.csv"
	CALL :DeleteFile "!CreditsFile!"
	SET CreditsCount=0
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Credits.csv) do (
		IF NOT %%i=="" (
			SET "Credit=%%i"
			SET Credit=!Credit:"=!
			IF DEFINED Credit (
				echo !Credit!>>"!CreditsFile!"
				)
			)
		)
	::Filter by Settings
	CALL :DatabaseSelect "Settings" 		"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Settings.csv"
	SET SettingsCount=0
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Settings.csv) do (
		IF NOT %%i=="" (
			SET "Setting=%%i"
			SET "Setting=!Setting:"""=""!"
			SET "Setting=!Setting:""="!"
			CALL :CreateFileConfiguration !Setting!
			)
		)
	::Filter by Prelude
	CALL :DatabaseSelect "Prelude" 			"User\Database-Matching+TopResult.csv"		"User\Database-Matching+TopResult+Prelude.csv"
	for /F "skip=1 delims=" %%i in (User\Database-Matching+TopResult+Prelude.csv) do (
		IF NOT %%i=="" (SET "FetchedPrelude=%%i")
		SET FetchedPrelude=!FetchedPrelude:"=!
		)

	::ParseSettings
	IF NOT exist "%1" (echo. 2>"%1") >NUL 2>&1
	::Cleanup
	CALL :DeleteFile "User\*.csv"
	EXIT /B

:UpdateBinauralSoftwareVersions
	CALL :SetBinauralSoftwareTypes
	for /F "tokens=2 delims==" %%T in ('set BinauralSoftwareType[') do (
		if !Fetched%%TVersion!=="" (
			if "!%%T!"=="!Fetched%%T!" (
				for /f "delims=" %%A in ("!%%T: =!") do (
					SET "Fetched%%TVersion=!%%AVersion!"
					SET %%TVersion=
					)
				) ELSE (
				for /f "delims=" %%A in ("!Fetched%%T!") do SET "%%TVersion=!%%AVersion!"
				)
			)
		IF DEFINED Fetched%%T (
			IF NOT !Fetched%%T!=="" (
				SET "%%T=!Fetched%%T!"
				)
			)
		)
	EXIT /B

:PrintManualSteps
	CALL :ResetWorkingDirectory
	if exist "!ManualStepsFile!" (
		for /f "delims=" %%a in ('Type "!ManualStepsFile!"') Do (
			CALL :PrintAndLog "[93m %%a[0m"
			)
		CALL :DeleteFile "!ManualStepsFile!"
		)
	EXIT /B

:PrintNotes
	CALL :ResetWorkingDirectory
	if exist "!NotesFile!" (
		for /f "delims=" %%a in ('Type "!NotesFile!"') Do (
			CALL :PrintAndLog "[93m %%a[0m"
			)
		CALL :DeleteFile "!NotesFile!"
		)
	EXIT /B

:PrintCredits
	CALL :ResetWorkingDirectory
	if exist "!CreditsFile!" (
		CALL :PrintAndLog ""
		CALL :PrintAndLog " Credits:"
		for /f "delims=" %%a in ('Type "!CreditsFile!"') Do (
			CALL :PrintAndLog "[1;95m %%a[0m"
			)
		CALL :DeleteFile "!CreditsFile!"
		)
	EXIT /B

:CheckIfInputRunning
	CALL :CheckIfRunning "!ExecutableFile!"
	if "!ERRORLEVEL!"=="0" (
		IF NOT "!InputStillRunning!"=="True" (CALL :PrintActionAndLog "[93m Executable (!ExecutableFile!) is still running.[0m [96mPlease close it to continue with installation...[0m")
		timeout 1 >NUL
		SET InputStillRunning=True
		GOTO :CheckIfInputRunning
		)
	EXIT /B

:EscapeRegExCharacters
	SET "%1=!%1:\=\\!"
	SET "%1=!%1:(=\(!"
	SET "%1=!%1:)=\)!"
	SET "%1=!%1:.=\.!"
	EXIT /B

:AddDesktopShortcut
	if exist "!USERPROFILE!\Desktop\!ScriptName!.lnk" (set "ScriptDesktopShortcut=Exists")
	IF NOT "!ScriptDesktopShortcut!"=="Created" (
		CALL :CreateShortcut "!USERPROFILE!\Desktop\!ScriptName!.lnk" "%~dpnx0" "" "%~dp0" "%~dp0Resources\Graphics\!ScriptName!.ico"
		CALL :SavePreferenceSetting "ScriptDesktopShortcut" "Created"
		)
	EXIT /B

:CheckIfEditFileWasCreated
	if exist "!InstallationFileDestinationFullPath[%1]:.edit=!" (
		CALL :ErrorCheckSuccess
		CALL :CreateFolder "!BackupPath!\!InstallationFileSourceRelativeFolderPathEdit[%1]!"
		CALL :Log ""
		CALL :Log "Backing up !InstallationFileDestinationFullPathEdit[%1]:.edit=! into !BackupPath!\!InstallationFileSourceRelativePathEdit[%1]:.edit=!"
		IF NOT exist "!BackupPath!\!InstallationFileSourceRelativePath[%1]:.edit=!" (copy /y "!InstallationFileDestinationFullPathEdit[%1]:.edit=!" "!BackupPath!\!InstallationFileSourceRelativePathEdit[%1]:.edit=!") 1>> !LogFilePath! 2>&1
		CALL :CheckIfInputRunning
		CALL :ErrorCheckSuccess
		CALL :UpdateFileConfiguration "!InstallationFileDestinationFullPath[%1]:.edit=!" "!FileEditSection[%1]!" "!FileEditKey[%1]!" "!FileEditValue[%1]!" ""
		SET !FileEditKey[%1]!Defined=True
		EXIT /B
		)
	timeout 1 >NUL
	GOTO :CheckIfEditFileWasCreated
	EXIT /B

:ClearConfigurationFile
	IF DEFINED ExecutableFolderPath (
		SET "UserConfigurationFilePath=!PortableConfigurationFilePath!"
		CALL :SaveConfiguration
		)
	CALL :DeleteFile "!DefaultConfigurationFilePath!"
	EXIT /B

:PromptInput
	SET %1=
	SET /P %1=%2
	EXIT /B 0

:SelectLogLevel
	cls
	CALL :PrintAndLog ""
	IF NOT "!LogLevel!"=="Off" (
		CALL :PrintAndLog "[90m Currently selected log level:[0m [1m!LogLevel![0m"
		) ELSE (
		CALL :PrintAndLog "[90m Currently selected log level:[0m [1mOff[0m"
		)
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[1m 0[0m [0mOff   - No logging.[0m"
	CALL :PrintAndLog "[1m 1[0m [93mInfo  - Minimal logging.[0m"
	CALL :PrintAndLog "[1m 2[0m [91mDebug - Logs all errors and printed text. Might slow down script.[0m"
	CALL :PrintAndLog ""
	CALL :PrintAndLog "[90m To change, type the corresponding[0m [1mindex number[0m [90mthen press[0m [0mEnter[0m. [1m"
	CALL :PromptInput LogLevelSelect
	if "!LogLevelSelect!"=="" (
		GOTO :MainScreen
		) ELSE (
		SET LogLevelSelect=!LogLevelSelect: =!
		IF !LogLevelSelect!==0 (
			CALL :LogLevelOff
			) ELSE (
			IF !LogLevelSelect!==1 (
				CALL :LogLevelInfo
				) ELSE (
				IF !LogLevelSelect!==2 (
					CALL :LogLevelDebug
					) ELSE (
					GOTO :SelectLogLevel
					)
				)
			)
		)
	CALL :SetLogLevel
	CALL :SavePreferenceSetting "LogLevel" "!LogLevel!"
	EXIT /B

:SetLogLevel
	if "!LogLevel!"=="Off"		(CALL :LogLevelOff)
	if "!LogLevel!"=="Info"		(CALL :LogLevelInfo)
	if "!LogLevel!"=="Debug"	(CALL :LogLevelDebug)
	EXIT /B

:LogLevelOff
	SET LogLevel=Off
	SET LogFilePath=NUL
	EXIT /B

:LogLevelInfo
	SET LogLevel=Info
	CALL :LogDefaultPath
	EXIT /B

:LogLevelDebug
	SET LogLevel=Debug
	CALL :LogDefaultPath
	EXIT /B

:LogDefaultPath
	SET "LogFilePath=User\Log.txt"
	EXIT /B

:CheckElevatedPrivileges
	NET SESSION >nul 2>&1
	EXIT /B

:CheckExecutableFilePath
	IF NOT EXIST "!ExecutableFilePath!" (
		IF DEFINED ExecutableFile (
		CALL :PrintAndLog ""
		CALL :PrintAndLog "[91m ERROR: Input does not exist[0m"
			CALL :PrintAndLog "[93m !ExecutableFile! does not exist in the specified location: !ExecutableFolderPath![0m"
			CALL :PrintAndLog "[96m Press any key to continue to the main menu and drop the !ExecutableParentFolder! folder or !ExecutableFile! directly into this window to get the correct path.[0m"
			pause>NUL
			GOTO :ExecutableMenu
			)
		) ELSE (
		IF NOT DEFINED InputFilePath (SET "InputFilePath=%~1")
		)
	EXIT /B

:AdjustSampleRate
	SET SampleRate=48000
	SET SampleRateAdjusted%1=True
	SET SampleRateAdjusted=%1
	EXIT /B

:ParseDSoundINI
	SET DSoundINICount=0
	for /l %%C in (1,1,!InstallationFileCount!) do (
		IF "!InstallationFile[%%C]!"=="dsound.ini" (
			pushd "!InstallationFolderDestinationPath[%%C]!"
			for /F "tokens=*" %%A in (dsound.ini) do (
				if exist "!InstallationFileDestinationFullPath[%%C]!" (
					SET /A DSoundINICount=DSoundINICount+1
					for /f "delims=" %%a in ("!DSoundINICount!") do (
						SET "DSoundINILine[%%a]=%%A%%0A"
						SET "DSoundINI=!DSoundINI!!DSoundINILine[%%a]!"
						)
					)
				)
			CALL :ResetWorkingDirectory
			)
		)
	EXIT /B

:SetUpDirectSound
	CALL :PrintAndLog ""
	CALL :RegisterDirectSound
	CALL :ParseDSoundINI
	EXIT /B

:RunAndWait
	For %%A in (%1) do (
		CALL :ForceCloseIfRunning "%%~nxA"
		)
	START "" /wait %*
	EXIT /B

:InstallSilent
	%1 /S 1>> !LogFilePath! 2>&1
	EXIT /B


:InstallSilentInno
	if exist %1 (
		For %%A in (%1) do (
			SET InstallSilentInnoFlags=/VERYSILENT /SUPPRESSMSGBOXES /FORCECLOSEAPPLICATIONS /NORESTART
			IF EXIST "%%~dpA\%%~nA.inf" (SET InstallSilentInnoFlags=!InstallSilentInnoFlags! /LOADINF="%%~dpA%%~nA.inf")
			CALL :Log ""
			CALL :Log "Installing Inno installer silently: %~1 !InstallSilentInnoFlags!"
			CALL %1 !InstallSilentInnoFlags! 1>> !LogFilePath! 2>&1
			)
		)
	EXIT /B

:GetExecutableArchitecture
	IF DEFINED ExecutableFilePath (
		IF EXIST "!ExecutableFilePath!" (
			SET ExecutableArchitecturexBits=
			for /f "delims=x tokens=2 usebackq" %%a in (`^""Resources\Tools\7-Zip\7z.exe" l "!ExecutableFilePath!" 2^>^>!LogFilePath! ^| findstr CPU^"`) do SET "ExecutableArchitecturexBits=x%%a"
			)
		)
	EXIT /B

:CopyFile
	attrib -r %2 1>> !LogFilePath! 2>&1
	CALL :Log ""
	CALL :Log "Copying %~1 into %~2"
	COPY /Y %1 %2 1>> !LogFilePath! 2>&1
	EXIT /B

:MoveFile
	attrib -r %1 1>> !LogFilePath! 2>&1
	CALL :Log ""
	CALL :Log "Moving %~1 into %~2"
	MOVE /Y %1 %2 1>> !LogFilePath! 2>&1
	EXIT /B

:CopyFolder
	xcopy %1 %2 /s /i /y 1>> !LogFilePath! 2>&1
	EXIT /B

:CreateFolder
	IF NOT EXIST %1 (MKDIR %1)
	EXIT /B

:Extract
	CALL :CreateFolder %2
	CALL :Log ""
	CALL :Log "Extracting %~1 into %~2"
	"Resources\Tools\7-Zip\7z.exe" x %1 -aoa -y -o%2 1>> NUL 2>&1
	IF !ERRORLEVEL! NEQ 0 (
		IF !ERRORLEVEL! EQU 1 (
			CALL :Log ""
			CALL :Log "ERROR: 7-Zip operation returned error !ERRORLEVEL!: Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not compressed."
			) ELSE (
			IF !ERRORLEVEL! EQU 2 (
				CALL :Log ""
				CALL :Log "ERROR: 7-Zip operation returned error !ERRORLEVEL!: Fatal error"
				) ELSE (
				IF !ERRORLEVEL! EQU 7 (
					CALL :Log ""
					CALL :Log "ERROR: 7-Zip operation returned error !ERRORLEVEL!: Command line error"
					) ELSE (
					IF !ERRORLEVEL! EQU 8 (
						CALL :Log ""
						CALL :Log "ERROR: 7-Zip operation returned error !ERRORLEVEL!: Not enough memory for operation"
						) ELSE (
						IF !ERRORLEVEL! EQU 255 (
							CALL :Log ""
							CALL :Log "ERROR: 7-Zip operation returned error !ERRORLEVEL!: User stopped the process"
							) ELSE (
							CALL :Log ""
							CALL :Log "ERROR: 7-Zip operation returned error !ERRORLEVEL!"
							)
						)
					)
				)
			)
		)
	EXIT /B

:CreateShortcut
	SET ShortcutPath=%~1
	SET ShortcutTargetPath=%~2
	SET ShortcutArguments=%~3
	SET ShortcutWorkingDirectoryPath=%~4
	SET ShortcutIconPath=%~5
	SET "CreateShortcutCommand=powershell "$s=^(New-Object -COM WScript.Shell^).CreateShortcut^('!ShortcutPath!'^)"
	IF DEFINED ShortcutTargetPath 			(SET "CreateShortcutCommand=!CreateShortcutCommand!;$s.TargetPath='"!ShortcutTargetPath!"'")
	IF DEFINED ShortcutArguments 			(SET "CreateShortcutCommand=!CreateShortcutCommand!;$s.Arguments='!ShortcutArguments!'")
	IF DEFINED ShortcutWorkingDirectoryPath	(SET "CreateShortcutCommand=!CreateShortcutCommand!;$s.WorkingDirectory='!ShortcutWorkingDirectoryPath!'")
	IF DEFINED ShortcutIconPath				(SET "CreateShortcutCommand=!CreateShortcutCommand!;$s.IconLocation='!ShortcutIconPath!'")
	SET "CreateShortcutCommand=!CreateShortcutCommand!;$s.Save()"
	For %%S in (%1) do CALL :CreateFolder "%%~dpS"
	for /f "tokens=4 delims= " %%c in ('chcp') do set "ActiveCodePage=%%c"
	chcp 437>NUL
	!CreateShortcutCommand!
	chcp !ActiveCodePage!>NUL
	IF NOT EXIST "!ShortcutTargetPath!" (CALL :Log ""
		CALL :Log "WARNING: Failed to create shortcut. !ShortcutTargetPath! does not exist.")
	SET ShortcutPath=
	SET ShortcutTargetPath=
	SET ShortcutArguments=
	SET ShortcutWorkingDirectoryPath=
	SET ShortcutIconPath=
	SET CreateShortcutCommand=
	EXIT /B

:RegistryKeyQuery
	SET RegistryKeyQueryErrorLevel=
	CALL :Log ""
	CALL :Log "Querying registry key: %~1..."
	reg query %1 1>> NUL 2>&1
	SET RegistryKeyQueryErrorLevel=!ERRORLEVEL!
	EXIT /B

:RegistryKeyQueryValue
	CALL :Log ""
	CALL :Log "Querying registry key value: %~1\%~2..."
	FOR /F "tokens=2* skip=2" %%a in ('reg query %1 /v %2 2^>^>!LogFilePath!') do SET "%~3=%%b"
	EXIT /B

:RegistryKeySetPermissions
	CALL :RegistryKeyQuery %1
	CALL :Log ""
	CALL :Log "Setting registry key permissions for: %~1..."
	if !RegistryKeyQueryErrorLevel! EQU 0 (
		"Resources\Tools\SetACL\SetACL!WindowsArchitectureBits!.exe" -on "%1" -ot reg -actn setowner -ownr "n:Administrators" -rec Yes   1>> !LogFilePath! 2>&1
		"Resources\Tools\SetACL\SetACL!WindowsArchitectureBits!.exe" -on "%1" -ot reg -actn ace -ace "n:Administrators;p:full" -rec Yes  1>> !LogFilePath! 2>&1
		)
	EXIT /B

:RegistryKeyAdd
	CALL :RegistryKeySetPermissions %1
	CALL :Log ""
	CALL :Log "Adding registry key: %~1\%~2=%~4 (%~3)..."
	reg add %1 /V %2 /T %3 /D %4 /F 1>> !LogFilePath! 2>&1
	EXIT /B

:RegistryKeyDelete
	CALL :RegistryKeySetPermissions %1
	CALL :Log ""
	CALL :Log "Deleting registry key: %~1..."
	reg delete %1 /f 1>> !LogFilePath! 2>&1
	EXIT /B 0

:RegistryKeyExport
	For %%F in (%2) do CALL :CreateFolder "%%~dpA"
	CALL :RegistryKeySetPermissions %1
	CALL :Log ""
	CALL :Log "Exporting registry key: %~1 into %~2..."
	reg export %1 %2 /y 1>> !LogFilePath! 2>&1
	EXIT /B

:ResetWorkingDirectory
	pushd "%~dp0"
	EXIT /B

:DatabaseSearch
	"Resources\Tools\XSV\xsv.exe" search -i -s %1 %2 %3 -o %4 1>> !LogFilePath! 2>&1
	EXIT /B

:DatabaseSlice
	"Resources\Tools\XSV\xsv.exe" slice -s 0 -l 1 %1 -o %2 1>> !LogFilePath! 2>&1
	EXIT /B

:DatabaseSelect
	"Resources\Tools\XSV\xsv.exe" select %1 %2	-o %3 1>> !LogFilePath! 2>&1
	for /F "skip=1 delims=" %%i in (%~3) do SET "%~4=%%i"
	EXIT /B

:BrowseURL
	START %~1
	EXIT /B

:CheckEULA
	IF NOT "!EULA!"=="Agreed" (
		CALL :SplashInfoSmall
		CALL :BrowseURL "!SoftwareURL!"
		SET LicenseFile[0]=License.txt
		SET LicenseFile[1]=License.BAM
		for /F "tokens=2 delims==" %%L in ('set LicenseFile[') do (
		For /R Resources %%l IN (%%L) do (
			ECHO [0m
			ECHO [0m
			ECHO [7m%%l[0m
			type "%%l"
			)
		ECHO [0m
		ECHO [96m Press any key to agree to all the licenses as required by the included software. Otherwise, close this window.[0m
		pause>NUL
		CALL :SavePreferenceSetting "EULA" "Agreed"
		)
	EXIT /B