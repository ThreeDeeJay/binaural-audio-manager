Crystal Mixer - OpenAL 3D Sound Mixer & Output as ASIO Driver

For compile you need:
1. OpenAL 1.1 SDK (http://openal.org/downloads/OpenAL11CoreSDK.zip)
2. ASIO SDK 2.3 (http://www.steinberg.net/sdk_downloads/asiosdk2.3.zip)
3. EAX 2.0 SDK (https://sourceforge.net/projects/foobar-openal/files/foo_dsp_openal/OpenAL%20SDK%20%28by%20Creative%29/EAX20_SDK_extracted.7z)
4. QT 5.5 SDK (http://www.qt.io/download-open-source/#section-2)
5. Visual Studio 2013
6. Visual Studio Add-in for Qt5
7. Compiled version of crystal_mixer.dll can be registered with regsrv32.exe in system32/syswow64 folder


VERSION HISTORY
2019.09.09     Changes in v1.2.0.0:
- Recompile with Visual Studio 2019 and Qt 5.13.1;
- Openal Soft library updated to 1.19.1;
- Fixed 32 bit depth audio output with new Openal Soft library;
- Added support for HDPI sreen resolution displays in CrystalMixer panel and alsoft-config panel;

2017.08.27     Changes in v1.1.0.6:
- Fixed loss of control;
- Fixed playback stop after switching 'Auxiliary effects';
- Restored up to 4 Auxiliary slots (if supported by the driver);

2017.08.20     Changes in v1.1.0.5:
- Fixed many bugs, improved stability;
- Fixed OpenAL Soft output devices code page;
- Fixed Control panel EAX20/EFX interface bugs;
- More stable memory management;

2017.08.06     Changes in v1.1.0.4:
- Fixed some memory problems;
- Fixed strem initialization problem;
- Added advanced device list in OpenAL Soft;
- Fixed compability problems;

2017.08.05     Changes in v1.1.0.3:
- Added little changes to be compatible with Windows XP
- Fix for playback stream reinitialization

2017.07.27     Changes in v1.1.0.2:
- Fixed BSOD problem with Creative driver in 3D mode / EFX after incorrect exit

2017.07.08     Changes in v1.1.0.1:
- Fixed some memory problems;
- Interface usability improving;
- Known issues: possible bsod with hardware driver when try to play after player incorrect exit

2017.06.18     Changes in v1.1.0.0:
- Fixed incorrect 3D view when sound field not in center
- Fixed custom channel mapping bug in foobar2000
- Fixed some memory errors
- Fixed compability with Multichannel DirectShow ASIO Renderer
- Added 3D movement effects
- Added 3D realtime view

2016.05.01 Changes in v1.0.9.7:
- Added 3D View for visual control
- Added EFX and EAX20 support in 2D mode
- Fixed channel mapping for mono channel in 2D mode
- Fixed bug when effect not starts on play
- Compiled with QT 5.5 SDK

2016.03.26 Changes in v1.0.9.0:
- Fixed incorrect buffer position at the end of playback
- Internal optimisation, lesser latency with "heavy" settings may be possible
- Added low-level settings automatic change control (may not be supported by the host program)

2016.03.15 Changes in v1.0.8.6:
- Fixed installer problems

2016.03.06 Changes in v1.0.8.5:
- First public release