Credits and License

Credit and thanks go to Дмитрий Н., who put together the A3D-Live package, which served as the basis and inspiration... and to John-Paul Ownby, the author of IndirectSound, for his terrific wrapper and generous license that allows for incorporating and re-distrubuting his work.

A3D-Live!:
http://www.worknd.ru
(c) Дмитрий Н.

IndirectSound:
Copyright (c) 2012-2014 John-Paul Ownby
All Rights Reserved
www.indirectsound.com
indirectsound@gmail.com

The license to use IndirectSound is the following:

Copyright (c) 2012-2014 John-Paul Ownby

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

A3D © Creative Technology Ltd.
Creative ALchemy © Creative Technology Ltd.
A3D-Live! © Дмитрий Н.
IndirectSound © 2012-2014 John-Paul Ownby
A3D Alchemy © 2017 Osprey 