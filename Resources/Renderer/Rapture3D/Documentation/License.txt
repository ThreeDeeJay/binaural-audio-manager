BLUE RIPPLE SOUND LIMITED

END USER LICENCE AGREEMENT

Notice to User: Please read this End User Licence Agreement ("Licence
Agreement") carefully. By downloading and/or using all or any part of the
software and/or using the supplied Authentication Key, you ("Licensee")
indicate your acceptance of the following terms from Blue Ripple Sound
Limited, c/o BSG Valentine & Co., 7-12 Tavistock Square, London WC1H 9BQ
("Blue Ripple"). The Licensee agrees to be bound by all the terms and
conditions of this Licence Agreement. The Licensee agrees that it is
enforceable as if it were a written agreement signed by the Licensee. If
the Licensee does not agree to the terms of this Licence Agreement,
the Licensee shall not download and/or use the Software and/or use the
Authentication Key.

NOW IT IS HEREBY AGREED AS FOLLOWS:

1. DEFINITIONS

"Authentication Key" means the series of numbers and letters supplied
to the Licensee to access the Software.

"Documentation" means the electronic user manual which is either supplied
to the Licensee with the Software or which is made available to the
Licensee on the Website.

"Effective Date" means the date on which the Licensee downloads
and/or uses all or any part of the Software and/or uses the supplied
Authorisation Key.

"Internal" means the Licensee only and excludes all subsidiaries or
holding company of the Licensee.

"Licence Fees" means the one-off non-refundable licence fees payable to
Blue Ripple as set out on the Website.

"Minimum Requirements" means the minimum technical specification required
to enable the Software to function in accordance with the Documentation
and/or at all, as set out in the Documentation and/or on the Website.

"Open Source Software" means open-source software as defined by the Open
Source Initiative (http://opensource.org) or the Free Software Foundation
(http://www.fsf.org) and for the avoidance of doubt includes the
open-source software listed at http://www.blueripplesound.com/free-stuff
or elsewhere on the Website.

"Software" means the object code form of Rapture3D User Edition together
with data which may be generated through use (and which remains part)
of that software, and any additional data, bug fixes, enhancements,
or other modifications of the Software which may be provided to the
Licensee and excludes Open Source Software.

"Trial" means a temporary, non-exclusive, non-transferable licence to
Use the Software.

"Use" means to download, install, store, access, display and utilise the
Software on a single PC or laptop upon which the Software is downloaded
and accessed using the Authentication Key for a) where the Licensee is
a consumer, the Licensee's sole personal use or, b) where the Licensee
is a business, the Licensee's sole internal business purposes.

"Website" means http://www.blueripplesound.com/ or any such unique
resource locator as designated by Blue Ripple.

2. GRANT OF RIGHTS; RESTRICTIONS

2.1 Subject to all the terms and conditions of this Licence Agreement
and in consideration for the Licence Fees, Blue Ripple hereby grants the
Licensee a perpetual (terminable only as provided in clauses 2.4 and 5.1)
worldwide, non-exclusive, non-transferable, paid-up licence to Use the
Software, save where the Licensee uses the Software in accordance with
clause 2.2.

2.2 In the event that the Software is provided to the Licensee on a
Trial basis, the Licensee acknowledges and agrees that Blue Ripple may
discontinue the Licensee's ability to Use the Software and terminate
this Licence at any time on notice to the Licensee. In the event that
the Licensee wishes to recommence Use of the Software, the Licensee
should notify Blue Ripple in accordance with the terms of this Licence
Agreement. Upon receipt of this notification from the Licensee, Blue
Ripple shall request payment of a Licence Fee from the Licensee in
return for a further Authentication Key. Upon termination not followed by
payment of a Licence Fee, the Licensee shall return to Blue Ripple all
copies of the Software and Documentation in the Licensee's possession
or under its control; and the Licensee shall, where requested, certify
in writing to Blue Ripple its compliance with the foregoing.

2.3 Save as permitted by the Licensor in writing or by e-mail, except as
expressly permitted in this Licence Agreement, the Licensee shall not,
and shall not permit, enable or assist others to: (i) modify, translate,
create derivative copies of or copy the Software (other than one backup
copy which reproduces all proprietary notices), in whole or in part; (ii)
reverse engineer, decompile, disassemble or otherwise reduce the object
code of the Software to source code form; (iii) distribute, sublicence,
assign, share, timeshare, sell, rent, lease, grant a security interest
in, use for service bureau purposes, or otherwise transfer the Software
or Licensee's right to Use the Software; (iv) remove or modify any
copyright, trademark, or other proprietary notices of Blue Ripple affixed
to the media containing the Software or contained within the Software,
(v) analyse or interpret the way in which the Software manipulates the
audio stream inputs it receives in order to deliver the outputs; (vi)
use the Software for the purposes of providing an automated service
to, for the benefit of or on behalf of a third party, or (vii) Use the
Software in any manner not expressly authorised by this Licence Agreement.

2.4 The Licensee shall provide Blue Ripple with such access to the
Licensee's systems for Blue Ripple to confirm that the Software is being
used in accordance with this Licence Agreement. In the event that the
Licensee's Use of the Software is not in accordance with this Licence
Agreement, Blue Ripple shall be entitled to immediately terminate the
licence and/or disable the Authentication Key in respect of the Software
and to terminate this Licence Agreement. For the avoidance of doubt,
clause 5.3 shall apply upon termination of this Licence Agreement in
accordance with this clause 2.4.

3. PROPRIETARY RIGHTS

3.1 Blue Ripple has sole and exclusive ownership (or licence to use)
of all right, title, and interest in and to the Software, including
all copyright and any other intellectual property rights therein. This
Licence Agreement conveys a limited licence to Use the Software and
shall not be construed to convey title to or ownership of the Software
to Licensee. All rights in and to the Software not expressly granted to
the Licensee are reserved by Blue Ripple.

4. LICENCE FEE

4.1 Save where this licence is granted on a Trial basis, in consideration
for the licence granted to the Licensee hereunder, Licensee shall pay
Blue Ripple the Licence Fee using the methods of payment as set out on
the Website. Such Licence Fee shall be due and payable on the Effective
Date. Such Licence Fee is exclusive of VAT or other sales tax.

5. TERM AND TERMINATION

5.1 This Licence Agreement shall commence on the Effective Date and
continue in effect indefinitely, unless terminated in accordance with
clauses 2.4 and 5.1. If either party breaches this Licence Agreement in
any material respect, the other party may give written notice to the
breaching party of its intent to terminate, and if such breach is not
cured within thirty (30) days after the breaching party's receipt of
such notice, this Licence Agreement shall terminate without any further
notice required (but no cure period is required for any breach that
cannot be cured).

5.2 Notwithstanding clause 5.1, Blue Ripple shall be entitled to (a)
immediately terminate this Licence Agreement upon notice; and (b)
permanently prohibit the Licensee and/or the IP addresses used by the
Licensee from making any use of and/or accessing the Software in the
event the Licensee is or Blue Ripple has reasons to be believe that
the Licensee is (i) using the Software to commit a criminal act or
to cause nuisance or annoyance or inconvenience to or harass others
including without limitation, to engage in hacking activities; (ii)
using illegal and/or unlawful means to access the Software including
without limitation, hacking the Software so as to dispense with the use
of the Authentication Key or using a hacked copy of the Software; (iii)
using the Authentication Key and/or doing an act or series of acts that
shall or may reasonably be deemed to infringe any patents, copyrights,
trademarks, design rights or any other intellectual property rights or
other rights of any third parties; and/or (iv) using the Software in
breach of this Licence Agreement.

5.3 Upon any termination of this Licence Agreement, (a) the rights
and licences granted to the Licensee herein shall terminate; (b) the
Licensee shall cease all Use of the Software; (c) the Licensee shall
return to Blue Ripple all copies of the Software and Documentation in
the Licensee's possession or under its control; and (d) the Licensee
shall, where requested, certify in writing to Blue Ripple its compliance
with the foregoing.  Clauses 1, 3, 5.3, 6.3, 7 and 8 shall survive any
termination of this Licence Agreement.

6. REPRESENTATIONS AND WARRANTIES

6.1 Blue Ripple warrants that the Software will function materially in
accordance with the Documentation for a period of thirty (30) days from
the Effective Date. Blue Ripple shall have no obligation to provide any
support or maintenance services to the Licensee unless the parties enter
into a separate maintenance agreement.

6.2 The above warranty is conditional upon the Licensee complying with
the Minimum Requirements.

6.3 The warranties set forth in this clause 6 are exclusive and in
lieu of all other warranties, express or implied, including without
limitation the implied warranties of merchantability, fitness for a
particular purpose, and any warranties arising by statute or otherwise
in law or from course of dealing, course of performance, or use of trade,
all of which are hereby excluded and disclaimed.

6.4 The Licensee acknowledges that Blue Ripple relies on third
party service providers to make the Software available to the
Licensee. Consequently, Blue Ripple does not warrant that the Licensee
shall have uninterrupted access to and Use of the Software.

6.5 The Licensee hereby represents that it shall (i) comply with all
applicable local and foreign laws and regulations which may govern the
Use of the Software, and (ii) Use the Software only for lawful purposes
and in accordance with the terms of this Licence Agreement.

6.6 You agree that Blue Ripple has provided no express or implied
warranties, oral or written, to you regarding the Open Source Software
and that the Open Source Software is provided "as is" without warranty of
any kind. Any Open Source Software provided by Blue Ripple may be used
by the Licensee according to the terms and conditions of the specific
licence under which the relevant Open Source Software is distributed.

7. LIMITATION OF LIABILITY

7.1 The Licensee's sole remedy with respect to any claims arising out
of this Licence Agreement shall be limited either (i) to the replacement
of the Software by Blue Ripple; or (ii) in the aggregate to the Licence
Fee paid by the Licensee to Blue Ripple under this Licence Agreement.

7.2 In no event shall Blue Ripple be liable for any special, indirect,
incidental, or consequential damages, including loss of profits and
goodwill, business or business benefit, or the cost of procurement of
substitute products by the Licensee even if advised of the possibility
of such damages.

7.3 In no circumstances shall Blue Ripple be liable for any failure of
the Software to perform in accordance with the Documentation, or at all,
resulting from a failure by the Licensee to comply with the Minimum
Requirements.

7.4 The Licensee acknowledges that whilst the Software may be used in
combination with third party software, Blue Ripple bears no liability,
howsoever arising, for any loss, damage or cost that arises from a
failure of the Software to integrate with the Licensee's software or
third party software.

7.5 For the avoidance of doubt, nothing in this Licence Agreement
shall be deemed to exclude, restrict or limit liability of either party
(or their respective agents or sub-contractors) for death or personal
injury resulting from their negligence or any liability for fraudulent
misrepresentation.

8. GENERAL

8.1 The parties shall not assign this Licence Agreement, in whole or in
part, without the prior written consent of the other party.

8.2 The Licensee consents to the use by Blue Ripple of the Licensee's
name in customer lists and other publicity, including interviews,
case studies, and conference discussions, provided that such publicity
accurately describes the nature of the relationship between the Licensee
and Blue Ripple.

8.3 The Licensee agrees that, because of the unique nature of the
Software and Blue Ripple's proprietary rights therein, a demonstrated
breach of this Licence Agreement by the Licensee would irreparably
harm Blue Ripple and monetary damages would be inadequate compensation.
Therefore, Licensee agrees that Blue Ripple shall be entitled to bring
a claim for preliminary and permanent injunctive relief, as determined
by any court of competent jurisdiction to enforce the provisions of this
Licence Agreement.

8.4 If any provision of this Licence Agreement or the Software thereof
is declared void, illegal, or unenforceable, the remainder of this
Licence Agreement will be valid and enforceable to the extent permitted
by applicable law.  In such event, the parties agree to use their best
efforts to replace the invalid or unenforceable provision by a provision
that, to the extent permitted by the applicable law, achieves the purposes
intended under the invalid or unenforceable provision.

8.5 Any failure by any party to this Licence Agreement to enforce at
any time any term or condition under this Licence Agreement will not
be considered a waiver of that party's right thereafter to enforce each
and every term and condition of this Licence Agreement.

8.6 Neither party will be responsible for delays resulting from
circumstances beyond the reasonable control of such party, provided
that the non-performing party uses reasonable efforts to avoid or remove
such causes of non-performance and continues performance hereunder with
reasonable dispatch whenever such causes are removed.

8.7 Any notice, request, instruction or other document to be given
hereunder shall be delivered or sent by first class post or by email
(in all cases to be confirmed by letter posted within 12 hours) to
the address of the other set out or referred to in this Agreement (or
such other address as may have been notified) and any such notice or
other document shall be deemed to have been served and deemed to have
been received (if delivered) at the time of delivery (if sent by post)
upon the expiration of 48 hours after posting and (if sent by email)
upon the expiration of 12 hours after dispatch.

8.8 Neither party shall be liable for any breach of this Agreement
resulting from causes beyond its reasonable control including but not
limited to fires, strikes (of its own or other employees) insurrection or
riots, wrecks or delays in transportation, inability to obtain supplies
and raw materials requirements or regulations of any civil or military
authority (a "Force Majeure Event"). Each party agrees to give notice
forthwith to the other upon becoming aware of a Force Majeure Event, such
notice to contain details of the circumstances giving rise to the Force
Majeure Event. If a default due to a Force Majeure Event shall continue
for more than 13 weeks, then the party not in default shall be entitled
to terminate this Agreement. Neither party shall have any liability to the
other in respect of such termination as a result of a Force Majeure Event.

8.9 This Licence Agreement constitutes the entire agreement and
understanding between the parties with respect to the subject matter
hereof and supersedes all prior agreements, oral and written, made with
respect to the subject matter hereof.

8.10 These Terms are governed by and construed in accordance with English
law. The Courts of England shall have exclusive jurisdiction over any
disputes arising out of these Terms. If the Licensee is a consumer, in
accordance with the consumer protection legislation of England and Wales
and lives outside the United Kingdom, English law shall apply only to
the extent that English law shall not deprive the Licensee of any legal
protection accorded in accordance with the law of the place where the
Licensee is habitually resident ("Local Law"). In the event English law
deprives the Licensee of any legal protection which is accorded to the
Licensee under Local Law, then these terms shall be governed by Local
Law and any dispute or claim arising out of or in connection with these
Terms shall be subject to the non-exclusive jurisdiction of the courts
where the Licensee is habitually resident.
